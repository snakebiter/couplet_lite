<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

    public static function getValue($key) {
        $setting = Setting::where('key', $key)->first();
        if($setting) {
            if($setting->value == 'Yes')
                return true;
            if($setting->value == 'No')
                return false;
            return $setting->value;
        }
        return false;
    }

}
