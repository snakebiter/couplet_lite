<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $fillable = [
        'sender_id', 'receiver_id', 'message',
    ];
    protected $appends = array('is_read');
    protected $dates = ['read_at'];

    public function getIsReadAttribute($value)
    {
        return $this->read_at->eq($this->updated_at);
    }

    public function sender()
    {
        return $this->belongsTo('App\Models\User', 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\Models\User', 'receiver_id');
    }

}
