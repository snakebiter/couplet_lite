<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {
	
	protected $fillable = [
		'payment_id', 'payment_method', 'user_id',
		'processor_id', 'amount', 'subscription',
		'currency', 'expires_at',
    ];
	
	public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
