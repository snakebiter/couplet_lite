<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrivateMessageThread extends Model
{
    protected $fillable = [
        'sender_id', 'receiver_id', 'message',
    ];
    protected $appends = array('is_read');
    protected $dates = ['read_at'];

    public function getIsReadAttribute()
    {
        if($this->read_at)
            return $this->read_at->eq($this->updated_at);
        return true;
    }

    public function sender()
    {
        return $this->belongsTo('App\Models\User', 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\Models\User', 'receiver_id');
    }

}
