<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    
    public function getDataAttribute($value)
    {
        return json_decode($value);
    }

}
