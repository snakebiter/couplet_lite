<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrivateMessageCount extends Model
{
    protected $fillable = [
        'user_id', 'month'
    ];

}
