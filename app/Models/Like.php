<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    public function member()
    {
        return $this->hasOne('App\Models\User', 'id', 'member_id');
    }
}
