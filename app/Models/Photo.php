<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'sender_id', 'receiver_id', 'message',
    ];
    protected $appends = array('thumbnail');

    public function getImageAttribute($value)
    {
        return asset("/storage/photos/" . $this->user_id ."/". $value);
    }


    public function getThumbnailAttribute($value)
    {
        return str_replace("/photos/", "/thumbs/", $this->image);
    }
}
