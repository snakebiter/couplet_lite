<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatMessageCount extends Model
{
    protected $fillable = [
        'user_id', 'month'
    ];

}
