<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\PrivateMessageCount;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'firstname', 'lastname',
        'display_name', 'pref_location', 'pref_marital_status',
        'pref_min_age', 'pref_max_age', 'password',
        'email', 'gender', 'pref_gender',
        'birth_day', 'birth_month', 'birth_year',
        'birthdate', 'relationship_status', 'country',
        'city', 'lat', 'lng',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = array('age', 'is_premium', 'just_paid');

    public function getAgeAttribute($value)
    {
        $date = $this->birth_month .'/'.  $this->birth_day .'/' . $this->birth_year;
        $bdate = date('Y-m-d', strtotime($date));
        $age = date_diff(date_create($bdate), date_create('now'))->y;
        return $age;
    }

    public function getLanguagesAttribute($value)
    {
        if(isJson($value)) {
            return json_decode($value);
        }
        return [];
    }

    public function getEthnicitiesAttribute($value)
    {
        if(isJson($value)) {
            return json_decode($value);
        }
        return [];
    }

    public function getJustPaidAttribute($value)
    {
        if(!$this->premium_tmp)
            return false;

        $now = Carbon::now();
        $active_until = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->premium_tmp);
        if($active_until->gt($now)) {
            return true;
        }        

        return false;
    }    
	
	public function getIsPremiumAttribute($value)
    {
        if(!$this->active_until)
            return false;

        $now = Carbon::now();
        $active_until = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->active_until);
        if($active_until->gt($now)) {
            return true;
        }        

        return false;
    }

    public function chat_message_count()
    {
        $usage = ChatMessageCount::firstOrNew(array('user_id' => $this->id, 'month' => date('MY')));
        if($usage)
            return $usage->usage;
        return 0;
    }

    public function private_message_count()
    {
        $usage = PrivateMessageCount::firstOrNew(array('user_id' => $this->id, 'month' => date('MY')));
        if($usage)
            return $usage->usage;
        return 0;
    }

    public function getAvatarAttribute($value)
    {

        $url = asset("/images/avatar_male.png");
        if ($this->gender == 'F') {
            $url = asset("/images/avatar_female.png");
        }
        if ($value) {
            $url = asset('/storage/'.$value);
        }

        return $url;
    }

    public function photos()
    {
        return $this->hasMany('App\Models\Photo');
    }

    public function threads()
    {
        return $this->hasMany('App\Models\PrivateMessageThread', 'receiver_id');
    }

    public function liked()
    {
        return $this->hasMany('App\Models\Like', 'user_id');
    }

    public function likedYou()
    {
        return $this->hasMany('App\Models\Like', 'member_id');
    }

    public function interactions()
    {
        return $this->hasMany('App\Models\Interaction', 'user_id');
    }

}
