<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\PageTranslation;
use Auth;
use Mail;
use Config;
use Input;

class ContactController extends SiteController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request)
    {
		$data = [];
        return view('contact', $data);
    }
	
	public function postIndex(Request $request)
    {
		//send the email and redirect to success
		$post_vars = $request->input();
		
		Mail::send('emails.support', $post_vars, function ($message) {
            $message->from(Input::get('email_address'), 'Support center');
            $message->to(Config::get('mail.from.address'));
        });
		
        return redirect('contact')->with('success', 'sent');
    }

}
