<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Auth;
use Schema;
use Storage;
use Hash;
use Validator;
use Image;
use Input;
use App\Models\User;
use App\Models\Photo;
use App\Models\PrivateMessage;
use App\Models\PrivateMessageThread;
use App\Models\Like;
use App\Models\Interaction;

class UserController extends Controller
{
    
    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'full_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            'birth_day' => 'required',
            'birth_month' => 'required',
            'birth_year' => 'required',
            'pref_gender' => 'required',
            'gender' => 'required',
            'city' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['result'=>false, 'errors' => $validator->errors()], 502);
        }

    	$input = $request->all();        
    	$input['password'] = Hash::make($input['password']);
    	$input['name'] = $input['full_name'];
    	$input['birthdate'] = $input['birth_year'] . "-" . $input['birth_month'] . "-" . $input['birth_day'];
    	User::create($input);

        return response()->json(['result'=>true]);
    }

    public function getLogin(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');
		
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 502);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }

    public function getLogout(Request $request) {
        Auth::logout();
        unset($_COOKIE['couplet_token']);
        return response()->json(['success' => true]);
    }

    public function getMe(Request $request)
    {
       try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }
        
        // the token is valid and we have found the user via the sub claim
        return response()->json($user);
    }


    public function putMe(Request $request)
    {
        //get user and save all changes
        $user = Auth::user();
        $unfillable = ['id', 'email', 'created_at', 'updated_at'];
        $inputs = Input::all();

        $columns = Schema::getColumnListing('users');
        foreach($inputs as $field => $value) {
            if(is_array($value)){
                $value = json_encode($value);
            }
            if(in_array($field, $unfillable)) {
                continue;
            }

            if(in_array($field, $columns)) {
                $user->$field = $value;
            } else {
                Schema::table('users', function ($table) use($field) {
                    $table->string($field)->nullable();
                });
                $user->$field = $value;
            }

        }
        $user->save();

        // the token is valid and we have found the user via the sub claim
        return response()->json([ 'success' => true ]);
    }

    public function refresh(Request $request) {
        return response()->json(['success'=>true]); 
    }

    public function uploadAvatar(Request $request)
    {

        //get user and save all changes
        $user = Auth::user();
        #Image::make(Input::file('file'))->resize(256, 256)->save(storage_path('foo.jpg');
        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->input('base64')));
        $img = Image::make($data);
        $img->fit(256, 256, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        }, 'top');
        $data = $img->encode('png');

        Storage::disk('public')->put('avatars/'.$user->id.'.png', $data, 'public');
        $user->avatar = 'avatars/'.$user->id.'.png';
        $user->save();
        //dd($user);

        // the token is valid and we have found the user via the sub claim
        return response()->json(['success'=>true]);
    }

    public function uploadPhoto(Request $request)
    {
        
        $user = Auth::user();
        $photo_id = uniqid().'.png';
        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->input('base64')));
        Storage::disk('public')->put('photos/'.$user->id.'/'.$photo_id, $data, 'public');

        $img = Image::make($data);
        $img = $img->resize(160, 160, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        }, 'center');
        $img->resizeCanvas(160, 160);
        $data = $img->encode('png');

        Storage::disk('public')->put('thumbs/'.$user->id.'/'.$photo_id, $data, 'public');

        $photo = new Photo();
        $photo->image = $photo_id;
        $photo->user_id = $user->id;
        $photo->save();

        return response()->json(['success'=>true]);
    }

    public function matches(Request $request)
    {
        $user = Auth::user();
        $members_you_liked = Like::where('user_id', $user->id)->select('member_id')->get();
        $members_you_liked = array_pluck($members_you_liked, 'member_id');
        $members_who_liked_you = Like::where('member_id', $user->id)->select('user_id')->get();
        $members_who_liked_you = array_pluck($members_who_liked_you, 'user_id');
        $members = array_unique(array_intersect($members_you_liked, $members_who_liked_you));
        $matches = Like::where('user_id', $user->id)
                                ->whereIn('member_id', $members)
                                ->with('user')
                                ->with('member')
                                ->get();
                                
        return response()->json($matches);
    }

    public function liked(Request $request)
    {
        $user = Auth::user();
        $liked_members = Like::where('user_id', $user->id)
                                ->with('user')
                                ->with('member')
                                ->get();
        return response()->json($liked_members);
    }

    public function likedYou(Request $request)
    {
        $user = Auth::user();
        $liked_members = Like::where('member_id', $user->id)
                                ->with('user')
                                ->with('member')
                                ->get();
        return response()->json($liked_members);
    }

    public function interactions(Request $request)
    {
        $user = Auth::user();
        $interactions = Interaction::where('member_id', $user->id)
                        ->with('user')
                        ->with('member')
                        ->get();
        return response()->json($interactions);
    }

    public function getPhotos(Request $request)
    {
        $user = Auth::user();
        return response()->json($user->photos);
    }

    public function deletePhoto(Request $request, $id)
    {
        $photo = Photo::where('user_id', Auth::user()->id)->where('id', $id)->first();
        $photo->delete();
        return response()->json(['success'=>true]);
    }


    public function getLatestMessages(Request $request)
    {
        $user = Auth::user();

        $messages = PrivateMessage::where(['receiver_id' => $user->id])
                    ->orWhere(['sender_id' => $user->id])
                    ->with('sender')
                    ->with('receiver')
                    ->orderBy('id', 'desc')
                    ->paginate(90);

        return response()->json($messages);
    }


    public function getThreads(Request $request)
    {
        $user = Auth::user();
        $threads = PrivateMessageThread::where('receiver_id', $user->id)
                    ->with('sender')
                    ->with('receiver')
                    ->orderBy('updated_at', 'DESC')
                    ->get();

        /*$threads = PrivateMessage::where(['receiver_id' => $user->id])
            ->orWhere(['sender_id' => $user->id])
            ->with('sender')
            ->with('receiver')
            ->groupBy('receiver')
            ->orderBy('updated_at', 'DESC')
            ->get();*/

        return response()->json($threads);
    }

    public function getSentThreads(Request $request)
    {
        $user = Auth::user();
        $threads = PrivateMessageThread::where('sender_id', $user->id)
                    ->with('sender')
                    ->with('receiver')
                    ->orderBy('updated_at', 'DESC')
                    ->get();

        return response()->json($threads);
    }

    public function getThreadMessages(Request $request, $sender_id)
    {
        $user = Auth::user();
        $messages = PrivateMessage::where(['receiver_id' => $user->id, 'sender_id' => $sender_id])
                    ->orWhere(['sender_id' => $user->id, 'receiver_id' => $sender_id])
                    ->with('sender')
                    ->with('receiver')
                    ->get();

        return response()->json($messages);
    }

    public function markAsRead(Request $request, $sender_id)
    {
        $user = Auth::user();
        $messages = PrivateMessage::where('sender_id', $sender_id)
                    ->where('receiver_id', $user->id)
                    ->whereNull('read_at')
                    ->get();
        foreach($messages as $message) {
            $message->read_at = \Carbon\Carbon::now();
            $message->save();
        }

        $thread = PrivateMessageThread::where('receiver_id', $user->id)
                    ->where('sender_id', $sender_id)
                    ->with('sender')
                    ->with('receiver')
                    ->first();
        $thread->read_at =  \Carbon\Carbon::now();
        $thread->save();
        
        return response()->json(['success'=>true]);
    }

	public function unread(Request $request) {
        $user = Auth::user();
        $threads = PrivateMessage::where('receiver_id', $user->id)
                    ->whereNull('read_at')
                    ->count();

        return response()->json($threads);
    }

}