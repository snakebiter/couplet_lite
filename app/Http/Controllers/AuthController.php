<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends SiteController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function postLogin()
    {
        $data = [];
        return view('welcome', $data);
    }
}
