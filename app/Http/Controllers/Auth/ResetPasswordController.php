<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
class ResetPasswordController extends \App\Http\Controllers\SiteController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;
    protected $redirectTo = '/password/success';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		parent::__construct();
        #$this->middleware('guest');
    }
	
	public function showResetForm(Request $request, $token = null)
    {
		$data = [];
		$data['title'] = 'f';
        return view('auth.passwords.reset', $data)->with(
            ['token' => $token, 'email' => $request->email]
        );
    }	
	public function success(Request $request, $token = null)
    {
		$data = [];
		$data['title'] = 'f';
        return view('auth.passwords.reset', $data)->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}
