<?php

namespace App\Http\Controllers;
use Request;
use JWTAuth;
use JWTException;
use App\Models\User;

class AuthenticateController extends Controller
{
    public function login()
    {
        $credentials = Request::only('email', 'password');

        try {
            // verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 500);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
		
		$user = User::where('email', Request::get('email'))->first();
		if(!$user->is_admin == 1) {
			return response()->json(['error' => 'invalid_credentials'], 500);
		}

        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }
	
	public function details()
    {

		 try {
				if (! $user = JWTAuth::parseToken()->authenticate()) {
					return response()->json(['user_not_found'], 404);
				}

			} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

				return response()->json(['token_expired'], $e->getStatusCode());

			} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

				return response()->json(['token_invalid'], $e->getStatusCode());

			} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

				return response()->json(['token_absent'], $e->getStatusCode());

			}

			// the token is valid and we have found the user via the sub claim
			return response()->json($user);
    }
	
	public function refresh(Request $request){
		$oldtoken = JWTAuth::getToken();
		try {
			// attempt to refresh token for the user
			if (! $token = JWTAuth::parseToken('bearer','authorization',$oldtoken)->refresh()) {
				return response()>json(['error' => 'invalid_token'], 401);
			}
		} catch (JWTException $e) {
			return response()->json(['error' => 'could_not_refresh_token'], 500);
		}
		return response()->json(compact('token'));
    }
}