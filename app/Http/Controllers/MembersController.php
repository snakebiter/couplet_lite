<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Like;
use App\Models\Interaction;
use App\Models\PrivateMessage;
use App\Models\PrivateMessageThread;
use App\Models\PrivateMessageCount;
use Auth;
use Setting;

class MembersController extends SiteController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {

        $search_properties = json_decode(file_get_contents(public_path('data/search_properties.json')), true);
        $language_codes = json_decode(file_get_contents(public_path('data/language-codes.json')), true);
        $profile_options = json_decode(file_get_contents(public_path('data/profile-options.json')), true);

        $data = [
            'title' => __('Members'),
            'search_properties' => $search_properties,
            'profile_options'=> $profile_options,
            'language_codes'=> $language_codes,
        ];

        return view('members', $data);

    }   

	public function sendMessage(Request $request, $id) {
        
        $user = Auth::user();
        $member = User::find($id);

        $pm = new PrivateMessage();
        $pm->sender_id = $user->id;
        $pm->receiver_id = $member->id;
        $pm->message = $request->input('message');
        $pm->save();

        $pm = PrivateMessageThread::firstOrNew(array('sender_id' => $user->id, 'receiver_id' => $member->id));
        $pm->sender_id = $user->id;
        $pm->receiver_id = $member->id;
        $pm->message = $request->input('message');
        $pm->save();

        $interaction = Interaction::firstOrNew(array('user_id' => $user->id, 'member_id' => $member->id));
        $interaction->user_id = $user->id;
        $interaction->member_id = $member->id;
        $interaction->save();

        $usage = PrivateMessageCount::firstOrNew(array('user_id' => $user->id, 'month' => date('MY')));
        if ($usage->exists) {
            $usage->increment('usage');
        } else {
            $usage->usage = 1;
            $usage->save();
        }

        $data = [];
        $data['sender_id'] = $user->id;
        $data['sender_name'] = $user->display_name;
        $data['message'] = $request->input('message');
        event(new \App\Events\PrivateMessageWasReceived($member->id, $data));

        return response()->json(['success'=>true]);
    }

	public function liked(Request $request, $id) {
        
        $user = Auth::user();
        $member = User::find($id);

        $like = Like::where(array('user_id' => $user->id, 'member_id' => $member->id))->first();
        #dd($like);
        return response()->json(['success'=>true, 'is_liked' => (boolean) ($like)]);
    }

	public function like(Request $request, $id) {
        
        $user = Auth::user();
        $member = User::find($id);

        $like = Like::where(array('user_id' => $user->id, 'member_id' => $member->id))->first();
        if($like) {
            $like->delete();
        } else {
            $like = new Like();
            $like->user_id = $user->id;
            $like->member_id = $member->id;
            $like->save();
        }
        $like = Like::where(array('user_id' => $user->id, 'member_id' => $member->id))->first();

        $interaction = Interaction::firstOrNew(array('user_id' => $user->id, 'member_id' => $member->id));
        $interaction->user_id = $user->id;
        $interaction->member_id = $member->id;
        $interaction->save();

        return response()->json(['success'=>true, 'is_liked' => (boolean) ($like)]);
    }
	
	public function show(Request $request, $id) {
        return User::find($id);
    }

	public function getProfile(Request $request, $id) {

        $member = User::find($id);

        $user_properties = json_decode(file_get_contents(public_path('data/user_properties.json')), true);
        $search_properties = json_decode(file_get_contents(public_path('data/search_properties.json')), true);
        $language_codes = json_decode(file_get_contents(public_path('data/language-codes.json')), true);
        $profile_options = json_decode(file_get_contents(public_path('data/profile-options.json')), true);
        $country_codes = json_decode(file_get_contents(public_path('data/country-codes.json')), true);

        foreach($profile_options as $key => $value) {
            $profile_names[$key] = collect($value)->keyBy('field');
        }

        $language_dict = [];
        foreach($language_codes as $key => $value) {
            $tmp = [];
            $tmp['name'] = $value['English'];
            $tmp['field'] = $value['alpha2'];
            $language_dict[$value['alpha2']] = $tmp;
        }
        $profile_names['mother_tongue'] = $language_dict;

        $country_dict = [];
        foreach($country_codes as $key => $value) {
            $tmp = [];
            $tmp['name'] = $value['name'];
            $tmp['field'] = $value['alpha-2'];
            $country_dict[$tmp['field']] = $tmp;
        }
        $profile_names['country_living_in'] = $country_dict;
        $profile_names['country_grew_up_in'] = $country_dict;

        foreach($user_properties as $i => $user_property) {
           $user_property['value'] = $member[$user_property['field']];
            if (isset($profile_names[$user_property['field']] )) {
                if( @isset( $profile_names[$user_property['field']][$member[$user_property['field']]] )) {
                    $user_property['value'] = @$profile_names[$user_property['field']][$member[$user_property['field']]]['name'];
                } else {
                    $user_property['value'] = $member[$user_property['field']];
                }
            } else {
                $user_property['value'] = $member[$user_property['field']];
            }

            if($user_property['field'] == 'country_living_in' || $user_property['field'] == 'country_grew_up_in' ) {
                $user_property['value'] = @$country_dict[$member[$user_property['field']]]['name'];
            }

            if($user_property['field'] == 'height') {
                $user_property['value'] = (string) (int) $user_property['value'];
                if($user_property['value'] == '0') {
                    $user_property['value'] = '';
                }
            }
            if($user_property['field'] == 'ethnicities' && is_array($user_property['value'])) {
                $user_property['value'] = implode(", ", $user_property['value']);
            }
            $user_properties[$i] = $user_property;
        }

        $profile_properties = [];
        for($i = 1; $i <= 6; $i++) {
            $tmp = [];
            $tmp['title'] = Setting::get('question_'.$i);
            $tmp['field'] =  'question_'.$i;
            $profile_properties[$i] = $tmp;
        }

        $data = [
            'id' => $id,
            'user'=> $member,
            'profile_properties'=> $profile_properties,
            'user_properties'=> $user_properties,
        ];

        return view('profile', $data);
    }

	public function latest(Request $request, $limit = 4) {
        $users = User::whereNotNull('avatar')->where('is_admin', '!=', 1)->orderBy('created_at', 'DESC')->limit($limit);
        return $users->get();
    }

	public function index(Request $request)
    {
        $input = $request->all();

        $users = new User();
		$users = $users->where('is_admin', '!=', 1);
        foreach($input['params'] as $param) {
            
            $field = $param[0];
            $values = $param[1];
            $type = $param[2];
            
            if($type == 'contains') {
                $users = $users->where(function($q) use ($field, $values) {
                    foreach($values as $value) {
                        $q = $q->orWhere($field, 'like', '%'.$value.'%');
                    }
                });
            }
            
            if($type == 'in') {
                $users = $users->whereIn($field, $values);
            }
            
            if($type == 'eq') {
                $users = $users->where($field, $values);
            }
            
            if($type == 'gt') {
                $users = $users->where($field, '>', $values);
            }
            
            if($type == 'gte') {
                $users = $users->where($field, '>=', $values);
            }     

            if($type == 'lt') {
                $users = $users->where($field, '<', $values);
            }
            
            if($type == 'lte') {
                $users = $users->where($field, '<=', $values);
            }
            
        }
        $users = $users->orderBy('created_at', 'DESC');
        return $users->paginate($request->get('limit', 12));

    }

    public function getPhotos(Request $request, $id)
    {
        $user = User::find($id);
        return response()->json($user->photos);
    }
	
}
