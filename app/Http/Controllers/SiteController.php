<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Cookie;
use Auth;
use JWTAuth;
use Theme;
use Config;
use Cache;
use App\Models\Locale;
use App\Models\Page;
use App\Models\Setting;

use Gettext\Translator;
use Gettext\Translations;

class SiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		$current_time = \Carbon\Carbon::now()->toDateTimeString();
#dd($current_time);
		Theme::set(\Setting::get('theme'));
        $this->middleware(function ($request, $next) {
            
            $lang = \Request::input('lang');
            if($lang) {
                session(['lang' => $lang]);
            }
			
            $current_locale = session('lang',  Config::get( 'app.locale' ));
            $t = new Translator();
            #dd($current_locale);
            $json_language = [];
            if(file_exists(base_path('resources/views/'.Theme::get().'/po/'.$current_locale.'.json'))) {
                $t->loadTranslations(Translations::fromJsonDictionaryFile(base_path('resources/views/'.Theme::get().'/po/'.$current_locale.'.json')));
                $json_language = json_decode(file_get_contents(base_path('resources/views/'.Theme::get().'/po/'.$current_locale.'.json')));
            }
            if(file_exists(base_path('resources/po/'.$current_locale.'.json')))
                $t->loadTranslations(Translations::fromPoFile(base_path('resources/po/'.$current_locale.'.json')));
            $t->register();
            
            View::share('json_language', $json_language);
            View::share('token', @$_COOKIE['couplet_token']);
            View::share('language', $current_locale);
            return $next($request);
        });

        //let's get the pages
        $pages = Page::with('translations')->get();
        $routes = [];
        foreach($pages as $page) {
            foreach($page->translations as $translation) {
                if($translation->slug && $page->handler) {
                    $routes[$page->handler][$translation->locale] = $translation->slug;
                }
            }
        }
        View::share('page_routes', $routes);
        View::share('title', Config::get('name'));
        Cache::put('page_routes', $routes, 1);


        $locales = Locale::where('visible', 1)->orderBy('position')->get();
        $locales_array = [];
        foreach($locales as $locale) {
            $locales_array[$locale->en] = $locale->toArray();
        }
        View::share('locales', $locales);
        Cache::put('locales', $locales_array);

        $settings = Setting::pluck('value', 'key');
        View::share('settings', $settings->toArray());


    }

}
