<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\PageTranslation;
use Auth;

class PageController extends SiteController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPage(Request $request, $slug)
    {

		$page_translation = PageTranslation::where('slug', $slug)->where('visibility', 'VISIBLE')->first();

		if(!$page_translation) {
			abort(403);
		}
        
        $page = Page::find($page_translation->page_id);
        $data = [
            'title' => $page_translation->title,
            'page_translation' => $page_translation,
            'page'=> $page,
        ];

        return view('page', $data);

    }

    public function getPageTranslation(Request $request, $lang, $slug)
    {

		$page_translation = PageTranslation::where('slug', $slug)->where('visibility', 'VISIBLE')->where('locale', $lang)->first();

		if(!$page_translation) {
			abort(403);
		}
        
        $page = Page::find($page_translation->page_id);
        $data = [
            'title' => $page_translation->title,
            'page_translation' => $page_translation,
            'page'=> $page,
        ];

        return view('page', $data);

    }

}
