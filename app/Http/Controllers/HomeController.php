<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cookie;
use Setting;

class HomeController extends SiteController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $token = Cookie::get();
        if(isset($_COOKIE['couplet_token'])) {
            return redirect('members');
        }
        
        $data = [];
        return view('welcome', $data);
    }
}
