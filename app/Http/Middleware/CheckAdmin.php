<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use JWTAuth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$user = JWTAuth::parseToken()->authenticate();
		
		if (!$user->is_admin) {
			 abort(403, 'Access denied');
		}
		
        return $next($request);
    }
}
