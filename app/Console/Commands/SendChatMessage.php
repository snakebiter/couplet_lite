<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SendChatMessage extends Command
{
    protected $signature = 'chat:message {message}';

    protected $description = 'Send chat message.';

    public function handle()
    {
        // Fire off an event, just randomly grabbing the first user for now
        $user = \App\Models\User::find(8);
        $sender = \App\Models\User::find(5);
        $message = \App\Models\ChatMessage::create([
            'sender_id' => $sender->id,
            'message' => $this->argument('message')
        ]);

        //$pusher = new \Pusher( '44fc4ece6cb6526d5a51', 'd41ce2467541db0e9eaa', 244068, array( 'encrypted' => true ) );
        //$pusher->trigger( 'test_channel', 'my_event', 'hello world' );
        $data = [];
        $data['sender_id'] = $sender->id;
        $data['sender_name'] = $user->display_name;
        $data['message'] = $this->argument('message');
        event(new \App\Events\ChatMessageWasReceived($user->id, $data));
    }
}