<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File;

class CreateTranslations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $directory = resource_path('views/default');
        $translations = new \Gettext\Translations();
        
        $files = File::allFiles($directory);
        foreach ($files as $file) {
            if (strpos($file->getFilename(), 'blade') !== false) {
                \Gettext\Extractors\Blade::fromFile($file->getPathname(), $translations);
            }
        }
        
		$languages = ['en', 'fr'];
		foreach($languages as $language) {
		    if(file_exists($directory.'/po/'.$language.'.json')) {
				$translations_saved = \Gettext\Translations::fromJsonDictionaryFile($directory.'/po/'.$language.'.json');
				$translations->mergeWith($translations_saved);
			}

			\Gettext\Generators\JsonDictionary::$options['json'] = JSON_PRETTY_PRINT;
			$translations->toJsonDictionaryFile($directory.'/po/'.$language.'.json');	
		}


        #dd($translations);
        die();

    }
}
