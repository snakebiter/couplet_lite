<?php

function get_page($string, $lang) {
	$page_routes = Cache::get('page_routes');
	$locales = Cache::get('locales');
	if(count($locales) == 1) {
		return url('page/'.@$page_routes[$string][$lang]);
	}
	return url('page/'.$lang.'/'.@$page_routes[$string][$lang]);	
}

function microtime_diff($start, $end = null)
{
	if (!$end) {
		$end = microtime();
	}
	list($start_usec, $start_sec) = explode(" ", $start);
	list($end_usec, $end_sec) = explode(" ", $end);
	$diff_sec = intval($end_sec) - intval($start_sec);
	$diff_usec = floatval($end_usec) - floatval($start_usec);
	return floatval($diff_sec) + $diff_usec;
}

function isJson($string) {
 json_decode($string);
 return (json_last_error() == JSON_ERROR_NONE);
}