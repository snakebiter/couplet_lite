'use strict';

var couplet = new Couplet(site_url('/api/1/').replace(location.origin, ''));
var userToken = '';

function setMomentOffset(serverTime) {
  var offset = new Date(serverTime).getTime() - Date.now();
  moment.now = function() {
    return offset + Date.now();
  }
}
setMomentOffset($('body').data('time')*1000);

// Show the progress bar 
NProgress.start();

// Increase randomly
var interval = setInterval(function () { NProgress.inc(); }, 1000);

// Trigger finish when page fully loaded
$(window).on('load', function () {
  clearInterval(interval);
  NProgress.done();
});

// Trigger bar when exiting the page
jQuery(window).unload(function () {
  NProgress.start();
});

Opentip.styles.myErrorStyle = {
  // Make it look like the alert style. If you omit this, it will default to "standard"
  extends: "dark",
  // Tells the tooltip to be fixed and be attached to the trigger, which is the default target
  target: true,
  stem: true,
  showOn: "creation"
};

$(function () {

  if ($('.home-search #start_element:visible').length > 0) {
    var myOpentip = new Opentip("#start_element", { target: "#start_element", delay: 2, tipJoint: "top right", targetJoint: "left bottom", style: "myErrorStyle" });

    //myOpentip.show(); // Shows the tooltip immediately
    myOpentip.setContent("Start meeting people now!"); // Updates Opentips content
  }
});
/*
$(document).on('click', '.panel-heading span.icon_minim', function (e) {
  var $this = $(this);
  if (!$this.hasClass('panel-collapsed')) {
    $this.parents('.panel').find('.panel-body').slideUp();
    $this.addClass('panel-collapsed');
    $this.removeClass('glyphicon-minus').addClass('glyphicon-plus');
  } else {
    $this.parents('.panel').find('.panel-body').slideDown();
    $this.removeClass('panel-collapsed');
    $this.removeClass('glyphicon-plus').addClass('glyphicon-minus');
  }
});
$(document).on('focus', '.panel-footer input.chat_input', function (e) {
  var $this = $(this);
  if ($('#minim_chat_window').hasClass('panel-collapsed')) {
    $this.parents('.panel').find('.panel-body').slideDown();
    $('#minim_chat_window').removeClass('panel-collapsed');
    $('#minim_chat_window').removeClass('glyphicon-plus').addClass('glyphicon-minus');
  }
});*/
$(document).on('click', '#new_chat', function (e) {
  var size = $(".chat-window:last-child").css("margin-left");
  size_total = parseInt(size) + 400;
  alert(size_total);
  var clone = $("#chat_window_1").clone().appendTo(".container");
  clone.css("margin-left", size_total);
});

$('.page-transition').hide();

var NProgress_interval = null;


function redirect_url(href) {
  window.location.href = site_url(href);
}
function avatar_image(user) {
  var url = (''+user.avatar) + '?t=' + +new Date();
  return url;
}

function isPremium(user) {
  return user.is_premium;
}

function _calculateAgeFromDate(birth_year, birth_month, birth_day) {
  var birthday = moment().year(birth_year).month(birth_month).date(birth_day).toDate();
  var ageDifMs = Date.now() - birthday.getTime();
  var ageDate = new Date(ageDifMs); // miliseconds from epoch
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function _calculateAge(user) {
	try {
  return _calculateAgeFromDate(user.birth_year, user.birth_month, user.birth_day);
	} catch (e) {
		return 0;
	}
}
var socket = null;

var currentUser = couplet.User.current();
var auth_routes = [
  'account/'
];
if (!currentUser && window.Laravel.userToken) {
  couplet.User.become(window.Laravel.userToken).then(function (user) {
    // The current user is now set to user.
    console.log(user);
  }, function (error) {
    console.log('The token could not be validated.', error);
  });
}

if (!currentUser) {
  //check if in allowed paths
  var current_path = _.trimStart(window.location.pathname, '/');
  auth_routes.forEach(function (auth_route) {
    if (current_path.indexOf(auth_routes) >= 0) {
      redirect_url('/');
    }
  })
} else {
  /*socket = io.connect('/');
  socket.on("connect", function () {
    console.log("Connected!");
  });

  socket.on(currentUser.id, function (data) {

    console.log('socket', data);

    if (data.type == 'PRIVATE_MESSAGE') {
      toastr.info(data.message);
      setTimeout(function () {
        router.app.$dispatch('update-messages');
      }, 1000);

      couplet.User.current().fetch({
        success: function (user) {
          couplet.User.current();
          router.app.$dispatch('update-user');
          router.app.$dispatch('update-messages');
        },
        error: function (user, error) {
          console.log('currentUser.error');
        }
      });
    }

  });*/

  $('.navbar-brand').attr('href', '/members');
  //check if in allowed paths
  var current_path = _.trimStart(window.location.pathname, '/');
  if (current_path == '') {
    redirect_url('/members');
  }

  var duration = moment.duration(moment().diff(moment(currentUser.last_active, "DD/MM/YYYY HH:mm:ss")));
  var seconds = duration.asSeconds();
  console.log('duration member', seconds);

}

//get the data that we need


Vue.config.debug = false;

Vue.component('latest-members', {
  template: '#latest-members',
  data: function () {
    return {
      members: []
    }
  },
  ready: function () {
    this.getLatestMembers();
  },
  methods: {
    getLatestMembers: function () {

      var self = this;
      couplet.members.getLatestMembers(4).then(function(members){
          self.members = members;
      });

    },
    calculate_age: function (user) {
      return _calculateAge(user);
    },
    avatar_image: function (user) {
      return avatar_image(user);
    },
  }
});

var navbarVue = Vue.component('navbar', {
  template: '#navbar-template',
  props: ['currentUser', 'inbox'],
  data: function () {
    return {
      //unreadChatMessages: [],
      unreadMessages: []
    }
  },
  ready: function () {
    if (this.currentUser) {
      this.getUnreadMessages();
      //this.getUnreadChatMessages();
	  
	  var self = this;
		/*echo.channel('App.User.' + self.currentUser.id)
		.listen('PrivateMessageWasReceived', function (result) {
			var t = toastr.info(result.data.sender_name + " has sent you a new private message");
			self.$dispatch('update-messages');
			console.log(self.currentUser);
			self.getUnreadMessages();				
			$(t).click(function(){
				window.location.href = "/account/inbox/" + result.data.sender_id;
			});
		});*/
    }
  },
  methods: {
    getUnreadChatMessages: function () {
	  var self = this;
      couplet.chat.getUnreadMessages().then(function(messages){
          self.unreadChatMessages = messages;
      });
    },
    getUnreadMessages: function () {
	  var self = this;
      couplet.User.getUnreadMessages().then(function(messages){
          self.unreadMessages = messages;
      });
    },
    isPremium: function (user) {
      return isPremium(user);
    },
    logout: function (event) {
		couplet.User.logOut().then(() => {
			redirect_url('/');
			this.$dispatch('logout');
		});
    }
  },
  watch: {
    'inbox': function (val, oldVal) {
      console.log(oldVal + 'has been changed to ' + val + ' from outside.');
    }
  },
});

var accountFriendsVue = Vue.component('account-friends', {
  template: '#account-friends',
  props: ['currentUser'],
  data: function () {
    return {
      memberMatches: null,
      membersYouLiked: null,
      membersWhoLikedYou: null,
      membersWhoInteractedWithYou: null,
      settings: settings,
    }
  },
  ready: function () {
    this.getMembersYouLiked();
    this.getMembersWhoLikedYou();
    this.getMembersWhoInteractedWithYou();
  },
  methods: {
    deleteImage: function (galleryImage) {
      var self = this;
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(function (result) {
        console.log(result, galleryImage);

        galleryImage.destroy({
          success: function (myObject) {
            self.getImages();
          },
          error: function (myObject, error) {
            console.log(myObject, error);
            // The delete failed.
            // error is a Parse.Error with an error code and message.
          }
        });

      })
    },
    getMatches: function () {
      var self = this;
	  
	  
	  var self = this;
	  couplet.User.getMatches().then(function(memberMatches){
		  self.memberMatches = memberMatches;
	  });

      var members = [];
      self.membersYouLiked.forEach(function (member) {
        members.push(member.member);
      });

    },
    getMembersYouLiked: function () {
      var self = this;
	  couplet.User.getMembersYouLiked().then(function(membersLiked){
		  self.membersYouLiked = membersLiked;
		  self.getMatches();
	  });
    },
    getMembersWhoLikedYou: function () {
      var self = this;
	  couplet.User.getMembersWhoLikedYou().then(function(membersLiked){
		  self.membersWhoLikedYou = membersLiked;
	  });
    },
    getMembersWhoInteractedWithYou: function () {
      var self = this;
	  couplet.User.getMembersWhoInteractedWithYou().then(function(membersWhoInteractedWithYou){
		  self.membersWhoInteractedWithYou = membersWhoInteractedWithYou;
	  });
    },
    avatar_image: function (user) {
      return avatar_image(user);
    },
    isPremium() {
      return isPremium(currentUser);
    },
  }
});

var accountGalleryVue = Vue.component('account-gallery', {
  template: '#account-gallery',
  props: ['currentUser'],
  data: function () {
    return {
      galleryImages: [], // my list for the v-for
      uploadedFiles: [], // my list for the v-for
      fileProgress: 0, // global progress
      allFilesUploaded: false, // is everything done?
      isUploading: false // is everything done?
    }
  },
  ready: function () {
    this.getImages();
  },
  methods: {
    deleteImage: function (galleryImage) {
      var self = this;
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(function (result) {
        console.log(result, galleryImage);
			if(result) {
				couplet.User.deletePhoto(galleryImage.id).then(function(){
					self.getImages();
				})
			}

      })
    },
    getImages: function () {
      var self = this;
	  couplet.User.getPhotos().then(function(galleryImages){
		  self.galleryImages = null;
          self.galleryImages = galleryImages;
		            console.log(galleryImages, self.galleryImages, self.galleryImages.length);

	  })

    }
  },
  events: {
    onFileClick: function (file) {
      console.log('onFileClick', file);
    },
    onFileChange: function (file) {
      console.log('onFileChange', file);
      // here is where we update our view
      this.fileProgress = 0;
      this.allFilesUploaded = false;
    },
    beforeFileUpload: function (file) {
      // called when the upload handler is called
      this.isUploading = true;
      console.log('beforeFileUpload', file);
    },
    afterFileUpload: function (file) {
      // called after the xhr.send() at the end of the upload handler
      console.log('afterFileUpload', file);
    },
    onFileProgress: function (progress) {
      console.log('onFileProgress', progress);
      // update our progress bar
      this.fileProgress = progress.percent;
    },
    onFileUpload: function (file, res) {
      console.log('onFileUpload', file, res);
      // update our list
      this.uploadedFiles.push(file);
    },
    onFileError: function (file, res) {
      console.error('onFileError', file, res);
    },
    onAllFilesUploaded: function (files) {
      console.log('onAllFilesUploaded', files);
      // everything is done!
      this.allFilesUploaded = true;
      this.isUploading = false;

      this.getImages();
    }
  }
});

var accountSettingsVue = Vue.component('account-settings', {
  template: '#account-settings',
  props: ['currentUser'],
  data: function () {
    return {
      settings: settings,
      saving: false,
      change_password: false,
      country_codes: [],
      user: currentUser,
      days: _.range(1, 31),
      years: _.range(new Date().getFullYear() - 100, new Date().getFullYear() - 18).reverse(),
    }
  },
  ready: function () {
    var self = this;
    $.getJSON(asset_url('data/country-codes.json'), function (data) {
      self.country_codes = data;
    });

    var placesAutocomplete = places({
      container: document.querySelector('#my-city')
    });
    placesAutocomplete.on('change', e => {
      console.log(e)
      self.$set('user.city', e.suggestion.name);
      self.$set('user.country', e.suggestion.countryCode.toUpperCase());
      self.$set('user.lat', e.suggestion.latlng.lat);
      self.$set('user.lng', e.suggestion.latlng.lng);
    });

  },

  methods: {
    toggle_password() {
      this.change_password = !this.change_password;
    },
    isPremium() {
      return isPremium(currentUser);
    },
    updateForm: function (event) {
      NProgress.start();
      NProgress_interval = setInterval(function () { NProgress.inc(); }, 1000);
      self = this;
      this.saving = true;

      var fill = {
        gender: this.user.gender,
        firstname: this.user.first_name,
        lastname: this.user.last_name,
        display_name: this.user.display_name,
        email_permission: this.user.email_permission,
        birth_day: this.user.birth_day,
        birth_month: this.user.birth_month,
        birth_year: this.user.birth_year,
        birthdate: moment().year(this.user.birth_year).month(this.user.birth_month).date(this.user.birth_day).format("YYYY-MM-DD HH:mm:ss"),

        city: this.user.city,
        country: this.user.country,
        lat: this.user.lat,
        lng: this.user.lng,
        protect_avatar: this.user.protect_avatar
      };
      if (this.user.password) {
        if (this.user.password == this.user.password_confirm) {
          fill['password'] = this.user.password;
        } else {
          alert("ERROR");
        }
      }
      console.log(this.currentUser);
	  
	  
		couplet.User.save(fill).then(function (currentUser) {
			setTimeout(function () {
				clearInterval(NProgress_interval);
				NProgress.done();
				toastr.info("Saved!");
				self.saving = false;
			}, 100);
			console.log(currentUser);
			self.change_password = false;
			couplet.User.fetch().then(function (currentUser) {
				self.currentUser = couplet.User.current()
			});
		}, function (error) {
			console.log(error);
			self.saving = false;
			NProgress.done();
		});

    }
  }
});

var user_properties = [
  {
    field: 'question_1',
    title: 'My self-summary',
    input: 'textarea',
    placeholder: '',
  },
  {
    field: 'question_2',
    title: 'What I’m doing with my life',
    input: 'textarea',
    placeholder: '',
  },
  {
    field: 'question_3',
    title: 'I’m really good at',
    input: 'textarea',
    placeholder: '',
  },
  {
    field: 'question_4',
    title: 'Favorite books, movies, shows, music, and food',
    input: 'textarea',
    placeholder: '',
  },
  {
    field: 'question_5',
    title: 'The six things I could never do without',
    input: 'textarea',
    placeholder: '',
  },
  {
    field: 'question_6',
    title: 'I spend a lot of time thinking about',
    input: 'textarea',
    placeholder: '',
  },
  { field: 'height', input: 'text', placeholder: '', title: 'Height (cm)' },
  { field: 'education', input: 'text', placeholder: '', title: 'Education' },
  { field: 'profession', input: 'text', placeholder: '', title: 'Profession' },
  { field: 'relationship_status', input: 'text', placeholder: '', title: 'Marital Status' },
  { field: 'religion', input: 'text', placeholder: '', title: 'Religion' },
  { field: 'mother_tongue', input: 'text', placeholder: '', title: 'Mother Tongue' },
  { field: 'country_living_in', input: 'text', placeholder: '', title: 'Country living in' },
  { field: 'country_grew_up_in', input: 'text', placeholder: '', title: 'Country grew up in' },
  { field: 'drinking', input: 'text', placeholder: '', title: 'Drinking' },
  { field: 'drugs', input: 'text', placeholder: '', title: 'Drugs' },
  { field: 'smoker', input: 'text', placeholder: '', title: 'Smoking' },
  { field: 'diet', input: 'text', placeholder: '', title: 'Eating habits' },
  { field: 'body_type', input: 'text', placeholder: '', title: 'Body type' },
  { field: 'ethnicities', input: 'text', placeholder: '', title: 'Ethnicity' },
];

/*var registration = new Vue({
  el: '#registration-form',*/
var registrationVue = Vue.component('registration-form', {
  template: '#registration-form',
  props: ['currentUser'],
  data: function () {
    return {
      time: +new Date,
      saving: false,
      user: {},
      error: '',
      days: _.range(1, 31),
      years: _.range(new Date().getFullYear() - 100, new Date().getFullYear() - 18).reverse()
    }
  },
  ready: function () {
    this.init();
  },
  methods: {
    init: function () {
      var self = this;

	  	var placesAutocomplete = places({
		  container: document.querySelector('#searchTextField')
		});
		placesAutocomplete.on('change', e => {
		  console.log(e)
		  self.$set('user.city', e.suggestion.name);
		  self.$set('user.country', e.suggestion.countryCode.toUpperCase());
		  self.$set('user.lat', e.suggestion.latlng.lat);
		  self.$set('user.lng', e.suggestion.latlng.lng);
		});
		
    },
    updateDisplayName: function () {
      console.log(this.user.display_name, this.user.email.match(/^([^@]*)@/)[1]);
      if (this.user.display_name == undefined) {
        console.log(this.user.display_name);
        this.user.display_name = this.user.email.match(/^([^@]*)@/)[1];
        this.user = Object.assign({}, this.user, { display_name: this.user.display_name })


      }
    },
    register: function () {
      var self = this;
      self.saving = true;

      var user = {};
      user["username"] = self.user.email.toLowerCase();
      user["full_name"] = self.user.full_name;

      var name = NameParse.parse(self.user.full_name);
      user["first_name"] = name.firstName;
      user["last_name"] = name.lastName;
      user["display_name"] = name.firstName;
      user["pref_location"] = 'anywhere';
      user["pref_marital_status"] = 'single';


      user["password"] = self.user.password;
      user["email"] = self.user.email;
      user["gender"] = self.user.gender;
      user["pref_gender"] = self.user.pref_gender;
      user["birth_day"] = self.user.birth_day;
      user["birth_month"] = self.user.birth_month;
      user["birth_year"] = self.user.birth_year;
	  try {
		user["birthdate"] = moment().year(self.user.birth_year).month(self.user.birth_month).date(self.user.birth_day).toDate();

		var age = _calculateAgeFromDate(self.user.birth_year, self.user.birth_month, self.user.birth_day);
		user["pref_min_age"] = age - 5;
		user["pref_max_age"] = age + 9;
	  } catch(e) {
		  
	  }

      user["relationship_status"] = 'single';

      user["country"] = self.user.country;
      user["city"] = self.user.city;
      user["lat"] = self.user.lat;
      user["lng"] = self.user.lng;

      couplet.User.signUp(user).then(function() {
		  couplet.User.logIn(self.user.email, self.user.password).then(function (user) {
              console.log(user);
              self.saving = false;
              self.$dispatch('login');
			  //window.location.href = site_url('account/profile');
          }, function (result) {

          });
	  }, function(result) {
		self.saving = false;
        self.error = __('error_' + Object.keys(result.errors)[0]);
	  });
	  
    }
  }

})
Vue.component('forgot-modal', {
  template: '#forgot-modal',
  props: ['currentUser'],
  data: function () {
    return {
      time: +new Date,
      saving: false,
      error: '',
      success: '',
    }
  },
  methods: {
    submit: function (email) {
      var self = this;
      self.saving = true;
      self.error = '';
      self.success = '';
	  
	  couplet.User.requestPasswordReset(email).then(function() {
			self.saving = false;
          self.success = "Password reset request was sent successfully";
	  }, function(error) {
          self.saving = false;
          self.error = error.message;
	  });
	  

    }
  }

})
Vue.component('message-modal', {
  template: '#message-modal',
  props: ['currentUser'],
  data: function () {
    return {
      time: +new Date,
      profileUser: false,
      saving: false,
      message: '',
      error: '',
    }
  },
  ready: function () {
    this.currentUser = currentUser;
    var self = this;
	
	couplet.members.get($('#profile').data('member')).then(function(user) {
		console.log('profileUser', self.currentUser);
		self.profileUser = user;
	})

  },
  methods: {
    submit: function (message) {


      var self = this;
      self.saving = true;


		couplet.members.sendPrivateMessage(message, self.profileUser.id).then(function (result) {
			self.saving = false;
			if ($('#myModalMessage:visible').length) {
				$('#myModalMessage').modal('hide');
			}
			self.message = '';
			toastr.info("Succcessfully sent");
		}, function (error) {
			  self.saving = false;
			  console.log(error);

			  if (error.message == 'LIMIT_EXCEEDED') {
				swal({
				  title: 'Exceeded monthly limit',
				  text: "Would you like to upgrade to a premium account?",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, go ahead!'
				}).then(function (result) {
				  console.log(result);
				  if (result) {
					window.location.href = "/account/dashboard"
				  }
				})
			  } else if (error.error == 'token_not_provided') {
				swal("Please make sure you're logged in to send a message.");
			  } else {
				swal('Failed to create new object, with error code: ' + error.message);
			  }
		});
	
    }
  }

})
Vue.component('login-modal', {
  template: '#login-modal',
  props: ['currentUser'],
  data: function () {
    return {
      time: +new Date,
      saving: false,
      error: '',
      email: '',
      password: '',
    }
  },
  methods: {
    login: function (username, password) {
      var self = this;
      self.saving = true;

		couplet.User.logIn(username, password)
			.then(function (user) {
				console.log('user', user);
				// Do stuff after successful login.
				self.saving = false;
				self.$dispatch('login');
			}, function (error) {
				// The login failed. Check error to see why.
				console.log(error);
				self.saving = false;
				self.error = __(error.error);
			}
		);

    }
  }

})
Vue.component('registration-modal', {
  template: '#registration-modal',
  props: ['currentUser'],
  data: function () {
    return {
      time: +new Date,
      saving: false,
      error: '',
      email: '',
      password: '',
    }
  },
  methods: {

  }

})

function toDataUrl(src, callback, outputFormat) {
  var img = new Image();
  img.crossOrigin = 'Anonymous';
  img.onload = function () {
    var canvas = document.createElement('CANVAS');
    var ctx = canvas.getContext('2d');
    var dataURL;
    canvas.height = this.height;
    canvas.width = this.width;
    ctx.drawImage(this, 0, 0);
    dataURL = canvas.toDataURL(outputFormat);
    callback(dataURL);
  };
  img.src = src;
  if (img.complete || img.complete === undefined) {
    img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
    img.src = src;
  }
}

Vue.component('account-sidebar', {
  template: '#account-sidebar',
  props: ['currentUser'],
  inherit: true,
  data: function () {
    return {
      time: +new Date,
      saving: false,
      settings: settings,
    }
  },
  methods: {
    avatar_image: function (user) {
      return avatar_image(user);
    },
    onFileChange: function (e) {
      var self = this;
      var files = e.target.files || e.dataTransfer.files;
      if (!files.length) return;

      var file = files[0];
      var name = this.currentUser.id + '_avatar.' + file.name.split('.').pop();

      loadImage(
        file,
        function (img) {
			
			couplet.User.uploadAvatar({ base64: img.toDataURL("image/png") }).then(function (result) {
				 self.currentUser = null;
			couplet.User.fetch().then(function (currentUser) {
				self.currentUser = couplet.User.current()
				self.$dispatch('update-user');

                  self.time = +new Date;
			});
                  
				console.log(result);
			}, function (error) {
              alert(error);
            });
        },
        { maxWidth: 300, maxHeight: 300, canvas: true } // Options 
      );




    }

  }

})

Vue.component('v-select', VueSelect.VueSelect);
var accountDashboardVue = Vue.component('account-dashboard', {
  template: '#account-dashboard',
  props: ['currentUser'],
  data: function () {
    return {
      user: currentUser,
      settings: settings,
      interval: 'price_30',
	  price: settings['price_30']
    }
  },
  ready: function () {
	var self = this;	
	window.location.href = site_url("/account/profile");
  }
});
var accountProfileVue = Vue.component('account-profile', {
  template: '#account-profile',
  props: ['currentUser'],
  data: function () {
    return {
      saving: false,
      user: currentUser,
      user_properties: user_properties,
      settings: settings,
      country_codes: [],
      language_codes: [],
      profile_names: {},
      selected: null,
      profile_options: [],
    }
  },
  init: function () {
    var self = this;
    $.getJSON(asset_url('data/country-codes.json'), function (data) {
      self.country_codes = data;
    });
    $.getJSON(asset_url('data/language-codes.json'), function (data) {
      self.language_codes = data;
    });
    $.getJSON(asset_url('data/profile-options.json'), function (data) {
      self.profile_options = data;
      self.profile_names = {};

      _.forEach(data, function (value, key) {
        self.profile_names[key] = _.keyBy(value, 'field');
      });

    });
  },
  ready: function () {
    var user = currentUser;
    if (user.ethnicities instanceof Array === false) {
      user.ethnicities = [];
    }
    this.user = user;
  },
  methods: {
    country_name(code) {
      var country = _.find(this.country_codes, { 'alpha-2': code });
      return (country) ? country.name : '';
    },
    language_name(code) {
      var language = _.find(this.language_codes, { 'alpha2': code });
      return (language) ? language.English : '';
    },
    updateForm: function (event) {

      var self = this;
      var fill = {};
      this.saving = true;
      var fields = ["summary", "life", "pref_location", "pref_marital_status", "pref_new_friends", "pref_long_term", "orientation", "ethnicities", "pref_min_age", "pref_max_age", "pref_gender", "pref_casual_sex", "pref_short_term", "relationship_status", "height", "diet", "smoking", "drinking", "drugs", "religion", "education", "offspring", "has_cat", "has_dog", "profession", "ethnicities", "languages", "why_message_me"];

      user_properties.forEach(function (user_property) {
        if (self.user[user_property.field] !== undefined)
          fill[user_property.field] = self.user[user_property.field];
      });

      fields.forEach(function (field) {
        if (self.user[field] !== undefined) {
          fill[field] = self.user[field];
        }
      });

      if (fill["pref_min_age"]) {
        fill["pref_min_age"] = parseInt(fill["pref_min_age"]);
      }
      if (fill["pref_max_age"]) {
        fill["pref_max_age"] = parseInt(fill["pref_max_age"]);
      }
      if (fill["height"]) {
        fill["height"] = parseInt(fill["height"]);
      }

      console.log('updateForm', fields, fill);
      NProgress.start();
      NProgress_interval = setInterval(function () { NProgress.inc(); }, 1000);
	  
		console.log('fill', fill);
	  
		couplet.User.save(fill).then(function (currentUser) {
				console.log('currentUser', currentUser);
			  couplet.User.fetch();

			  if ($('#mySearch:visible').length) {
				  $('#mySearch').modal('hide');
			  }

			  if ($('#mySettings:visible').length) {
				  $('#mySettings').modal('hide');
			  }

		setTimeout(function () {
			clearInterval(NProgress_interval);
			  NProgress.done();
			  toastr.info("Saved!");
			  self.saving = false;
			}, 1000);
			self.$dispatch('update-user');
		}, function (error) {
			self.saving = false;
		  alert(error);
			NProgress.done();
		});
	 

    }
  }
});


var search_properties = [
  { field: 'gender', input: 'text', template: false, title: 'Gender' },
  { field: 'age_range', input: 'text', template: false, title: 'Age range' },
  { field: 'relationship_status', input: 'text', template: false, title: 'Marital Status' },
  { field: 'religion', input: 'text', template: false, title: 'Religion' },
  { field: 'mother_tongue', input: 'text', template: false, title: 'Mother Tongue' },
  { field: 'country_living_in', input: 'text', template: false, title: 'Country living in' },
  { field: 'diet', input: 'text', template: false, title: 'Eating habits' },
  { field: 'drinking', input: 'text', template: false, title: 'Drinking' },
  { field: 'drugs', input: 'text', template: false, title: 'Drugs' },
  { field: 'smoker', input: 'text', template: false, title: 'Smoking' },
  { field: 'body_type', input: 'text', template: false, title: 'Body type' },
  { field: 'ethnicities', input: 'text', template: false, title: 'Ethnicity' },
  { field: 'height', input: 'text', template: false, title: 'Height (cm)' },
];

var membersVm = Vue.component('browse-members', {
  template: '#browse-members',
  props: ['currentUser'],
  components: {
    VueSelect
  },
  data: function () {
    return {
      page: 1,
      total: 1,
      total_pages: 1,
      count: "",
      members: null,
      profile_options: [],
      language_codes: [],
      country_codes: [],
      starting: true,
      loading: false,
      search_properties: search_properties,
      params: {
        body_type: [],
        relationship_status: [],
        religion: [],
        mother_tongue: null,
        country_living_in: null,
        diet: [],
        drinking: [],
        drugs: [],
        smoker: [],
        ethnicities: [],
        height: null,
      },
      any: {},
      selected: null,
    }
  },
  init: function () {
    var self = this;
    $.getJSON(asset_url('data/country-codes.json'), function (data) {
      self.country_codes = data;
    });
    $.getJSON(asset_url('data/language-codes.json'), function (data) {
      self.language_codes = data;
    });
    $.getJSON(asset_url('data/profile-options.json'), function (data) {
      self.profile_options = data;
      self.profile_names = {};

      _.forEach(data, function (value, key) {
        self.profile_names[key] = _.keyBy(value, 'field');
      });
    });
  },
  ready: function () {
    this.set_search_params();
    this.search();
    this.count_members();
    this.starting = false;

  },
  watch: {
    params: {
      handler: function (val, oldVal) {
        this.search();
      },
      deep: true
    }
  },
  methods: {

    calculate_age: function (user) {
      if(user)
        return _calculateAge(user);
      else
        return "";
    },
    avatar_image: function (user) {
      return avatar_image(user);
    },
    set_search_params: function () {
      this.selected = [];
    },

    count_members: function () {
      var self = this;
    },

    clearParams: function (field) {
      console.log('clearParams', this.any[field]);
      //if has -1
      if (this.any[field] !== undefined && this.any[field]) {
        this.params[field] = [];
      }
    },

    clearAny: function (field) {
      console.log('clearParams', this.any[field]);
      //if has -1
      /*if(_.includes(this.params[field], '-1')) {
          this.params[field] = _.without(this.params[field], '-1');
      }*/
      if (this.any[field] !== undefined && this.any[field]) {
        this.any[field] = false;
      }
    },

    resetSearch: function () {
      this.params = {
        body_type: [],
        relationship_status: [],
        religion: [],
        mother_tongue: null,
        country_living_in: null,
        diet: [],
        drinking: [],
        drugs: [],
        smoker: [],
        ethnicities: [],
        height: null,
      }
    },

    previousPage: function () {
      var page = this.page - 1;
      if (page >= 0) {
        this.search(page);
      }
    },

    nextPage: function () {
      var page = this.page + 1;
      if (page <= this.total_pages) {
        this.search(page);
      }
    },

    search: function (page) {
      page = (page !== undefined) ? page : 1;
      var self = this;
      self.loading = true;
      console.log('search', self.params);

      var d = new Date();
      var current_year = d.getFullYear();

      var query = new couplet.Query();
      if (self.params.gender && self.params.gender.length) {
        query.equalTo('gender', self.params.gender);
      }
      if (self.params.relationship_status && self.params.relationship_status.length) {
        query.containedIn('relationship_status', self.params.relationship_status);
      }
      if (self.params.religion && self.params.religion.length) {
        query.containedIn('religion', self.params.religion);
      }
      if (self.params.mother_tongue) {
        query.equalTo('mother_tongue', self.params.mother_tongue);
      }
      if (self.params.languages && self.params.languages.length) {
        query.containsAll('languages', _.map(self.params.languages, 'alpha2'));
      }
      if (self.params.country_living_in) {
        query.equalTo('country', self.params.country_living_in);
      }
      if (self.params.diet.length) {
        query.containedIn('diet', self.params.diet);
      }
      if (self.params.drinking.length) {
        query.containedIn('drinking', self.params.drinking);
      }
      if (self.params.drugs.length) {
        query.containedIn('drugs', self.params.drugs);
      }
      if (self.params.smoker.length) {
        query.containedIn('smoker', self.params.smoker);
      }
      if (self.params.body_type.length) {
        query.containedIn('body_type', self.params.body_type);
      }
      if (self.params.ethnicities.length) {
        query.containsAll('ethnicities', self.params.ethnicities);
      }
      if (self.params.height_min) {
        query.greaterThanOrEqualTo('height', self.params.height_min);
      }
      if (self.params.height_max) {
        query.lessThanOrEqualTo('height', self.params.height_max);
      }
      if (self.params.age_min) {
        var startdate = moment();
        startdate.subtract(parseInt(self.params.age_min), 'y');
        console.log('age_min', startdate);
        query.lessThanOrEqualTo('birthdate', startdate.toDate());
      }
      if (self.params.age_max) {
        var startdate = moment();
        startdate.subtract(parseInt(self.params.age_max) + 1, 'y');
        console.log('age_max', startdate);
        query.greaterThanOrEqualTo('birthdate', startdate.toDate());
      }
	  
	  var params = query.getParams();
	  console.log('params', params);
	  couplet.members.setQuery(params);
	  couplet.members.setLimit(12);
	  couplet.members.setPage(page);
	  couplet.members.find(params).then(function (results) {
		console.log('results', results);
		self.members = null;
		self.members = results.data;
		self.loading = false;
		self.page = page;
		self.total_pages = results.last_page;
		$('.starting').hide();
      }, function (error) {
        self.loading = false;
        console.log("Error: " + error.code + " " + error.message);
      });


    }
  }

});

var Events = new Vue({});
if(document.getElementById('profile')) {
new Vue({
  el: '#profile',
  data: function () {
    return {
      galleryImages: null,
      profileIsLiked: false,
      currentUserIsLiked: false,
      currentUser: null,
      profileUser: null,
      settings: settings
    }
  },
  init: function () {

  },
  ready: function () {
    this.currentUser = couplet.User.current();
    var self = this;
    if (currentUser) {
		couplet.members.get($('#profile').data('member')).then(function(user) {
			self.profileUser = user;
			console.log(self.profileUser);
			self.isLiked();
			//self.isCurrentUserLiked();
		})
    }
  },
  methods: {
    isPremium() {
      return isPremium(currentUser);
    },
    startChat: function () {
      var self = this;
      Events.$emit('chatEventFired');
    },
    isCurrentUserLiked: function () {
	  		var self = this;
	  	couplet.members.isLiked(self.currentUser.id).then(function (result) {
			self.currentUserIsLiked = result['is_liked'];
		});
    },
	isLiked: function () {
		var self = this;
	  	couplet.members.isLiked(self.profileUser.id).then(function (result) {
			self.profileIsLiked = result['is_liked'];
			self.currentUserIsLiked = result['is_liked'];
		});
    },
    likeMember: function () {
		var self = this;
		couplet.members.like(self.profileUser.id).then(function (result) {
			if(result['is_liked']) {
				toastr.info('Liked');
				self.profileIsLiked = true;
			} else {
				toastr.info('Unliked');
				self.profileIsLiked = false;
			}
		}, function (error) {
			self.loading = false;
			console.log("Error: " + error.code + " " + error.message);
		});
    },
    getImages: function () {
      var self = this;
	  if (self.galleryImages == null && self.currentUser) {
		  couplet.members.getPhotos(self.profileUser.id).then(function(galleryImages) {
				self.galleryImages = null;
				self.galleryImages = galleryImages;
		  });
	  }
    }
  }
});
}

var accountInboxVue = Vue.component('account-inbox', {
  template: '#account-inbox',
  props: ['currentUser'],
  data: function () {
    return {
      threads: null,
      sent_threads: null,
      history: null,
      mode: 'all',
      refreshing: false
    }
  },
  ready: function () {
    this.getThreads();
    this.getLatestMessages();
  },
  methods: {
    changeMode: function (mode) {
      this.mode = mode;
      if (mode == 'sent') {
        this.getSentTheads();
      }
      if (mode == 'all') {
        this.getLatestMessages();
      }
    },
    refresh: function () {
      var self = this;
      self.refreshing = true;
	  
	  	couplet.User.getThreads().then(function(threads){
		      self.threads = threads;
          self.refreshing = false;
	    });
      couplet.User.getLatestMessages()

    },
    isRead(thread) {
      console.log(thread.get('readAt'), thread.get('postedAt'))
      if (thread.get('readAt') !== undefined && thread.get('readAt').getTime() > thread.get('postedAt').getTime()) {
        return true;
      } else {
        return false;
      }
    },
    getSentTheads: function () {
      var self = this;
	  
	  
		couplet.User.getSentThreads().then(function(threads){
			self.sent_threads = threads;
			self.refreshing = false;
		})
    },
    getThreads: function () {
      var self = this;
	  
		couplet.User.getThreads().then(function(threads){
		 self.threads = threads;

	  })
	  
    },
    getLatestMessages: function () {
      var self = this;
	  
      couplet.User.getLatestMessages().then(function(history){
      self.history = history.data;
      })
	  
    }
  },
  events: {
    'update-inbox': function () {
		this.getThreads();
    }
  }
});

var accountInboxThreadVue = Vue.component('inbox-thread', {
  template: '#inbox-thread',
  props: ['currentUser'],
  data: function () {
    return {
      messages: null,
      member: null,
      message: '',
    }
  },
  ready: function () {
    var self = this;
	couplet.members.get(this.$route.params.user_id).then(function(user){
		self.member = user;
		self.getMessages();
	});
  },
  methods: {
    avatar_image: function (user) {
      return avatar_image(user);
    },
    submit: function (message) {

      var self = this;
      self.saving = true;
      self.currentUser = couplet.User.current();
	  
	  couplet.members.sendPrivateMessage(message, self.member.id).then(function(result) {
		  self.saving = false;
          if ($('#myModalMessage:visible').length) {
            $('#myModalMessage').modal('hide');
          }
          self.message = '';
          self.getMessages();
	  }, function(error) {
		  self.saving = false;
          if (error.message == 'LIMIT_EXCEEDED') {
            swal({
              title: 'Exceeded monthly limit',
              text: "Would you like to upgrade to a premium account?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, go ahead!'
            }).then(function (result) {
              console.log(result);
              if (result) {
                window.location.href = "/account/dashboard"
              }
            })
          } else {
            swal('Failed to create new object, with error code: ' + error.message);
          }
	  });

    },
    markAsRead: function (member) {
      var self = this;
      //console.log('this.$parent.threads', this.$parent);
	  couplet.members.markPrivateMessageAsRead(member.id).then(function() {
		  self.$dispatch('update-messages');
	  });
    },
    getMessages: function () {
		var self = this;
		couplet.User.getThreadMessages(self.member.id).then(function(messages){
			self.messages = messages;
			self.markAsRead(self.member);
		});
    }
  }
});

var chatBox = Vue.component('chat-box', {
  template: '#chat-box',
  props: ['currentUser'],
  data: function () {
    return {
      messages: [],
      message: "",
      member: null,
      contactList: [],
      currentView: 'list',
      show_box: false,
      chat_on: false,
      chat_count: 0,
      blink: false
    }
  },
  ready: function () {
    var self = this;

    self.currentView = 'list';
    self.currentUser = couplet.User.current();
	
    if (currentUser) {
		self.getContacts();	
      self.getUnreadChatMessages();
      Events.$on('chatEventFired', () => {
        self.showChatBox();
      });
	 if($('#profile').data('member')) {
		couplet.members.get($('#profile').data('member')).then(function(user) {
			self.member = user;
          self.chat_on = true;
          self.getMessages();
		  self.currentView = 'chat';
		});
	 }
/*
		echo.channel('App.User.' + self.currentUser.id)
			.listen('ChatMessageWasReceived', function (result) {
				console.log('ChatMessageWasReceived', result);
				console.log('ChatMessageWasReceived', result.data.sender_id);
				toastr.info(result.data.sender_name + " has sent you a new message.");
				
					if(self.member && result.data.sender_id == self.member.id) {
						self.getMessages();
					} else {
											
						couplet.members.get(result.data.sender_id).then(function(user) {
							
							if(self.currentView == 'list' || self.show_box == 0) {

								self.member = user;
								self.chat_on = true;
								self.getMessages();
								self.currentView = 'chat';
								self.showChatBox();
			
							} else {
								self.getMessages();
							}

						});
					}
				
			});
*/
      /*
        var query = new Parse.Query('Chat');
        query.equalTo("receiver", currentUser);
        //query.include("sender");
        var subscription = query.subscribe();
        subscription.on('open', () => {
          console.log('subscription opened');
        });
        subscription.on('update', (object) => {
          console.log('object updated', object);
          self.getContacts(object);
        });
        subscription.on('create', (object) => {
          console.log('object created', object);
          self.getContacts(object);
        });
        subscription.on('error', (object) => {
          console.log('error created');
        });
        subscription.on('enter', (object) => {
          console.log('enter created');
        });*/
    }
  },
  methods: {
    getUnreadChatMessages: function () {
		var self = this;
		couplet.chat.getUnreadMessages().then(function(chat_count) {
			self.chat_count = chat_count;
		});
    },
    avatar_image: function (user) {
      return avatar_image(user);
    },
    showChatBox: function () {
      this.show_box = true;
      if (this.currentView == 'chat') {
        setTimeout(function () {
          $(".msg_container_base").animate({ scrollTop: $('.msg_container_base').prop("scrollHeight") }, 100);
        }, 300)
      }
    },
    hideChatBox: function () {
      this.show_box = false;
    },
    refreshChat: function () {
      this.getMessages();
      toastr.info("Refreshing chat...");
    },
	scrollDown: function () {
      	  			  $(".msg_container_base").animate({ scrollTop: $('.msg_container_base').prop("scrollHeight") }, 100);
			  setTimeout(function () {
				$(".msg_container_base").animate({ scrollTop: $('.msg_container_base').prop("scrollHeight") }, 100);
			  }, 300)
    },
    submit: function (message) {
      if (this.saving) {
        return;
      }

      var self = this;
      self.saving = true;
	  self.messages.push({id: 0, sender_id: self.currentUser.id, receiver_id: self.member.id, message: message, sender:self.currentUser});
	  self.scrollDown();
	  
		couplet.chat.sendMessage(message, self.member.id).then(function() {
			self.saving = false;
          self.getMessages();
          self.message = "";
		}, function(error) {
			self.saving = false;

          if (error.message == 'LIMIT_EXCEEDED') {
            swal({
              title: 'Exceeded monthly limit',
              text: "Your message couldn't be sent. Would you like to upgrade to a premium account?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, go ahead!'
            }).then(function (result) {
              console.log(result);
              if (result) {
                window.location.href = "/account/dashboard"
              }
            })
          } else if (error.message == 'IDENTICAL_USER') {
            swal('Cannot chat with the same user');          
		} else if (error.error == 'token_expired') {
            swal('Please login to send a message.');
          } else {
            swal('Failed to create new object, with error code: ' + error.message);
          }
		});
    },
    openList: function () {
      this.currentView = 'list';
    },
    openChat: function (member) {
      this.member = member;
      this.getMessages();
    },
    isRead(thread) {
      console.log(thread.get('readAt'), thread.get('postedAt'))
      if (thread.get('readAt') !== undefined && thread.get('readAt').getTime() > thread.get('postedAt').getTime()) {
        return true;
      } else {
        return false;
      }
    },
    getContacts: function () {
		console.log('getContacts chat:');
		var self = this;
		couplet.chat.getChats().then(function(contacts) {
			self.contactList = contacts;
		});
    },
    markAsRead: function (member) {
		var self = this;
		couplet.chat.markRead(member.id).then(function() {
			self.getUnreadChatMessages();
		});
    },
    getMessages: function () {
      var self = this;
      console.log('getMessages', self.member);
	  
		couplet.chat.getMessages(self.member.id).then(function(messages) {
			  self.messages = _.reverse(messages);
			  self.currentView = 'chat';
			  self.markAsRead(self.member);
			  self.scrollDown();
		});


    }
  }
});

$(document).ready(function () {
		$(".fancybox").fancybox();
});

function subscribeNow(interval) {
	console.log('interval', interval);
  handler.open({
    name: settings[interval] + ' Month Plan',
    description: "Match Subscription (" + settings[interval] + " " + settings.default_currency + " per month)",
    panelLabel: "Subscribe",
    allowRememberMe: false,
    email: currentUser.email
  });
}

// Close Checkout on page navigation:
$(window).on('popstate', function () {
  handler.close();
});



var router = new VueRouter({
  history: true,
  root: site_url('').replace(location.origin, '')
});

var trackPage = {
  route: {
    // @NOTE: not activate, as transition can be aborted
    data: function () {
      this.$dispatch('nav-enter', this);
    },
  },
};
var accountVue = Vue.extend({
  template: '#account',
  data: function () {
    return {
      currentUser: currentUser
    }
  },
  methods: {
    isPremium() {
      return isPremium(currentUser);
    }
  }
})
router.map({
  '/account': {
    component: accountVue,
    // add a subRoutes map under /foo
    subRoutes: {
      '/dashboard': {
        component: accountDashboardVue
      },
      '/gallery': {
        component: accountGalleryVue
      },
      '/friends': {
        component: accountFriendsVue
      },
      '/inbox': {
        component: accountInboxVue,
      },
      '/inbox/:user_id': {
        component: accountInboxThreadVue
      },
      '/settings': {
        component: accountSettingsVue
      },
      '/profile': {
        component: accountProfileVue
      }
    }
  }
});

var App = Vue.extend({
  data: function () {
    return {
      messages: [],
      currentUser: currentUser,
      inbox: 0,
      chat_count: 0
    }
  },
  components: {
    'navbar': navbarVue
  },
  ready: function () {
	var self = this;
	if (this.currentUser) {
		couplet.User.fetch().then(function (fetchedUser) {
			currentUser = self.currentUser = couplet.User.current();
			console.log('currentUser.is_premium', self.currentUser.is_premium);
			self.$broadcast('sync-user');
		});
		this.getUnreadThreads();
	//this.inbox = couplet.User.current().get('private_message_count');
	}
  },
  methods: {
    avatar_image: function (user) {
      return avatar_image(user);
    },
    getUnreadThreads: function (user) {
		var self = this;
	  	couplet.User.getUnreadMessages().then(function(chat_count) {
			console.log('getUnreadMessages', chat_count);
			self.inbox = chat_count;
		});
    },


  },
  events: {
    'login': function () {
      this.currentUser = couplet.User.current();
      couplet.User.fetch().then(function (fetchedUser) {
        redirect_url('/account/profile');
      });
    },
    'logout': function () {
      console.log(this.currentUser);
      this.currentUser = couplet.User.current();
      couplet.User.fetch().then(function (fetchedUser) {
        redirect_url('/');
      });
    },
    'update-user': function () {
      this.currentUser = couplet.User.current();
    },
    'update-messages': function () {
      this.getUnreadThreads();
	  this.$broadcast('update-inbox');
    }
  }
})

router.start(App, '#app');
if (window.location.href.indexOf("/account/") > -1) {
  //  router.start(App, '#app');
} else {

}
if (window.location.href.indexOf("/members") > -1 && currentUser == null) {
  $('#modalRegister').modal('show')
}
if (window.location.href.indexOf("/profile") > -1 && currentUser == null) {
  $('#modalRegister').modal('show')
}

$('#modalLogin').on('show.bs.modal', function () {
  // Load up a new modal...
  $('#modalRegister').modal('hide')
})



$('#accordion .panel-collapse').on('shown.bs.collapse', function () {
  console.log($(this).prev().find("i"));
  $(this).prev().find("i").removeClass("fa-plus").addClass("fa-minus");
});

$('#accordion .panel-collapse').on('hidden.bs.collapse', function () {
  $(this).prev().find("i").removeClass("fa-minus").addClass("fa-plus");
});

function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}