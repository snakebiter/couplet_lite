(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.Couplet = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Deferred = require('deferred-js');
var superagent = require('./myagent');
var jwtDecode = require('jwt-decode');
var Cookies = require('js-cookie');
var _store = require('store');

var Couplet = function () {
	function Couplet(base) {
		_classCallCheck(this, Couplet);

		this.base = base;
		this.User = new CoupletUser(this);
		this.Query = CoupletQuery;
		this.members = new CoupletMembers(this);
		this.chat = new CoupletChat(this);

		//superagent.set({'csrftoken': Laravel.csrfToken});
	}

	_createClass(Couplet, [{
		key: 'call',
		value: function call(method, url, data) {
			var defer = new Deferred();

			var req = superagent(method, this.base + url);
			if (method == 'POST') {
				req.send(data);
			}
			if (method == 'PUT') {
				req.send(data);
			}
			if (method == 'GET') {
				req.query(data);
			}
			req.end(function (err, res) {
				console.log('err + err', err);
				console.log('res', err, res);
				if (err || !res.ok) {
					defer.reject(res.body);
				} else {
					defer.resolve(res.body);
				}
			});

			return defer.promise();
		}
	}, {
		key: 'request',
		value: function request(method, url, data) {

			data = typeof data !== 'undefined' ? data : {};
			var defer = new Deferred();
			var self = this;
			var needs_refreshing = false;

			if (Cookies.get('couplet_token')) {
				var decoded = jwtDecode(Cookies.get('couplet_token'));

				var currentDate = new Date();
				currentDate.setTime(currentDate.getTime() - 1 * 60 * 1000);
				console.log('jwt_decode', decoded.nbf, currentDate / 1000, decoded.nbf - currentDate / 1000, decoded.nbf > currentDate / 1000);
				needs_refreshing = decoded.nbf < currentDate / 1000;
			}
			//var needs_refreshing = false;
			if (needs_refreshing) {
				console.log("Needs refreshing");

				self.User.refresh(method, url, data).then(function (res) {

					self.call(method, url, data).then(function (res) {
						defer.resolve(res);
					}, function (res) {
						defer.reject(res);
					});
				}, function (res) {
					defer.reject(res);
				});
			} else {

				self.call(method, url, data).then(function (res) {
					defer.resolve(res);
				}, function (res) {
					defer.reject(res);
				});
			}

			return defer.promise();
		}
	}]);

	return Couplet;
}();

var CoupletMembers = function () {
	function CoupletMembers(couplet) {
		_classCallCheck(this, CoupletMembers);

		this.couplet = couplet;
		this.params = [];
		this.limit = 10;
		this.page = 10;
	}

	_createClass(CoupletMembers, [{
		key: 'setLimit',
		value: function setLimit(limit) {
			this.limit = limit;
		}
	}, {
		key: 'setPage',
		value: function setPage(page) {
			this.page = page;
		}
	}, {
		key: 'setQuery',
		value: function setQuery(params) {
			this.params = params;
		}
	}, {
		key: 'get',
		value: function get(id) {
			var defer = new Deferred();
			console.log(this.params);
			superagent('GET', this.couplet.base + 'members/' + id).end(function (err, res) {
				if (err || !res.ok) {
					defer.reject(res.body);
				} else {
					defer.resolve(res.body);
				}
			});
			return defer.promise();
		}
	}, {
		key: 'getLatestMembers',
		value: function getLatestMembers(limit) {
			var defer = new Deferred();
			this.couplet.request('GET', 'members/latest/' + limit).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'markPrivateMessageAsRead',
		value: function markPrivateMessageAsRead(memberId) {

			var defer = new Deferred();
			this.couplet.request('GET', 'members/mark-read/' + memberId).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'sendPrivateMessage',
		value: function sendPrivateMessage(message, memberId) {
			var defer = new Deferred();
			this.couplet.request('POST', 'members/send-message/' + memberId, { 'message': message }).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'getPhotos',
		value: function getPhotos(id) {
			var defer = new Deferred();
			this.couplet.request('GET', 'members/' + id + '/photos').then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'isLiked',
		value: function isLiked(memberId) {
			var defer = new Deferred();
			this.couplet.request('GET', 'members/liked/' + memberId).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'like',
		value: function like(memberId) {

			var defer = new Deferred();
			this.couplet.request('GET', 'members/like/' + memberId).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'find',
		value: function find() {

			var defer = new Deferred();
			this.couplet.request('POST', 'members', { 'params': this.params, limit: this.limit, page: this.page }).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}]);

	return CoupletMembers;
}();

var CoupletQuery = function () {
	function CoupletQuery(CoupletObject) {
		_classCallCheck(this, CoupletQuery);

		this.couplet = couplet;
		this.params = [];
	}

	_createClass(CoupletQuery, [{
		key: 'equalTo',
		value: function equalTo(field, value) {
			this.params.push([field, value, 'eq']);
		}
	}, {
		key: 'containsAll',
		value: function containsAll(field, value) {
			this.params.push([field, value, 'contains']);
		}
	}, {
		key: 'containedIn',
		value: function containedIn(field, value) {
			this.params.push([field, value, 'in']);
		}
	}, {
		key: 'greaterThan',
		value: function greaterThan(field, value) {
			this.params.push([field, value, 'gt']);
		}
	}, {
		key: 'lessThan',
		value: function lessThan(field, value) {
			this.params.push([field, value, 'lt']);
		}
	}, {
		key: 'lessThanOrEqualTo',
		value: function lessThanOrEqualTo(field, value) {
			this.params.push([field, value, 'lte']);
		}
	}, {
		key: 'greaterThanOrEqualTo',
		value: function greaterThanOrEqualTo(field, value) {
			this.params.push([field, value, 'gte']);
		}
	}, {
		key: 'getParams',
		value: function getParams() {
			return this.params;
		}
	}]);

	return CoupletQuery;
}();

var CoupletUser = function () {
	function CoupletUser(couplet) {
		_classCallCheck(this, CoupletUser);

		this.couplet = couplet;
		this.user = null;
	}

	_createClass(CoupletUser, [{
		key: 'refresh',
		value: function refresh() {
			var defer = new Deferred();
			self = this;
			superagent('POST', this.couplet.base + 'users/refresh').end(function (err, res) {
				if (err || !res.ok) {
					defer.reject(res.body);
				} else {
					console.log('res.header');
					console.log('res.header', res.header.authorization.replace("Bearer ", ""));
					self.become(res.header.authorization.replace("Bearer ", ""));
					defer.resolve(res.body);
				}
			});
			return defer.promise();
		}
	}, {
		key: 'become',
		value: function become(token) {
			var defer = new Deferred();

			superagent.set({ 'Authorization': 'Bearer ' + token });
			var decoded = jwtDecode(token);
			console.log('become', decoded);

			Cookies.set('couplet_token', token, { expires: 365 });
			this.token = token;

			defer.resolve(true);
			return defer.promise();
		}
	}, {
		key: 'current',
		value: function current() {
			var token = Cookies.get('couplet_token');
			superagent.set({ 'Authorization': 'Bearer ' + token });
			return _store.get('couplet_user');
		}
	}, {
		key: 'clear',
		value: function clear() {
			_store.remove('couplet_user');
			Cookies.remove('couplet_token');
		}
	}, {
		key: 'store',
		value: function store(user) {
			_store.set('couplet_user', user);
		}
	}, {
		key: 'getThreadMessages',
		value: function getThreadMessages(id) {
			var defer = new Deferred();
			this.couplet.request('GET', 'users/thread-messages/' + id).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'getSentThreads',
		value: function getSentThreads(data) {
			var defer = new Deferred();
			this.couplet.request('GET', 'users/sent-threads', data).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'getThreads',
		value: function getThreads(data) {
			var defer = new Deferred();
			this.couplet.request('GET', 'users/threads', data).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'getLatestMessages',
		value: function getLatestMessages(data) {
			var defer = new Deferred();
			this.couplet.request('GET', 'users/latest-messages', data).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'getUnreadMessages',
		value: function getUnreadMessages(data) {
			var defer = new Deferred();
			this.couplet.request('GET', 'users/unread', data).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'uploadAvatar',
		value: function uploadAvatar(data) {
			var defer = new Deferred();
			this.couplet.request('POST', 'users/upload-avatar', data).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'getPhotos',
		value: function getPhotos(data) {
			var defer = new Deferred();
			this.couplet.request('GET', 'users/photos').then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'deletePhoto',
		value: function deletePhoto(id) {

			var defer = new Deferred();
			this.couplet.request('DELETE', 'users/photos/' + id).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'uploadPhoto',
		value: function uploadPhoto(data) {

			var defer = new Deferred();
			this.couplet.request('POST', 'users/upload-photo', data).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'save',
		value: function save(data) {

			var defer = new Deferred();

			var defer = new Deferred();
			var self = this;
			console.log(data);
			this.couplet.request('PUT', 'users/me', data).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'getMatches',
		value: function getMatches() {
			var defer = new Deferred();
			this.couplet.request('GET', 'users/matches').then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'getMembersYouLiked',
		value: function getMembersYouLiked() {
			var defer = new Deferred();
			this.couplet.request('GET', 'users/liked').then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'getMembersWhoLikedYou',
		value: function getMembersWhoLikedYou() {
			var defer = new Deferred();
			this.couplet.request('GET', 'users/liked-you').then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'getMembersWhoInteractedWithYou',
		value: function getMembersWhoInteractedWithYou() {
			var defer = new Deferred();
			this.couplet.request('GET', 'users/interactions').then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'fetch',
		value: function fetch() {
			var defer = new Deferred();
			var self = this;
			this.couplet.request('GET', 'users/me').then(function (res) {
				self.store(res);
				defer.resolve(res);
			}, function (res) {
				self.clear();
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'signUp',
		value: function signUp(data) {
			var defer = new Deferred();
			var self = this;
			this.couplet.call('POST', 'users/register', data).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'logIn',
		value: function logIn(email, password) {
			var defer = new Deferred();
			var self = this;
			console.log({ email: email, password: password });
			this.couplet.call('GET', 'users/login', { email: email, password: password }).then(function (res) {
				self.become(res.token);
				self.fetch();
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'logOut',
		value: function logOut() {
			var defer = new Deferred();
			var self = this;
			Cookies.remove('couplet_token');
			this.couplet.call('GET', 'users/logout').then(function (res) {
				self.clear();
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'stripePayment',
		value: function stripePayment(token, interval) {
			var defer = new Deferred();
			var self = this;
			this.couplet.call('POST', 'stripe/after-payment', { token: token, interval: interval }).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'requestPasswordReset',
		value: function requestPasswordReset(email) {
			var defer = new Deferred();
			var self = this;
			this.couplet.call('POST', 'users/reset-password', { email: email }).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}]);

	return CoupletUser;
}();

var CoupletObject = function () {
	function CoupletObject(couplet) {
		_classCallCheck(this, CoupletObject);

		this.couplet = couplet;
		this.object = object;
		this.className = couplet.toUnderscore();
		superagent.set({ 'csrftoken': Laravel.csrfToken });
	}

	_createClass(CoupletObject, [{
		key: 'toUnderscore',
		value: function toUnderscore(name) {
			return name.replace(/([A-Z])/g, function ($1) {
				return "_" + $1.toLowerCase();
			});
		}
	}, {
		key: 'get',
		value: function get(id) {
			var self = this;
			var defer = new Deferred();

			superagent('GET', this.couplet.base + '' + className + '/' + id).end(function (err, res) {
				if (err || !res.ok) {
					defer.reject(res.body);
				} else {
					//save the jwt token
					//fetch the user
					defer.resolve(res.body);
				}
			});
			return defer.promise();
		}
	}]);

	return CoupletObject;
}();

var CoupletChat = function () {
	function CoupletChat(couplet) {
		_classCallCheck(this, CoupletChat);

		this.couplet = couplet;
	}

	_createClass(CoupletChat, [{
		key: 'sendMessage',
		value: function sendMessage(message, memberId) {
			var defer = new Deferred();
			this.couplet.request('POST', 'chat/send-message/' + memberId, { 'message': message }).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'getMessages',
		value: function getMessages(memberId) {
			var defer = new Deferred();
			this.couplet.request('GET', 'chat/messages/' + memberId).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'getUnreadMessages',
		value: function getUnreadMessages(memberId) {
			var defer = new Deferred();
			this.couplet.request('GET', 'chat/unread').then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'getChats',
		value: function getChats() {
			var defer = new Deferred();
			this.couplet.request('GET', 'chat').then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}, {
		key: 'markRead',
		value: function markRead(memberId) {
			var defer = new Deferred();
			this.couplet.request('GET', 'chat/mark-read/' + memberId).then(function (res) {
				defer.resolve(res);
			}, function (res) {
				defer.reject(res);
			});
			return defer.promise();
		}
	}]);

	return CoupletChat;
}();

module.exports = Couplet;

},{"./myagent":2,"deferred-js":4,"js-cookie":5,"jwt-decode":8,"store":9}],2:[function(require,module,exports){
'use strict';

var superagent = require('superagent');

var defaultHeaders = {};
function isObject(obj) {
   return Object(obj) === obj;
};

function request(method, url) {
   return superagent(method, url).set(defaultHeaders);
}

request.set = function (field, value) {
   if (isObject(field)) {
      for (var key in field) {
         this.set(key, field[key]);
      }return this;
   }
   defaultHeaders[field] = value;
   return this;
};
module.exports = request;

},{"superagent":10}],3:[function(require,module,exports){

/**
 * Expose `Emitter`.
 */

if (typeof module !== 'undefined') {
  module.exports = Emitter;
}

/**
 * Initialize a new `Emitter`.
 *
 * @api public
 */

function Emitter(obj) {
  if (obj) return mixin(obj);
};

/**
 * Mixin the emitter properties.
 *
 * @param {Object} obj
 * @return {Object}
 * @api private
 */

function mixin(obj) {
  for (var key in Emitter.prototype) {
    obj[key] = Emitter.prototype[key];
  }
  return obj;
}

/**
 * Listen on the given `event` with `fn`.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.on =
Emitter.prototype.addEventListener = function(event, fn){
  this._callbacks = this._callbacks || {};
  (this._callbacks['$' + event] = this._callbacks['$' + event] || [])
    .push(fn);
  return this;
};

/**
 * Adds an `event` listener that will be invoked a single
 * time then automatically removed.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.once = function(event, fn){
  function on() {
    this.off(event, on);
    fn.apply(this, arguments);
  }

  on.fn = fn;
  this.on(event, on);
  return this;
};

/**
 * Remove the given callback for `event` or all
 * registered callbacks.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.off =
Emitter.prototype.removeListener =
Emitter.prototype.removeAllListeners =
Emitter.prototype.removeEventListener = function(event, fn){
  this._callbacks = this._callbacks || {};

  // all
  if (0 == arguments.length) {
    this._callbacks = {};
    return this;
  }

  // specific event
  var callbacks = this._callbacks['$' + event];
  if (!callbacks) return this;

  // remove all handlers
  if (1 == arguments.length) {
    delete this._callbacks['$' + event];
    return this;
  }

  // remove specific handler
  var cb;
  for (var i = 0; i < callbacks.length; i++) {
    cb = callbacks[i];
    if (cb === fn || cb.fn === fn) {
      callbacks.splice(i, 1);
      break;
    }
  }
  return this;
};

/**
 * Emit `event` with the given args.
 *
 * @param {String} event
 * @param {Mixed} ...
 * @return {Emitter}
 */

Emitter.prototype.emit = function(event){
  this._callbacks = this._callbacks || {};
  var args = [].slice.call(arguments, 1)
    , callbacks = this._callbacks['$' + event];

  if (callbacks) {
    callbacks = callbacks.slice(0);
    for (var i = 0, len = callbacks.length; i < len; ++i) {
      callbacks[i].apply(this, args);
    }
  }

  return this;
};

/**
 * Return array of callbacks for `event`.
 *
 * @param {String} event
 * @return {Array}
 * @api public
 */

Emitter.prototype.listeners = function(event){
  this._callbacks = this._callbacks || {};
  return this._callbacks['$' + event] || [];
};

/**
 * Check if this emitter has `event` handlers.
 *
 * @param {String} event
 * @return {Boolean}
 * @api public
 */

Emitter.prototype.hasListeners = function(event){
  return !! this.listeners(event).length;
};

},{}],4:[function(require,module,exports){
// deferred/deferred
module.exports = (function(window) {
    var deferId = 0,
        defNum = 0,
        isArray = function(arr) {
		  return Object.prototype.toString.call(arr) === '[object Array]';
	   };    
    
	function foreach(arr, handler) {
		if (isArray(arr)) {
			for (var i = 0; i < arr.length; i++) {
				handler(arr[i]);
			}
		}
		else
			handler(arr);
	}
    
	function D(fn) {
		var status = 'pending',
			doneFuncs = [],
			failFuncs = [],
			progressFuncs = [],
            lastNotify = null,
			resultArgs = null,
            thisId = deferId++,

		promise = {
			done: function() {
				for (var i = 0; i < arguments.length; i++) {
					// skip any undefined or null arguments
					if (!arguments[i]) {
						continue;
					}

					if (isArray(arguments[i])) {
						var arr = arguments[i];
						for (var j = 0; j < arr.length; j++) {
							// immediately call the function if the deferred has been resolved
							if (status === 'resolved') {
								arr[j].apply(this === deferred ? promise : this, resultArgs);
							}

							doneFuncs.push(arr[j].bind(this === deferred ? promise : this));
						}
					}
					else {
						// immediately call the function if the deferred has been resolved
						if (status === 'resolved') {
							arguments[i].apply(this === deferred ? promise : this, resultArgs);
						}

						doneFuncs.push(arguments[i].bind(this === deferred ? promise : this));
					}
				}
				
				return this;
			},

			fail: function() {
				for (var i = 0; i < arguments.length; i++) {
					// skip any undefined or null arguments
					if (!arguments[i]) {
						continue;
					}

					if (isArray(arguments[i])) {
						var arr = arguments[i];
						for (var j = 0; j < arr.length; j++) {
							// immediately call the function if the deferred has been resolved
							if (status === 'rejected') {
								arr[j].apply(this === deferred ? promise : this, resultArgs);
							}

							failFuncs.push(arr[j].bind(this === deferred ? promise : this));
						}
					}
					else {
						// immediately call the function if the deferred has been resolved
						if (status === 'rejected') {
							arguments[i].apply(this === deferred ? promise : this, resultArgs);
						}

						failFuncs.push(arguments[i].bind(this === deferred ? promise : this));
					}
				}
				
				return this;
			},

			always: function() {
//				return promise.done.apply(this, arguments).fail.apply(this, arguments);
                promise.done.apply(promise, arguments).fail.apply(promise, arguments);
                
                return this;
			},

			progress: function() {
				for (var i = 0; i < arguments.length; i++) {
					// skip any undefined or null arguments
					if (!arguments[i]) {
						continue;
					}

					if (isArray(arguments[i])) {
						var arr = arguments[i];
						for (var j = 0; j < arr.length; j++) {
							// immediately call the function if the deferred has been resolved/rejected
							if (status === 'pending') {
				                progressFuncs.push(arr[j]);
							}
                            if (lastNotify !== null){
                               arr[j].apply(deferred, lastNotify);
                            }                            
						}
					}
					else {
						// immediately call the function if the deferred has been resolved/rejected
						if (status === 'pending') {    
                            progressFuncs.push(arguments[i]);
							}
                            if (lastNotify !== null){
					           arguments[i].apply(deferred, lastNotify);
                            }                                                
					   }
				}

//                if (status !== 'pending' && lastNotify !== null) {
//                    deferred.notifyWith.apply(deferred, lastNotify);
//                }
                
				return this;
			},

//			then: function() {
//				// fail callbacks
//				if (arguments.length > 1 && arguments[1]) {
//					this.fail(arguments[1]);
//				}
//
//				// done callbacks
//				if (arguments.length > 0 && arguments[0]) {
//					this.done(arguments[0]);
//				}
//
//				// notify callbacks
//				if (arguments.length > 2 && arguments[2]) {
//					this.progress(arguments[2]);
//				}
//                
//                return this;
//			},

			promise: function(obj) {
				if (obj == null) {
					return promise;
				} else {
					for (var i in promise) {
						obj[i] = promise[i];
					}
					return obj;
				}
			},

			state: function() {
				return status;
			},

			debug: function() {
                console.log('id', thisId);
				console.log('[debug]', doneFuncs, failFuncs, status);
			},

			isRejected: function() {
				return status === 'rejected';
			},

			isResolved: function() {
				return status === 'resolved';
			},

			pipe: function(done, fail, progress) {
				var newDef = D(function(def) {
                    var that = this;
					foreach(done || null, function(func) {
						// filter function
						if (typeof func === 'function') {
							deferred.done(function() {
								var returnval = func.apply(this, arguments);
								// if a new deferred/promise is returned, its state is passed to the current deferred/promise
								if (returnval && typeof returnval.promise === 'function') {
									returnval.promise().done(def.resolve).fail(def.reject).progress(def.notify);
								}
								else {	// if new return val is passed, it is passed to the piped done
									def.resolveWith(this === promise ? def.promise() : this, [returnval]);
								}
							}.bind(that));
						} else {
							deferred.done(def.resolve);
						}
					});

					foreach(fail || null, function(func) {
						if (typeof func === 'function') {
							deferred.fail(function() {
								var returnval = func.apply(this, arguments);
								
								if (returnval && typeof returnval.promise === 'function') {
									returnval.promise().done(def.resolve).fail(def.reject).progress(def.notify);
								} else {
									def.rejectWith(this === promise ? def.promise() : this, [returnval]);
								}
							}.bind(that));
						}
						else {
							deferred.fail(def.reject);
						}
					});
                    
					foreach(progress || null, function(func) {
						if (typeof func === 'function') {
							deferred.progress(function() {
								var returnval = func.apply(this, arguments);
								
								if (returnval && typeof returnval.promise === 'function') {
									returnval.promise().done(def.resolve).fail(def.reject).progress(def.notify);
								} else {
									def.notifyWith(this === promise ? def.promise() : this, [returnval]);
								}
							}.bind(that));
						}
						else {
							deferred.progress(def.notify);
						}
					});
				});
                
                return newDef.promise();
			},
            
            getContext: function() {
                return context;
            },
            
            getId: function() {
                return thisId;
            }
		},

		deferred = {
			resolveWith: function(ctx) {
				if (status === 'pending') {
					status = 'resolved';
					var args = resultArgs = (arguments.length > 1) ? arguments[1] : [];
					for (var i = 0; i < doneFuncs.length; i++) {
						doneFuncs[i].apply(ctx, args);
					}
				}
                
                // context = ctx;                
                
				return this;
			},

			rejectWith: function(ctx) {
				if (status === 'pending') {
					status = 'rejected';
					var args = resultArgs = (arguments.length > 1) ? arguments[1] : [];
					for (var i = 0; i < failFuncs.length; i++) {
						failFuncs[i].apply(ctx, args);
					}
				}
                
                // context = ctx;                
                
				return this;
			},

			notifyWith: function(ctx) {
				var args;
                
                if (status === 'pending') {                
                    args = lastNotify = (arguments.length > 1) ? arguments[1] : [];
                    for (var i = 0; i < progressFuncs.length; i++) {    
                        progressFuncs[i].apply(ctx, args);
                    }
                    
                    // context = ctx;
                }
                
				return this;
			},

			resolve: function() {
				var ret = deferred.resolveWith(this === deferred ? promise : this, arguments);
                return this !== deferred ? this : ret;
			},

			reject: function() {
				var ret = deferred.rejectWith(this === deferred ? promise : this, arguments);
                return this !== deferred ? this : ret;                    
			},

			notify: function() {
				var ret = deferred.notifyWith(this === deferred ? promise : this, arguments);
                return this !== deferred ? this : ret;                    
			}
		}

        promise.then = promise.pipe;
                    
		var obj = promise.promise(deferred);
        
        context = obj;
        
        obj.id = deferred.id = thisId;

		if (fn) {
			fn.apply(obj, [obj]);
		}
        
		return obj;
	};

	D.when = function() {
		if (arguments.length < 2) {
			var obj = arguments.length ? arguments[0] : undefined;
			if (obj && (typeof obj.isResolved === 'function' && typeof obj.isRejected === 'function')) {
				return obj.promise();			
			}
			else {
				return D().resolveWith(window, [obj]).promise();
			}
		}
		else {
			return (function(args){
				var df = D(),
					size = args.length,
					done = 0,
					rp = new Array(size),	// resolve params: params of each resolve, we need to track down them to be able to pass them in the correct order if the master needs to be resolved
                    pp = new Array(size),
                    whenContext = [];

				for (var i = 0; i < args.length; i++) {
                    whenContext[i] = args[i] && args[i].promise ? args[i].promise() : undefined;
					(function(j) {
                        var obj = null;
                        
                        if (args[j].done) {
                            args[j].done(function() { rp[j] = (arguments.length < 2) ? arguments[0] : arguments; if (++done == size) { df.resolve.apply(whenContext, rp); }})
                            .fail(function() { df.reject.apply(whenContext, arguments); });
                        } else {
                            obj = args[j];
                            args[j] = new Deferred();
                            
                            args[j].done(function() { rp[j] = (arguments.length < 2) ? arguments[0] : arguments; if (++done == size) { df.resolve.apply(whenContext, rp); }})
                            .fail(function() { df.reject.apply(whenContext, arguments); }).resolve(obj);
                        }
                        
                        console.log('execing progress');
                        args[j].progress(function() {
                            pp[j] = (arguments.length < 2) ? arguments[0] : arguments;
                            df.notify.apply(whenContext, pp);
                        });
					})(i);
				}

				return df.promise();
			})(arguments);
		}
	}
    
    return D;
})({});
},{}],5:[function(require,module,exports){
/*!
 * JavaScript Cookie v2.1.3
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
;(function (factory) {
	var registeredInModuleLoader = false;
	if (typeof define === 'function' && define.amd) {
		define(factory);
		registeredInModuleLoader = true;
	}
	if (typeof exports === 'object') {
		module.exports = factory();
		registeredInModuleLoader = true;
	}
	if (!registeredInModuleLoader) {
		var OldCookies = window.Cookies;
		var api = window.Cookies = factory();
		api.noConflict = function () {
			window.Cookies = OldCookies;
			return api;
		};
	}
}(function () {
	function extend () {
		var i = 0;
		var result = {};
		for (; i < arguments.length; i++) {
			var attributes = arguments[ i ];
			for (var key in attributes) {
				result[key] = attributes[key];
			}
		}
		return result;
	}

	function init (converter) {
		function api (key, value, attributes) {
			var result;
			if (typeof document === 'undefined') {
				return;
			}

			// Write

			if (arguments.length > 1) {
				attributes = extend({
					path: '/'
				}, api.defaults, attributes);

				if (typeof attributes.expires === 'number') {
					var expires = new Date();
					expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
					attributes.expires = expires;
				}

				try {
					result = JSON.stringify(value);
					if (/^[\{\[]/.test(result)) {
						value = result;
					}
				} catch (e) {}

				if (!converter.write) {
					value = encodeURIComponent(String(value))
						.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
				} else {
					value = converter.write(value, key);
				}

				key = encodeURIComponent(String(key));
				key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
				key = key.replace(/[\(\)]/g, escape);

				return (document.cookie = [
					key, '=', value,
					attributes.expires ? '; expires=' + attributes.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
					attributes.path ? '; path=' + attributes.path : '',
					attributes.domain ? '; domain=' + attributes.domain : '',
					attributes.secure ? '; secure' : ''
				].join(''));
			}

			// Read

			if (!key) {
				result = {};
			}

			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all. Also prevents odd result when
			// calling "get()"
			var cookies = document.cookie ? document.cookie.split('; ') : [];
			var rdecode = /(%[0-9A-Z]{2})+/g;
			var i = 0;

			for (; i < cookies.length; i++) {
				var parts = cookies[i].split('=');
				var cookie = parts.slice(1).join('=');

				if (cookie.charAt(0) === '"') {
					cookie = cookie.slice(1, -1);
				}

				try {
					var name = parts[0].replace(rdecode, decodeURIComponent);
					cookie = converter.read ?
						converter.read(cookie, name) : converter(cookie, name) ||
						cookie.replace(rdecode, decodeURIComponent);

					if (this.json) {
						try {
							cookie = JSON.parse(cookie);
						} catch (e) {}
					}

					if (key === name) {
						result = cookie;
						break;
					}

					if (!key) {
						result[name] = cookie;
					}
				} catch (e) {}
			}

			return result;
		}

		api.set = api;
		api.get = function (key) {
			return api.call(api, key);
		};
		api.getJSON = function () {
			return api.apply({
				json: true
			}, [].slice.call(arguments));
		};
		api.defaults = {};

		api.remove = function (key, attributes) {
			api(key, '', extend(attributes, {
				expires: -1
			}));
		};

		api.withConverter = init;

		return api;
	}

	return init(function () {});
}));

},{}],6:[function(require,module,exports){
/**
 * The code was extracted from:
 * https://github.com/davidchambers/Base64.js
 */

var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

function InvalidCharacterError(message) {
  this.message = message;
}

InvalidCharacterError.prototype = new Error();
InvalidCharacterError.prototype.name = 'InvalidCharacterError';

function polyfill (input) {
  var str = String(input).replace(/=+$/, '');
  if (str.length % 4 == 1) {
    throw new InvalidCharacterError("'atob' failed: The string to be decoded is not correctly encoded.");
  }
  for (
    // initialize result and counters
    var bc = 0, bs, buffer, idx = 0, output = '';
    // get next character
    buffer = str.charAt(idx++);
    // character found in table? initialize bit storage and add its ascii value;
    ~buffer && (bs = bc % 4 ? bs * 64 + buffer : buffer,
      // and if not first of each 4 characters,
      // convert the first 8 bits to one ascii character
      bc++ % 4) ? output += String.fromCharCode(255 & bs >> (-2 * bc & 6)) : 0
  ) {
    // try to find character in table (0-63, not found => -1)
    buffer = chars.indexOf(buffer);
  }
  return output;
}


module.exports = typeof window !== 'undefined' && window.atob && window.atob.bind(window) || polyfill;

},{}],7:[function(require,module,exports){
var atob = require('./atob');

function b64DecodeUnicode(str) {
  return decodeURIComponent(atob(str).replace(/(.)/g, function (m, p) {
    var code = p.charCodeAt(0).toString(16).toUpperCase();
    if (code.length < 2) {
      code = '0' + code;
    }
    return '%' + code;
  }));
}

module.exports = function(str) {
  var output = str.replace(/-/g, "+").replace(/_/g, "/");
  switch (output.length % 4) {
    case 0:
      break;
    case 2:
      output += "==";
      break;
    case 3:
      output += "=";
      break;
    default:
      throw "Illegal base64url string!";
  }

  try{
    return b64DecodeUnicode(output);
  } catch (err) {
    return atob(output);
  }
};

},{"./atob":6}],8:[function(require,module,exports){
'use strict';

var base64_url_decode = require('./base64_url_decode');

module.exports = function (token,options) {
  if (typeof token !== 'string') {
    throw new Error('Invalid token specified');
  }

  options = options || {};
  var pos = options.header === true ? 0 : 1;
  return JSON.parse(base64_url_decode(token.split('.')[pos]));
};

},{"./base64_url_decode":7}],9:[function(require,module,exports){
(function (global){
"use strict"
// Module export pattern from
// https://github.com/umdjs/umd/blob/master/returnExports.js
;(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.store = factory();
  }
}(this, function () {
	
	// Store.js
	var store = {},
		win = (typeof window != 'undefined' ? window : global),
		doc = win.document,
		localStorageName = 'localStorage',
		scriptTag = 'script',
		storage

	store.disabled = false
	store.version = '1.3.20'
	store.set = function(key, value) {}
	store.get = function(key, defaultVal) {}
	store.has = function(key) { return store.get(key) !== undefined }
	store.remove = function(key) {}
	store.clear = function() {}
	store.transact = function(key, defaultVal, transactionFn) {
		if (transactionFn == null) {
			transactionFn = defaultVal
			defaultVal = null
		}
		if (defaultVal == null) {
			defaultVal = {}
		}
		var val = store.get(key, defaultVal)
		transactionFn(val)
		store.set(key, val)
	}
	store.getAll = function() {}
	store.forEach = function() {}

	store.serialize = function(value) {
		return JSON.stringify(value)
	}
	store.deserialize = function(value) {
		if (typeof value != 'string') { return undefined }
		try { return JSON.parse(value) }
		catch(e) { return value || undefined }
	}

	// Functions to encapsulate questionable FireFox 3.6.13 behavior
	// when about.config::dom.storage.enabled === false
	// See https://github.com/marcuswestin/store.js/issues#issue/13
	function isLocalStorageNameSupported() {
		try { return (localStorageName in win && win[localStorageName]) }
		catch(err) { return false }
	}

	if (isLocalStorageNameSupported()) {
		storage = win[localStorageName]
		store.set = function(key, val) {
			if (val === undefined) { return store.remove(key) }
			storage.setItem(key, store.serialize(val))
			return val
		}
		store.get = function(key, defaultVal) {
			var val = store.deserialize(storage.getItem(key))
			return (val === undefined ? defaultVal : val)
		}
		store.remove = function(key) { storage.removeItem(key) }
		store.clear = function() { storage.clear() }
		store.getAll = function() {
			var ret = {}
			store.forEach(function(key, val) {
				ret[key] = val
			})
			return ret
		}
		store.forEach = function(callback) {
			for (var i=0; i<storage.length; i++) {
				var key = storage.key(i)
				callback(key, store.get(key))
			}
		}
	} else if (doc && doc.documentElement.addBehavior) {
		var storageOwner,
			storageContainer
		// Since #userData storage applies only to specific paths, we need to
		// somehow link our data to a specific path.  We choose /favicon.ico
		// as a pretty safe option, since all browsers already make a request to
		// this URL anyway and being a 404 will not hurt us here.  We wrap an
		// iframe pointing to the favicon in an ActiveXObject(htmlfile) object
		// (see: http://msdn.microsoft.com/en-us/library/aa752574(v=VS.85).aspx)
		// since the iframe access rules appear to allow direct access and
		// manipulation of the document element, even for a 404 page.  This
		// document can be used instead of the current document (which would
		// have been limited to the current path) to perform #userData storage.
		try {
			storageContainer = new ActiveXObject('htmlfile')
			storageContainer.open()
			storageContainer.write('<'+scriptTag+'>document.w=window</'+scriptTag+'><iframe src="/favicon.ico"></iframe>')
			storageContainer.close()
			storageOwner = storageContainer.w.frames[0].document
			storage = storageOwner.createElement('div')
		} catch(e) {
			// somehow ActiveXObject instantiation failed (perhaps some special
			// security settings or otherwse), fall back to per-path storage
			storage = doc.createElement('div')
			storageOwner = doc.body
		}
		var withIEStorage = function(storeFunction) {
			return function() {
				var args = Array.prototype.slice.call(arguments, 0)
				args.unshift(storage)
				// See http://msdn.microsoft.com/en-us/library/ms531081(v=VS.85).aspx
				// and http://msdn.microsoft.com/en-us/library/ms531424(v=VS.85).aspx
				storageOwner.appendChild(storage)
				storage.addBehavior('#default#userData')
				storage.load(localStorageName)
				var result = storeFunction.apply(store, args)
				storageOwner.removeChild(storage)
				return result
			}
		}

		// In IE7, keys cannot start with a digit or contain certain chars.
		// See https://github.com/marcuswestin/store.js/issues/40
		// See https://github.com/marcuswestin/store.js/issues/83
		var forbiddenCharsRegex = new RegExp("[!\"#$%&'()*+,/\\\\:;<=>?@[\\]^`{|}~]", "g")
		var ieKeyFix = function(key) {
			return key.replace(/^d/, '___$&').replace(forbiddenCharsRegex, '___')
		}
		store.set = withIEStorage(function(storage, key, val) {
			key = ieKeyFix(key)
			if (val === undefined) { return store.remove(key) }
			storage.setAttribute(key, store.serialize(val))
			storage.save(localStorageName)
			return val
		})
		store.get = withIEStorage(function(storage, key, defaultVal) {
			key = ieKeyFix(key)
			var val = store.deserialize(storage.getAttribute(key))
			return (val === undefined ? defaultVal : val)
		})
		store.remove = withIEStorage(function(storage, key) {
			key = ieKeyFix(key)
			storage.removeAttribute(key)
			storage.save(localStorageName)
		})
		store.clear = withIEStorage(function(storage) {
			var attributes = storage.XMLDocument.documentElement.attributes
			storage.load(localStorageName)
			for (var i=attributes.length-1; i>=0; i--) {
				storage.removeAttribute(attributes[i].name)
			}
			storage.save(localStorageName)
		})
		store.getAll = function(storage) {
			var ret = {}
			store.forEach(function(key, val) {
				ret[key] = val
			})
			return ret
		}
		store.forEach = withIEStorage(function(storage, callback) {
			var attributes = storage.XMLDocument.documentElement.attributes
			for (var i=0, attr; attr=attributes[i]; ++i) {
				callback(attr.name, store.deserialize(storage.getAttribute(attr.name)))
			}
		})
	}

	try {
		var testKey = '__storejs__'
		store.set(testKey, testKey)
		if (store.get(testKey) != testKey) { store.disabled = true }
		store.remove(testKey)
	} catch(e) {
		store.disabled = true
	}
	store.enabled = !store.disabled
	
	return store
}));

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],10:[function(require,module,exports){
/**
 * Root reference for iframes.
 */

var root;
if (typeof window !== 'undefined') { // Browser window
  root = window;
} else if (typeof self !== 'undefined') { // Web Worker
  root = self;
} else { // Other environments
  console.warn("Using browser-only version of superagent in non-browser environment");
  root = this;
}

var Emitter = require('emitter');
var requestBase = require('./request-base');
var isObject = require('./is-object');

/**
 * Noop.
 */

function noop(){};

/**
 * Expose `request`.
 */

var request = module.exports = require('./request').bind(null, Request);

/**
 * Determine XHR.
 */

request.getXHR = function () {
  if (root.XMLHttpRequest
      && (!root.location || 'file:' != root.location.protocol
          || !root.ActiveXObject)) {
    return new XMLHttpRequest;
  } else {
    try { return new ActiveXObject('Microsoft.XMLHTTP'); } catch(e) {}
    try { return new ActiveXObject('Msxml2.XMLHTTP.6.0'); } catch(e) {}
    try { return new ActiveXObject('Msxml2.XMLHTTP.3.0'); } catch(e) {}
    try { return new ActiveXObject('Msxml2.XMLHTTP'); } catch(e) {}
  }
  throw Error("Browser-only verison of superagent could not find XHR");
};

/**
 * Removes leading and trailing whitespace, added to support IE.
 *
 * @param {String} s
 * @return {String}
 * @api private
 */

var trim = ''.trim
  ? function(s) { return s.trim(); }
  : function(s) { return s.replace(/(^\s*|\s*$)/g, ''); };

/**
 * Serialize the given `obj`.
 *
 * @param {Object} obj
 * @return {String}
 * @api private
 */

function serialize(obj) {
  if (!isObject(obj)) return obj;
  var pairs = [];
  for (var key in obj) {
    pushEncodedKeyValuePair(pairs, key, obj[key]);
  }
  return pairs.join('&');
}

/**
 * Helps 'serialize' with serializing arrays.
 * Mutates the pairs array.
 *
 * @param {Array} pairs
 * @param {String} key
 * @param {Mixed} val
 */

function pushEncodedKeyValuePair(pairs, key, val) {
  if (val != null) {
    if (Array.isArray(val)) {
      val.forEach(function(v) {
        pushEncodedKeyValuePair(pairs, key, v);
      });
    } else if (isObject(val)) {
      for(var subkey in val) {
        pushEncodedKeyValuePair(pairs, key + '[' + subkey + ']', val[subkey]);
      }
    } else {
      pairs.push(encodeURIComponent(key)
        + '=' + encodeURIComponent(val));
    }
  } else if (val === null) {
    pairs.push(encodeURIComponent(key));
  }
}

/**
 * Expose serialization method.
 */

 request.serializeObject = serialize;

 /**
  * Parse the given x-www-form-urlencoded `str`.
  *
  * @param {String} str
  * @return {Object}
  * @api private
  */

function parseString(str) {
  var obj = {};
  var pairs = str.split('&');
  var pair;
  var pos;

  for (var i = 0, len = pairs.length; i < len; ++i) {
    pair = pairs[i];
    pos = pair.indexOf('=');
    if (pos == -1) {
      obj[decodeURIComponent(pair)] = '';
    } else {
      obj[decodeURIComponent(pair.slice(0, pos))] =
        decodeURIComponent(pair.slice(pos + 1));
    }
  }

  return obj;
}

/**
 * Expose parser.
 */

request.parseString = parseString;

/**
 * Default MIME type map.
 *
 *     superagent.types.xml = 'application/xml';
 *
 */

request.types = {
  html: 'text/html',
  json: 'application/json',
  xml: 'application/xml',
  urlencoded: 'application/x-www-form-urlencoded',
  'form': 'application/x-www-form-urlencoded',
  'form-data': 'application/x-www-form-urlencoded'
};

/**
 * Default serialization map.
 *
 *     superagent.serialize['application/xml'] = function(obj){
 *       return 'generated xml here';
 *     };
 *
 */

 request.serialize = {
   'application/x-www-form-urlencoded': serialize,
   'application/json': JSON.stringify
 };

 /**
  * Default parsers.
  *
  *     superagent.parse['application/xml'] = function(str){
  *       return { object parsed from str };
  *     };
  *
  */

request.parse = {
  'application/x-www-form-urlencoded': parseString,
  'application/json': JSON.parse
};

/**
 * Parse the given header `str` into
 * an object containing the mapped fields.
 *
 * @param {String} str
 * @return {Object}
 * @api private
 */

function parseHeader(str) {
  var lines = str.split(/\r?\n/);
  var fields = {};
  var index;
  var line;
  var field;
  var val;

  lines.pop(); // trailing CRLF

  for (var i = 0, len = lines.length; i < len; ++i) {
    line = lines[i];
    index = line.indexOf(':');
    field = line.slice(0, index).toLowerCase();
    val = trim(line.slice(index + 1));
    fields[field] = val;
  }

  return fields;
}

/**
 * Check if `mime` is json or has +json structured syntax suffix.
 *
 * @param {String} mime
 * @return {Boolean}
 * @api private
 */

function isJSON(mime) {
  return /[\/+]json\b/.test(mime);
}

/**
 * Return the mime type for the given `str`.
 *
 * @param {String} str
 * @return {String}
 * @api private
 */

function type(str){
  return str.split(/ *; */).shift();
};

/**
 * Return header field parameters.
 *
 * @param {String} str
 * @return {Object}
 * @api private
 */

function params(str){
  return str.split(/ *; */).reduce(function(obj, str){
    var parts = str.split(/ *= */),
        key = parts.shift(),
        val = parts.shift();

    if (key && val) obj[key] = val;
    return obj;
  }, {});
};

/**
 * Initialize a new `Response` with the given `xhr`.
 *
 *  - set flags (.ok, .error, etc)
 *  - parse header
 *
 * Examples:
 *
 *  Aliasing `superagent` as `request` is nice:
 *
 *      request = superagent;
 *
 *  We can use the promise-like API, or pass callbacks:
 *
 *      request.get('/').end(function(res){});
 *      request.get('/', function(res){});
 *
 *  Sending data can be chained:
 *
 *      request
 *        .post('/user')
 *        .send({ name: 'tj' })
 *        .end(function(res){});
 *
 *  Or passed to `.send()`:
 *
 *      request
 *        .post('/user')
 *        .send({ name: 'tj' }, function(res){});
 *
 *  Or passed to `.post()`:
 *
 *      request
 *        .post('/user', { name: 'tj' })
 *        .end(function(res){});
 *
 * Or further reduced to a single call for simple cases:
 *
 *      request
 *        .post('/user', { name: 'tj' }, function(res){});
 *
 * @param {XMLHTTPRequest} xhr
 * @param {Object} options
 * @api private
 */

function Response(req, options) {
  options = options || {};
  this.req = req;
  this.xhr = this.req.xhr;
  // responseText is accessible only if responseType is '' or 'text' and on older browsers
  this.text = ((this.req.method !='HEAD' && (this.xhr.responseType === '' || this.xhr.responseType === 'text')) || typeof this.xhr.responseType === 'undefined')
     ? this.xhr.responseText
     : null;
  this.statusText = this.req.xhr.statusText;
  this._setStatusProperties(this.xhr.status);
  this.header = this.headers = parseHeader(this.xhr.getAllResponseHeaders());
  // getAllResponseHeaders sometimes falsely returns "" for CORS requests, but
  // getResponseHeader still works. so we get content-type even if getting
  // other headers fails.
  this.header['content-type'] = this.xhr.getResponseHeader('content-type');
  this._setHeaderProperties(this.header);
  this.body = this.req.method != 'HEAD'
    ? this._parseBody(this.text ? this.text : this.xhr.response)
    : null;
}

/**
 * Get case-insensitive `field` value.
 *
 * @param {String} field
 * @return {String}
 * @api public
 */

Response.prototype.get = function(field){
  return this.header[field.toLowerCase()];
};

/**
 * Set header related properties:
 *
 *   - `.type` the content type without params
 *
 * A response of "Content-Type: text/plain; charset=utf-8"
 * will provide you with a `.type` of "text/plain".
 *
 * @param {Object} header
 * @api private
 */

Response.prototype._setHeaderProperties = function(header){
  // content-type
  var ct = this.header['content-type'] || '';
  this.type = type(ct);

  // params
  var obj = params(ct);
  for (var key in obj) this[key] = obj[key];
};

/**
 * Parse the given body `str`.
 *
 * Used for auto-parsing of bodies. Parsers
 * are defined on the `superagent.parse` object.
 *
 * @param {String} str
 * @return {Mixed}
 * @api private
 */

Response.prototype._parseBody = function(str){
  var parse = request.parse[this.type];
  if (!parse && isJSON(this.type)) {
    parse = request.parse['application/json'];
  }
  return parse && str && (str.length || str instanceof Object)
    ? parse(str)
    : null;
};

/**
 * Set flags such as `.ok` based on `status`.
 *
 * For example a 2xx response will give you a `.ok` of __true__
 * whereas 5xx will be __false__ and `.error` will be __true__. The
 * `.clientError` and `.serverError` are also available to be more
 * specific, and `.statusType` is the class of error ranging from 1..5
 * sometimes useful for mapping respond colors etc.
 *
 * "sugar" properties are also defined for common cases. Currently providing:
 *
 *   - .noContent
 *   - .badRequest
 *   - .unauthorized
 *   - .notAcceptable
 *   - .notFound
 *
 * @param {Number} status
 * @api private
 */

Response.prototype._setStatusProperties = function(status){
  // handle IE9 bug: http://stackoverflow.com/questions/10046972/msie-returns-status-code-of-1223-for-ajax-request
  if (status === 1223) {
    status = 204;
  }

  var type = status / 100 | 0;

  // status / class
  this.status = this.statusCode = status;
  this.statusType = type;

  // basics
  this.info = 1 == type;
  this.ok = 2 == type;
  this.clientError = 4 == type;
  this.serverError = 5 == type;
  this.error = (4 == type || 5 == type)
    ? this.toError()
    : false;

  // sugar
  this.accepted = 202 == status;
  this.noContent = 204 == status;
  this.badRequest = 400 == status;
  this.unauthorized = 401 == status;
  this.notAcceptable = 406 == status;
  this.notFound = 404 == status;
  this.forbidden = 403 == status;
};

/**
 * Return an `Error` representative of this response.
 *
 * @return {Error}
 * @api public
 */

Response.prototype.toError = function(){
  var req = this.req;
  var method = req.method;
  var url = req.url;

  var msg = 'cannot ' + method + ' ' + url + ' (' + this.status + ')';
  var err = new Error(msg);
  err.status = this.status;
  err.method = method;
  err.url = url;

  return err;
};

/**
 * Expose `Response`.
 */

request.Response = Response;

/**
 * Initialize a new `Request` with the given `method` and `url`.
 *
 * @param {String} method
 * @param {String} url
 * @api public
 */

function Request(method, url) {
  var self = this;
  this._query = this._query || [];
  this.method = method;
  this.url = url;
  this.header = {}; // preserves header name case
  this._header = {}; // coerces header names to lowercase
  this.on('end', function(){
    var err = null;
    var res = null;

    try {
      res = new Response(self);
    } catch(e) {
      err = new Error('Parser is unable to parse the response');
      err.parse = true;
      err.original = e;
      // issue #675: return the raw response if the response parsing fails
      err.rawResponse = self.xhr && self.xhr.responseText ? self.xhr.responseText : null;
      // issue #876: return the http status code if the response parsing fails
      err.statusCode = self.xhr && self.xhr.status ? self.xhr.status : null;
      return self.callback(err);
    }

    self.emit('response', res);

    var new_err;
    try {
      if (res.status < 200 || res.status >= 300) {
        new_err = new Error(res.statusText || 'Unsuccessful HTTP response');
        new_err.original = err;
        new_err.response = res;
        new_err.status = res.status;
      }
    } catch(e) {
      new_err = e; // #985 touching res may cause INVALID_STATE_ERR on old Android
    }

    // #1000 don't catch errors from the callback to avoid double calling it
    if (new_err) {
      self.callback(new_err, res);
    } else {
      self.callback(null, res);
    }
  });
}

/**
 * Mixin `Emitter` and `requestBase`.
 */

Emitter(Request.prototype);
for (var key in requestBase) {
  Request.prototype[key] = requestBase[key];
}

/**
 * Set Content-Type to `type`, mapping values from `request.types`.
 *
 * Examples:
 *
 *      superagent.types.xml = 'application/xml';
 *
 *      request.post('/')
 *        .type('xml')
 *        .send(xmlstring)
 *        .end(callback);
 *
 *      request.post('/')
 *        .type('application/xml')
 *        .send(xmlstring)
 *        .end(callback);
 *
 * @param {String} type
 * @return {Request} for chaining
 * @api public
 */

Request.prototype.type = function(type){
  this.set('Content-Type', request.types[type] || type);
  return this;
};

/**
 * Set responseType to `val`. Presently valid responseTypes are 'blob' and
 * 'arraybuffer'.
 *
 * Examples:
 *
 *      req.get('/')
 *        .responseType('blob')
 *        .end(callback);
 *
 * @param {String} val
 * @return {Request} for chaining
 * @api public
 */

Request.prototype.responseType = function(val){
  this._responseType = val;
  return this;
};

/**
 * Set Accept to `type`, mapping values from `request.types`.
 *
 * Examples:
 *
 *      superagent.types.json = 'application/json';
 *
 *      request.get('/agent')
 *        .accept('json')
 *        .end(callback);
 *
 *      request.get('/agent')
 *        .accept('application/json')
 *        .end(callback);
 *
 * @param {String} accept
 * @return {Request} for chaining
 * @api public
 */

Request.prototype.accept = function(type){
  this.set('Accept', request.types[type] || type);
  return this;
};

/**
 * Set Authorization field value with `user` and `pass`.
 *
 * @param {String} user
 * @param {String} pass
 * @param {Object} options with 'type' property 'auto' or 'basic' (default 'basic')
 * @return {Request} for chaining
 * @api public
 */

Request.prototype.auth = function(user, pass, options){
  if (!options) {
    options = {
      type: 'basic'
    }
  }

  switch (options.type) {
    case 'basic':
      var str = btoa(user + ':' + pass);
      this.set('Authorization', 'Basic ' + str);
    break;

    case 'auto':
      this.username = user;
      this.password = pass;
    break;
  }
  return this;
};

/**
* Add query-string `val`.
*
* Examples:
*
*   request.get('/shoes')
*     .query('size=10')
*     .query({ color: 'blue' })
*
* @param {Object|String} val
* @return {Request} for chaining
* @api public
*/

Request.prototype.query = function(val){
  if ('string' != typeof val) val = serialize(val);
  if (val) this._query.push(val);
  return this;
};

/**
 * Queue the given `file` as an attachment to the specified `field`,
 * with optional `filename`.
 *
 * ``` js
 * request.post('/upload')
 *   .attach('content', new Blob(['<a id="a"><b id="b">hey!</b></a>'], { type: "text/html"}))
 *   .end(callback);
 * ```
 *
 * @param {String} field
 * @param {Blob|File} file
 * @param {String} filename
 * @return {Request} for chaining
 * @api public
 */

Request.prototype.attach = function(field, file, filename){
  this._getFormData().append(field, file, filename || file.name);
  return this;
};

Request.prototype._getFormData = function(){
  if (!this._formData) {
    this._formData = new root.FormData();
  }
  return this._formData;
};

/**
 * Invoke the callback with `err` and `res`
 * and handle arity check.
 *
 * @param {Error} err
 * @param {Response} res
 * @api private
 */

Request.prototype.callback = function(err, res){
  var fn = this._callback;
  this.clearTimeout();
  fn(err, res);
};

/**
 * Invoke callback with x-domain error.
 *
 * @api private
 */

Request.prototype.crossDomainError = function(){
  var err = new Error('Request has been terminated\nPossible causes: the network is offline, Origin is not allowed by Access-Control-Allow-Origin, the page is being unloaded, etc.');
  err.crossDomain = true;

  err.status = this.status;
  err.method = this.method;
  err.url = this.url;

  this.callback(err);
};

/**
 * Invoke callback with timeout error.
 *
 * @api private
 */

Request.prototype._timeoutError = function(){
  var timeout = this._timeout;
  var err = new Error('timeout of ' + timeout + 'ms exceeded');
  err.timeout = timeout;
  this.callback(err);
};

/**
 * Compose querystring to append to req.url
 *
 * @api private
 */

Request.prototype._appendQueryString = function(){
  var query = this._query.join('&');
  if (query) {
    this.url += ~this.url.indexOf('?')
      ? '&' + query
      : '?' + query;
  }
};

/**
 * Initiate request, invoking callback `fn(res)`
 * with an instanceof `Response`.
 *
 * @param {Function} fn
 * @return {Request} for chaining
 * @api public
 */

Request.prototype.end = function(fn){
  var self = this;
  var xhr = this.xhr = request.getXHR();
  var timeout = this._timeout;
  var data = this._formData || this._data;

  // store callback
  this._callback = fn || noop;

  // state change
  xhr.onreadystatechange = function(){
    if (4 != xhr.readyState) return;

    // In IE9, reads to any property (e.g. status) off of an aborted XHR will
    // result in the error "Could not complete the operation due to error c00c023f"
    var status;
    try { status = xhr.status } catch(e) { status = 0; }

    if (0 == status) {
      if (self.timedout) return self._timeoutError();
      if (self._aborted) return;
      return self.crossDomainError();
    }
    self.emit('end');
  };

  // progress
  var handleProgress = function(direction, e) {
    if (e.total > 0) {
      e.percent = e.loaded / e.total * 100;
    }
    e.direction = direction;
    self.emit('progress', e);
  }
  if (this.hasListeners('progress')) {
    try {
      xhr.onprogress = handleProgress.bind(null, 'download');
      if (xhr.upload) {
        xhr.upload.onprogress = handleProgress.bind(null, 'upload');
      }
    } catch(e) {
      // Accessing xhr.upload fails in IE from a web worker, so just pretend it doesn't exist.
      // Reported here:
      // https://connect.microsoft.com/IE/feedback/details/837245/xmlhttprequest-upload-throws-invalid-argument-when-used-from-web-worker-context
    }
  }

  // timeout
  if (timeout && !this._timer) {
    this._timer = setTimeout(function(){
      self.timedout = true;
      self.abort();
    }, timeout);
  }

  // querystring
  this._appendQueryString();

  // initiate request
  if (this.username && this.password) {
    xhr.open(this.method, this.url, true, this.username, this.password);
  } else {
    xhr.open(this.method, this.url, true);
  }

  // CORS
  if (this._withCredentials) xhr.withCredentials = true;

  // body
  if ('GET' != this.method && 'HEAD' != this.method && 'string' != typeof data && !this._isHost(data)) {
    // serialize stuff
    var contentType = this._header['content-type'];
    var serialize = this._serializer || request.serialize[contentType ? contentType.split(';')[0] : ''];
    if (!serialize && isJSON(contentType)) serialize = request.serialize['application/json'];
    if (serialize) data = serialize(data);
  }

  // set header fields
  for (var field in this.header) {
    if (null == this.header[field]) continue;
    xhr.setRequestHeader(field, this.header[field]);
  }

  if (this._responseType) {
    xhr.responseType = this._responseType;
  }

  // send stuff
  this.emit('request', this);

  // IE11 xhr.send(undefined) sends 'undefined' string as POST payload (instead of nothing)
  // We need null here if data is undefined
  xhr.send(typeof data !== 'undefined' ? data : null);
  return this;
};


/**
 * Expose `Request`.
 */

request.Request = Request;

/**
 * GET `url` with optional callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed|Function} [data] or fn
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */

request.get = function(url, data, fn){
  var req = request('GET', url);
  if ('function' == typeof data) fn = data, data = null;
  if (data) req.query(data);
  if (fn) req.end(fn);
  return req;
};

/**
 * HEAD `url` with optional callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed|Function} [data] or fn
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */

request.head = function(url, data, fn){
  var req = request('HEAD', url);
  if ('function' == typeof data) fn = data, data = null;
  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};

/**
 * OPTIONS query to `url` with optional callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed|Function} [data] or fn
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */

request.options = function(url, data, fn){
  var req = request('OPTIONS', url);
  if ('function' == typeof data) fn = data, data = null;
  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};

/**
 * DELETE `url` with optional callback `fn(res)`.
 *
 * @param {String} url
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */

function del(url, fn){
  var req = request('DELETE', url);
  if (fn) req.end(fn);
  return req;
};

request['del'] = del;
request['delete'] = del;

/**
 * PATCH `url` with optional `data` and callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed} [data]
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */

request.patch = function(url, data, fn){
  var req = request('PATCH', url);
  if ('function' == typeof data) fn = data, data = null;
  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};

/**
 * POST `url` with optional `data` and callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed} [data]
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */

request.post = function(url, data, fn){
  var req = request('POST', url);
  if ('function' == typeof data) fn = data, data = null;
  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};

/**
 * PUT `url` with optional `data` and callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed|Function} [data] or fn
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */

request.put = function(url, data, fn){
  var req = request('PUT', url);
  if ('function' == typeof data) fn = data, data = null;
  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};

},{"./is-object":11,"./request":13,"./request-base":12,"emitter":3}],11:[function(require,module,exports){
/**
 * Check if `obj` is an object.
 *
 * @param {Object} obj
 * @return {Boolean}
 * @api private
 */

function isObject(obj) {
  return null !== obj && 'object' === typeof obj;
}

module.exports = isObject;

},{}],12:[function(require,module,exports){
/**
 * Module of mixed-in functions shared between node and client code
 */
var isObject = require('./is-object');

/**
 * Clear previous timeout.
 *
 * @return {Request} for chaining
 * @api public
 */

exports.clearTimeout = function _clearTimeout(){
  this._timeout = 0;
  clearTimeout(this._timer);
  return this;
};

/**
 * Override default response body parser
 *
 * This function will be called to convert incoming data into request.body
 *
 * @param {Function}
 * @api public
 */

exports.parse = function parse(fn){
  this._parser = fn;
  return this;
};

/**
 * Override default request body serializer
 *
 * This function will be called to convert data set via .send or .attach into payload to send
 *
 * @param {Function}
 * @api public
 */

exports.serialize = function serialize(fn){
  this._serializer = fn;
  return this;
};

/**
 * Set timeout to `ms`.
 *
 * @param {Number} ms
 * @return {Request} for chaining
 * @api public
 */

exports.timeout = function timeout(ms){
  this._timeout = ms;
  return this;
};

/**
 * Promise support
 *
 * @param {Function} resolve
 * @param {Function} reject
 * @return {Request}
 */

exports.then = function then(resolve, reject) {
  if (!this._fullfilledPromise) {
    var self = this;
    this._fullfilledPromise = new Promise(function(innerResolve, innerReject){
      self.end(function(err, res){
        if (err) innerReject(err); else innerResolve(res);
      });
    });
  }
  return this._fullfilledPromise.then(resolve, reject);
}

exports.catch = function(cb) {
  return this.then(undefined, cb);
};

/**
 * Allow for extension
 */

exports.use = function use(fn) {
  fn(this);
  return this;
}


/**
 * Get request header `field`.
 * Case-insensitive.
 *
 * @param {String} field
 * @return {String}
 * @api public
 */

exports.get = function(field){
  return this._header[field.toLowerCase()];
};

/**
 * Get case-insensitive header `field` value.
 * This is a deprecated internal API. Use `.get(field)` instead.
 *
 * (getHeader is no longer used internally by the superagent code base)
 *
 * @param {String} field
 * @return {String}
 * @api private
 * @deprecated
 */

exports.getHeader = exports.get;

/**
 * Set header `field` to `val`, or multiple fields with one object.
 * Case-insensitive.
 *
 * Examples:
 *
 *      req.get('/')
 *        .set('Accept', 'application/json')
 *        .set('X-API-Key', 'foobar')
 *        .end(callback);
 *
 *      req.get('/')
 *        .set({ Accept: 'application/json', 'X-API-Key': 'foobar' })
 *        .end(callback);
 *
 * @param {String|Object} field
 * @param {String} val
 * @return {Request} for chaining
 * @api public
 */

exports.set = function(field, val){
  if (isObject(field)) {
    for (var key in field) {
      this.set(key, field[key]);
    }
    return this;
  }
  this._header[field.toLowerCase()] = val;
  this.header[field] = val;
  return this;
};

/**
 * Remove header `field`.
 * Case-insensitive.
 *
 * Example:
 *
 *      req.get('/')
 *        .unset('User-Agent')
 *        .end(callback);
 *
 * @param {String} field
 */
exports.unset = function(field){
  delete this._header[field.toLowerCase()];
  delete this.header[field];
  return this;
};

/**
 * Write the field `name` and `val`, or multiple fields with one object
 * for "multipart/form-data" request bodies.
 *
 * ``` js
 * request.post('/upload')
 *   .field('foo', 'bar')
 *   .end(callback);
 *
 * request.post('/upload')
 *   .field({ foo: 'bar', baz: 'qux' })
 *   .end(callback);
 * ```
 *
 * @param {String|Object} name
 * @param {String|Blob|File|Buffer|fs.ReadStream} val
 * @return {Request} for chaining
 * @api public
 */
exports.field = function(name, val) {

  // name should be either a string or an object.
  if (null === name ||  undefined === name) {
    throw new Error('.field(name, val) name can not be empty');
  }

  if (isObject(name)) {
    for (var key in name) {
      this.field(key, name[key]);
    }
    return this;
  }

  // val should be defined now
  if (null === val || undefined === val) {
    throw new Error('.field(name, val) val can not be empty');
  }
  this._getFormData().append(name, val);
  return this;
};

/**
 * Abort the request, and clear potential timeout.
 *
 * @return {Request}
 * @api public
 */
exports.abort = function(){
  if (this._aborted) {
    return this;
  }
  this._aborted = true;
  this.xhr && this.xhr.abort(); // browser
  this.req && this.req.abort(); // node
  this.clearTimeout();
  this.emit('abort');
  return this;
};

/**
 * Enable transmission of cookies with x-domain requests.
 *
 * Note that for this to work the origin must not be
 * using "Access-Control-Allow-Origin" with a wildcard,
 * and also must set "Access-Control-Allow-Credentials"
 * to "true".
 *
 * @api public
 */

exports.withCredentials = function(){
  // This is browser-only functionality. Node side is no-op.
  this._withCredentials = true;
  return this;
};

/**
 * Set the max redirects to `n`. Does noting in browser XHR implementation.
 *
 * @param {Number} n
 * @return {Request} for chaining
 * @api public
 */

exports.redirects = function(n){
  this._maxRedirects = n;
  return this;
};

/**
 * Convert to a plain javascript object (not JSON string) of scalar properties.
 * Note as this method is designed to return a useful non-this value,
 * it cannot be chained.
 *
 * @return {Object} describing method, url, and data of this request
 * @api public
 */

exports.toJSON = function(){
  return {
    method: this.method,
    url: this.url,
    data: this._data,
    headers: this._header
  };
};

/**
 * Check if `obj` is a host object,
 * we don't want to serialize these :)
 *
 * TODO: future proof, move to compoent land
 *
 * @param {Object} obj
 * @return {Boolean}
 * @api private
 */

exports._isHost = function _isHost(obj) {
  var str = {}.toString.call(obj);

  switch (str) {
    case '[object File]':
    case '[object Blob]':
    case '[object FormData]':
      return true;
    default:
      return false;
  }
}

/**
 * Send `data` as the request body, defaulting the `.type()` to "json" when
 * an object is given.
 *
 * Examples:
 *
 *       // manual json
 *       request.post('/user')
 *         .type('json')
 *         .send('{"name":"tj"}')
 *         .end(callback)
 *
 *       // auto json
 *       request.post('/user')
 *         .send({ name: 'tj' })
 *         .end(callback)
 *
 *       // manual x-www-form-urlencoded
 *       request.post('/user')
 *         .type('form')
 *         .send('name=tj')
 *         .end(callback)
 *
 *       // auto x-www-form-urlencoded
 *       request.post('/user')
 *         .type('form')
 *         .send({ name: 'tj' })
 *         .end(callback)
 *
 *       // defaults to x-www-form-urlencoded
 *      request.post('/user')
 *        .send('name=tobi')
 *        .send('species=ferret')
 *        .end(callback)
 *
 * @param {String|Object} data
 * @return {Request} for chaining
 * @api public
 */

exports.send = function(data){
  var obj = isObject(data);
  var type = this._header['content-type'];

  // merge
  if (obj && isObject(this._data)) {
    for (var key in data) {
      this._data[key] = data[key];
    }
  } else if ('string' == typeof data) {
    // default to x-www-form-urlencoded
    if (!type) this.type('form');
    type = this._header['content-type'];
    if ('application/x-www-form-urlencoded' == type) {
      this._data = this._data
        ? this._data + '&' + data
        : data;
    } else {
      this._data = (this._data || '') + data;
    }
  } else {
    this._data = data;
  }

  if (!obj || this._isHost(data)) return this;

  // default to json
  if (!type) this.type('json');
  return this;
};

},{"./is-object":11}],13:[function(require,module,exports){
// The node and browser modules expose versions of this with the
// appropriate constructor function bound as first argument
/**
 * Issue a request:
 *
 * Examples:
 *
 *    request('GET', '/users').end(callback)
 *    request('/users').end(callback)
 *    request('/users', callback)
 *
 * @param {String} method
 * @param {String|Function} url or callback
 * @return {Request}
 * @api public
 */

function request(RequestConstructor, method, url) {
  // callback
  if ('function' == typeof url) {
    return new RequestConstructor('GET', method).end(url);
  }

  // url first
  if (2 == arguments.length) {
    return new RequestConstructor('GET', method);
  }

  return new RequestConstructor(method, url);
}

module.exports = request;

},{}]},{},[1])(1)
});