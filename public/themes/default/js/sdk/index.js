'use strict';
var Deferred = require('deferred-js');
var superagent = require('./myagent');
var jwtDecode = require('jwt-decode');
var Cookies = require('js-cookie');
var store = require('store');

class Couplet {
  constructor(base) {
    this.base = base;
    this.User = new CoupletUser(this);
    this.Query = CoupletQuery;
    this.members = new CoupletMembers(this);
    this.chat = new CoupletChat(this);
	
	//superagent.set({'csrftoken': Laravel.csrfToken});
  }
  
  call(method, url, data) {
	var defer = new Deferred();
		
	var req = superagent(method, this.base + url);
	if(method == 'POST') {
		req.send(data);
	}	
	if(method == 'PUT') {
		req.send(data);
	}	
	if(method == 'GET') {
		req.query(data);
	}
	req.end(function (err, res) {
		console.log('err + err', err);
		console.log('res', err, res);
		if (err || !res.ok) {
			defer.reject(res.body);
		} else {
			defer.resolve(res.body);
		}
	});
	
	return defer.promise();
  }
  
  request(method, url, data) {
		
		data = typeof data !== 'undefined' ? data : {};
		var defer = new Deferred();
		var self = this;
		var needs_refreshing = false;

		if(Cookies.get('couplet_token')) {
			var decoded = jwtDecode(Cookies.get('couplet_token'));
			
			var currentDate = new Date();
			currentDate.setTime(currentDate.getTime() - 1*60*1000);
			console.log('jwt_decode', decoded.nbf, (currentDate/1000), decoded.nbf - (currentDate/1000), decoded.nbf > (currentDate/1000));
			needs_refreshing = decoded.nbf < (currentDate/1000);
		}
		//var needs_refreshing = false;
		if(needs_refreshing) {
			console.log("Needs refreshing");
						
			self.User.refresh(method, url, data).then(function(res){
				
				self.call(method, url, data).then(function(res){
					defer.resolve(res);
				}, function(res){
					defer.reject(res);
				});
			
			}, function(res){
				defer.reject(res);
			});
			
		} else {
			
			self.call(method, url, data).then(function(res){
				defer.resolve(res);
			}, function(res){
				defer.reject(res);
			});
			
			
		}
	
		return defer.promise();

  }
  
}

class CoupletMembers {
	
	constructor(couplet) {
		this.couplet = couplet;
		this.params = [];
		this.limit = 10;
		this.page = 10;
	}
	
	setLimit(limit) {
		this.limit = limit;
	}	
	
	setPage(page) {
		this.page = page;
	}
		
	setQuery(params) {
		this.params = params;
	}
	
	get(id) {
		var defer = new Deferred();
		console.log(this.params);
		superagent('GET', this.couplet.base + 'members/' + id)
			.end(function (err, res) {
				if (err || !res.ok) {
					defer.reject(res.body);
				} else {
					defer.resolve(res.body);
				}
			});
		return defer.promise();
	}
	
	getLatestMembers(limit) {
		var defer = new Deferred();
		this.couplet.request('GET', 'members/latest/' + limit).then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();
	}	
	
	markPrivateMessageAsRead(memberId) {
		
		var defer = new Deferred();
		this.couplet.request('GET', 'members/mark-read/' + memberId).then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();

	}	
	
	sendPrivateMessage(message, memberId) {
		var defer = new Deferred();
		this.couplet.request('POST', 'members/send-message/' + memberId, {'message' : message }).then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();
	}

	getPhotos(id) {	  
		var defer = new Deferred();
		this.couplet.request('GET', 'members/' + id + '/photos').then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();
	}
  
	isLiked(memberId) {
		var defer = new Deferred();
		this.couplet.request('GET', 'members/liked/' + memberId).then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();
	}
		
	like(memberId) {
		
		var defer = new Deferred();
		this.couplet.request('GET', 'members/like/' + memberId).then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();
		
	}
	
	find() {
		
		var defer = new Deferred();
		this.couplet.request('POST', 'members', {'params' : this.params, limit:this.limit, page:this.page}).then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();
		
	}
	
}

class CoupletQuery {
	
	constructor(CoupletObject) {
		this.couplet = couplet;
		this.params = [];
	}
	
	equalTo(field, value) {
		this.params.push([field, value, 'eq']);
	}
	
	containsAll(field, value) {
		this.params.push([field, value, 'contains']);
	}	
	
	containedIn(field, value) {
		this.params.push([field, value, 'in']);
	}	
	
	greaterThan(field, value) {
		this.params.push([field, value, 'gt']);
	}	
	
	lessThan(field, value) {
		this.params.push([field, value, 'lt']);
	}
	
	lessThanOrEqualTo(field, value) {
		this.params.push([field, value, 'lte']);
	}
	
	greaterThanOrEqualTo(field, value) {
		this.params.push([field, value, 'gte']);
	}
	
	getParams() {
		return this.params;
	}

}

class CoupletUser {
  constructor(couplet) {
    this.couplet = couplet;
    this.user = null;
  }

  refresh() {
	  var defer = new Deferred();
	  self = this;
	  	superagent('POST',  this.couplet.base + 'users/refresh')
		.end(function (err, res) {
			if (err || !res.ok) {
				defer.reject(res.body);
			} else {
				console.log('res.header');
				console.log('res.header', res.header.authorization.replace("Bearer ", ""));
				self.become(res.header.authorization.replace("Bearer ", ""));
				defer.resolve(res.body);
			}
		});
	return defer.promise();
  }
  
  become(token) {
	var defer = new Deferred();

    superagent.set({'Authorization': 'Bearer ' + token});
	var decoded = jwtDecode(token);
	console.log('become', decoded);
	
	Cookies.set('couplet_token', token, { expires: 365 });
    this.token = token;
	
	defer.resolve(true);
	return defer.promise();
  }

  current() {
	var token = Cookies.get('couplet_token');
	superagent.set({'Authorization': 'Bearer ' + token});
	return store.get('couplet_user');
  }

  clear() {
	  store.remove('couplet_user');
	  Cookies.remove('couplet_token');
  }
  
  store(user) {
	  store.set('couplet_user', user);
  }

  
  getThreadMessages(id) {
	var defer = new Deferred();
	this.couplet.request('GET', 'users/thread-messages/' + id).then(function(res){
		defer.resolve(res);
	}, function(res){
		defer.reject(res);
	})
	return defer.promise();
  } 
  
  getSentThreads(data) {
	  	var defer = new Deferred();
		  this.couplet.request('GET', 'users/sent-threads', data).then(function(res){
			defer.resolve(res);
	  }, function(res){
			defer.reject(res);
	  })
	return defer.promise();
  }  
  getThreads(data) {
	  	var defer = new Deferred();
		  this.couplet.request('GET', 'users/threads', data).then(function(res){
			defer.resolve(res);
	  }, function(res){
			defer.reject(res);
	  })
	return defer.promise();
  } 
  
  getLatestMessages(data) {
	  	var defer = new Deferred();
		  this.couplet.request('GET', 'users/latest-messages', data).then(function(res){
			defer.resolve(res);
	  }, function(res){
			defer.reject(res);
	  })
	return defer.promise();
  }

  getUnreadMessages(data) {
	  	var defer = new Deferred();
		  this.couplet.request('GET', 'users/unread', data).then(function(res){
			defer.resolve(res);
	  }, function(res){
			defer.reject(res);
	  })
	return defer.promise();
  }
  
  uploadAvatar(data) {
	var defer = new Deferred();
		  this.couplet.request('POST', 'users/upload-avatar', data).then(function(res){
			defer.resolve(res);
	  }, function(res){
			defer.reject(res);
	  })
	return defer.promise();
  }
  
  getPhotos(data) {	  
	var defer = new Deferred();
	this.couplet.request('GET', 'users/photos').then(function(res){
		defer.resolve(res);
	}, function(res){
		defer.reject(res);
	})
	return defer.promise();
  }
  
  deletePhoto(id) {
	  
	var defer = new Deferred();
	this.couplet.request('DELETE', 'users/photos/' + id).then(function(res){
		defer.resolve(res);
	}, function(res){
		defer.reject(res);
	})
	return defer.promise();
  }
  
  uploadPhoto(data) {
	  
	var defer = new Deferred();
	this.couplet.request('POST', 'users/upload-photo', data).then(function(res){
		defer.resolve(res);
	}, function(res){
		defer.reject(res);
	})
	return defer.promise();
	
  } 
  
  save(data) {
	  
	var defer = new Deferred();
	
	
		  var defer = new Deferred();
	  var self = this;
	  console.log(data);
	  this.couplet.request('PUT', 'users/me', data).then(function(res){
			defer.resolve(res);
	  }, function(res){
			defer.reject(res);
	  })
	  return defer.promise();
	
  }
  	getMatches() {
		var defer = new Deferred();
		this.couplet.request('GET', 'users/matches').then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();
	}  	getMembersYouLiked() {
		var defer = new Deferred();
		this.couplet.request('GET', 'users/liked').then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();
	}
	
	getMembersWhoLikedYou() {
		var defer = new Deferred();
		this.couplet.request('GET', 'users/liked-you').then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();
	}
		
	getMembersWhoInteractedWithYou() {
		var defer = new Deferred();
		this.couplet.request('GET', 'users/interactions').then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();
	}
	
  fetch() {
	  var defer = new Deferred();
	  var self = this;
	  this.couplet.request('GET', 'users/me').then(function(res){
		  self.store(res);
		  defer.resolve(res);
	  }, function(res){
		  self.clear();
		  defer.reject(res);
	  })
	  return defer.promise();
  }

  signUp(data) {
	  var defer = new Deferred();
	  var self = this;
	  this.couplet.call('POST', 'users/register', data).then(function(res){
			defer.resolve(res);
	  }, function(res){
			defer.reject(res);
	  })
	  return defer.promise();
  }
  
  logIn(email, password) {
	  var defer = new Deferred();
	  var self = this;
	  console.log({ email: email, password: password });
	  this.couplet.call('GET', 'users/login', { email: email, password: password }).then(function(res){
			self.become(res.token);
			self.fetch();
			defer.resolve(res);
	  }, function(res){
			defer.reject(res);
	  })
	  return defer.promise();
  }
  
  logOut() {
    var defer = new Deferred();
    var self = this;
	Cookies.remove('couplet_token');
	this.couplet.call('GET', 'users/logout').then(function(res){
		self.clear();
		defer.resolve(res);
	}, function(res){
		defer.reject(res);
	})
    return defer.promise();
  }
  
  stripePayment(token, interval) {
	var defer = new Deferred();
	var self = this;
	this.couplet.call('POST', 'stripe/after-payment', {token:token, interval:interval}).then(function(res){
		defer.resolve(res);
	}, function(res){
		defer.reject(res);
	})
	return defer.promise();
  }
  
  requestPasswordReset(email) {
	var defer = new Deferred();
	var self = this;
	this.couplet.call('POST', 'users/reset-password', {email:email}).then(function(res){
		defer.resolve(res);
	}, function(res){
		defer.reject(res);
	})
	return defer.promise();
  }
  
}


class CoupletObject {
	
	constructor(couplet) {
		this.couplet = couplet;
		this.object = object;
		this.className = couplet.toUnderscore();
		superagent.set({'csrftoken': Laravel.csrfToken});
	}
	
	toUnderscore(name) {
		return name.replace(/([A-Z])/g, function($1){return "_"+$1.toLowerCase();});
	}
	
	get(id) {
		var self = this;
		var defer = new Deferred();
		
		superagent('GET',  this.couplet.base + ''+className+'/' + id)
		  .end(function (err, res) {
			if (err || !res.ok) {
			  defer.reject(res.body);
			} else {
			  //save the jwt token
			  //fetch the user
			  defer.resolve(res.body);
			}
		  });
		return defer.promise();
	}
	
}



class CoupletChat {
	
	constructor(couplet) {
		this.couplet = couplet;
	}
	
	sendMessage(message, memberId) {
		var defer = new Deferred();
		this.couplet.request('POST', 'chat/send-message/' + memberId, {'message' : message }).then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();
	}
		
	getMessages(memberId) {
		var defer = new Deferred();
		this.couplet.request('GET', 'chat/messages/' + memberId).then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();
	}	
	
	getUnreadMessages(memberId) {
		var defer = new Deferred();
		this.couplet.request('GET', 'chat/unread').then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();
	}
	
	getChats() {
		var defer = new Deferred();
		this.couplet.request('GET', 'chat').then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();
	}
	
		
	markRead(memberId) {
		var defer = new Deferred();
		this.couplet.request('GET', 'chat/mark-read/' + memberId).then(function(res){
			defer.resolve(res);
		}, function(res){
			defer.reject(res);
		})
		return defer.promise();
	}	
	
}

module.exports = Couplet;