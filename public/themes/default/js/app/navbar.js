Vue.component('navbar', {
  template: '#navbar-template',
  props: ['currentUser'],
  data: function () {
    return {   }
  },
  methods: {
    login: function (event) {
      var self = this;
    Parse.User.logIn("test", "test", {
      success: function(user) {
        // Do stuff after successful login.
        console.log(user);
        self.$dispatch('login');

      },
      error: function(user, error) {
        // The login failed. Check error to see why.
        console.log(user, error);
      }
    });
      return false;
    },
    logout: function (event) {
      Parse.User.logOut().then(() => {
        redirect_url('/');
        this.$dispatch('logout');

      });
    }
  }
});