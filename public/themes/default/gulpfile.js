var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require("gulp-uglify");
const babel = require('gulp-babel');
var webpack = require('gulp-webpack');
var less = require('gulp-less');
var path = require('path');
var cleanCSS = require('gulp-clean-css');
var minify = require('gulp-minify');
var gutil = require('gulp-util');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');
var tap = require('gulp-tap');
var run = require('gulp-run');

gulp.task('vue', function() {
	gulp.src('./js/vue.js')
	.pipe(babel({
		presets: ['es2015']
	}))
	//.pipe(uglify())
	.pipe(concat('compiled.js'))
	.pipe(gulp.dest('./js'));
});

gulp.task('sdk', function() {
	return run('browserify js/sdk/index.js -s Couplet -o js/sdk.js -t [ babelify --presets [ es2015 ] ]').exec().pipe(gulp.dest('output'));
		/*gulp.src('sdk.js')
		.pipe(babel({
			presets: ['es2015']
		}))
		//.pipe(uglify())
		.pipe(concat('compiled.js'))
		.pipe(gulp.dest('./js'));*/
});

gulp.task('js', function() {
  return gulp.src([
		'node_modules/blueimp-load-image/js/load-image.all.min.js',
		'node_modules/jquery/dist/jquery.js',
		'node_modules/opentip/downloads/opentip-jquery.js',
		'node_modules/superagent/superagent.js',
		'node_modules/lodash/lodash.js',
		'node_modules/bootstrap/dist/js/bootstrap.js',
		'node_modules/vue/dist/vue.js',
		'node_modules/vue-router/dist/vue-router.js',
		'node_modules/vue-select/dist/vue-select.js',
		'node_modules/toastr/toastr.js',
		'node_modules/nprogress/nprogress.js',
		'node_modules/moment/moment.js',
		'node_modules/vue-select/vue-select.js',
		'node_modules/vue-msgbox/vue-msgbox.js',
		'node_modules/sweetalert2/dist/sweetalert2.js',
		'node_modules/fancybox/dist/js/jquery.fancybox.js',
		'node_modules/fancybox/dist/helpers/js/jquery.fancybox-media.js',
		'node_modules/fancybox/dist/helpers/js/jquery.fancybox-buttons.js',
		'node_modules/places.js/dist/places.js',
		'js/vue-plugins/vue-moment.js',
		'js/vue-plugins/vue-file-upload-component.js',
		'js/libs/parse-names.js',
		//'public/js/compiled.js',
	])
	/*.pipe(babel({
		presets: ['es2015']
    }))*/
    .pipe(concat('app.js'))
	/*.pipe(uglify({
        mangle: false,

    }))*/
    .pipe(gulp.dest('./js/'));
});

gulp.task('bootstrap', function() {
	gutil.log('compiling bootstrap...');
	return gulp.src('./less/bootstrap.less')
		.pipe(less())
		.pipe(gulp.dest('./css'));
});

gulp.task('theme', function() {
	gutil.log('compiling theme...');
	return gulp.src('./less/theme.less')
		.pipe(less())
		.pipe(gulp.dest('./css'));
});

gulp.task('css', function() {
	gutil.log('compiling css...');
	return gulp.src([
		'css/bootstrap.css',
		'css/theme.css',
		'node_modules/opentip/css/opentip.css',
		'node_modules/toastr/build/toastr.css',
		'node_modules/nprogress/nprogress.css',
		'node_modules/sweetalert2/dist/sweetalert2.css',
		'node_modules/fancybox/dist/css/jquery.fancybox.css',
		'node_modules/fancybox/dist/helpers/css/jquery.fancybox-thumbs.css',
		'node_modules/fancybox/dist/helpers/css/jquery.fancybox-buttons.css',
	])
    .pipe(concat('app.css'))
	//.pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./css/'));	
});

gulp.task('default',['bootstrap', 'theme', 'css', 'vue', 'sdk', 'js']);

