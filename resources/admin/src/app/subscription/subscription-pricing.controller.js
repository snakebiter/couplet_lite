'use strict';

angular.module('ngAdmin')
    .controller('SubscriptionPricingCtrl', function ($scope, ContentService, $uibModal, $stateParams, SettingsService) {

    window.scope = $scope;
    $scope.settings = {};
    

    $scope.saveDetails = function() {
        var n = noty({text: 'Saving...', type:'warning', timeout: 2000});
        SettingsService.save($scope.settings).then(function(data) {
            var n = noty({text: 'Saved', type:'warning', timeout: 2000});
        }, function(response) {
            var n = noty({text: 'Error saving!', type:'danger', timeout: 2000});
        });
    }

    $scope.init = function() {
        console.log("GET CONTENT RESTFULL");
        $scope.showLoader();

        SettingsService.getArray().then(function(response) {
			$scope.settings = response;			
			$scope.hideLoader();
		}, function(response) {
			$scope.hideLoader();
		});

        
    };
    
    $scope.init();
});
