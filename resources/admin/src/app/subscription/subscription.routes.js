'use strict';

angular.module('ngAdmin')
.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('app.subscription', {
        url: '/subscription',
        abstract: true,
        templateUrl: 'app/viewer.html',
        data: {
            requireLogin: true
        }
    })  
    .state('app.subscription.pricing', {
        url: '/pricing',
        templateUrl: 'app/subscription/subscription-pricing.html',
        controller: 'SubscriptionPricingCtrl',
        data: {
            requireLogin: true
        }
    })
    .state('app.subscription.index', {
        url: '/index',
        templateUrl: 'app/subscription/subscription.html',
        controller: 'SubscriptionCtrl',
        data: {
            requireLogin: true
        }
    });

});
