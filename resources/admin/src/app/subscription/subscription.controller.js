'use strict';

angular.module('ngAdmin')
    .controller('SubscriptionCtrl', function ($scope, ContentService, $uibModal, $stateParams, SettingsService, TransactionService) {

    window.scope = $scope;
    $scope.settings = {};
    $scope.payments = [];
    
    window.scope = $scope;
    $scope.themeList = [];
    $scope.currentPage = 1;
    $scope.totalItems = 10;
    $scope.currentPage = 1;
    $scope.maxSize = 5;
	
    $scope.getTransactions = function() {
		$scope.showLoader();
        TransactionService.getList({page: $scope.currentPage}).then(function(transactions) {
			console.log(transactions.meta);
            $scope.payments = transactions;
			$scope.pageInfo = transactions.meta;
            $scope.currentPage = transactions.meta.current_page;
            $scope.totalItems = transactions.meta.total;
			
			$scope.hideLoader();
        }, function(response) {
            $scope.hideLoader();
        });
    }
		   
	$scope.pageChanged = function() {
        console.log('Page changed to: ' + $scope.currentPage);
		$scope.getTransactions();
    };
	
    $scope.saveDetails = function() {
        var n = noty({text: 'Saving...', type:'warning', timeout: 2000});
        $scope.setting_object.save($scope.settings, {
            success: function (response) {
                var n = noty({text: 'Saved', type:'warning', timeout: 2000});
            },
            error: function (response, error) {
                var n = noty({text: 'Error saving!', type:'danger', timeout: 2000});
            }
        });
    }

    $scope.init = function() {        
        $scope.getTransactions();
    };
    
    $scope.init();
});
