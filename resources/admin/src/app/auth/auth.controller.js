'use strict';

angular.module('ngAdmin')
  .controller('AuthCtrl', function ($scope, $auth, $state, $http, locker, $rootScope) {
    $scope.loginForm = {};
    $scope.setPageClass('auth');

    window.auth = $auth;
    window.state = $state;

    $scope.init = function() {

      
      /*if($scope.user.signedIn) {
        $state.go('app');
      }*/
    };
    $scope.init();

    $scope.loading = false;
    $scope.submitLogin = function() {

      $scope.invalidLogin = false;
      $scope.loading = true;
       
	    // Use Satellizer's $auth service to login
      console.log($scope.loginForm);
        $auth.login($scope.loginForm).then(function(payload) {

            $http.get(API_URL + 'auth/details').success(function(userInfo) {
              console.log(userInfo);
                locker.put('userInfo', userInfo);
                $rootScope.authenticated = true;
                $rootScope.user = userInfo;

                $state.go('app.members.index');

            }).error(function(error) {
                $scope.loading = false;
                $scope.showError = true;
                $scope.errorMessage = error;
            });

            
          // handle success response
          console.log(payload);
          $scope.loading = false;
        }).catch(function(response) {
          // handle error response
          console.log(response);
          $scope.invalidLogin = true;
          $scope.loading = false;
        });

    };

});
