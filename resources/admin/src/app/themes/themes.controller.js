'use strict';

angular.module('ngAdmin')
    .controller('ThemesCtrl', function ($scope, ContentService, $uibModal, $stateParams, SettingsService, ThemeService) {

    window.scope = $scope;
    $scope.themeList = [];
    
    $scope.getThemes = function() {
		$scope.showLoader();
        ThemeService.getList().then(function(data) {
            $scope.themeList = data;
			$scope.hideLoader();
        }, function(response) {
            $scope.hideLoader();
        });
    }
	
    $scope.setTheme = function(key) {
        var n = noty({text: 'Saving...', type:'warning', timeout: 2000});
        SettingsService.save({'theme' : key}).then(function(data) {
            $scope.settings.theme = key;		
            var n = noty({text: 'Saved', type:'warning', timeout: 2000});
        }, function(response) {
            var n = noty({text: 'Error saving!', type:'danger', timeout: 2000});
        });
    }
	
    $scope.init = function() {
        console.log("GET CONTENT RESTFULL");
        $scope.getThemes();
                         
		SettingsService.getArray().then(function(response) {
			$scope.settings = response;			
			$scope.hideLoader();
		}, function(response) {
			$scope.hideLoader();
		});
        
    };
    
    $scope.init();
});
