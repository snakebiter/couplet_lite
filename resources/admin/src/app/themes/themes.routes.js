'use strict';

angular.module('ngAdmin')
.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('app.themes', {
        url: '/themes',
        abstract: true,
        templateUrl: 'app/viewer.html',
        data: {
            requireLogin: true
        }
    })  
    .state('app.themes.index', {
        url: '/index',
        templateUrl: 'app/themes/themes.html',
        controller: 'ThemesCtrl',
        data: {
            requireLogin: true
        }
    });

});
