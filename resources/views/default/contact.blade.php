@extends('layouts.app')
@section('title', __("Contact us"))
@section('content')

    <br />
    <br />

    <div class="container">
        <div class="row">


            <div class="col-md-8 col-md-offset-2">

                <h1>
                    <?= __('Contact us') ?>
                </h1>
                <hr />
				<br />
				<br />
				<? if (session('success')) : ?>
					<div class="alert alert-info"><?= __("Successfully sent") ?></div>
				<? endif; ?>
                <!-- Headings & Paragraph Copy -->
                <div class="row">

                    <div class="col-md-12">
				<form method="POST" action="<?= url('contact') ?>" accept-charset="UTF-8" class="form">			
				<fieldset>
					<div class="form-group">
						
						<div class="row">
							<div class="col-sm-12">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label for="exampleInputEmail1"><?= __('First name') ?></label>
											<input class="form-control" name="first_name" type="text" value="">										</div>
									</div>
									
									<div class="col-sm-6">
										<div class="form-group">
											<label for="exampleInputEmail1"><?= __('Last name') ?></label>
											<input class="form-control" name="last_name" type="text" value="">										</div>
									</div>
								</div>
								<br />								
								
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label for="exampleInputEmail1"><?= __('Email') ?></label>
											<input class="form-control" name="email_address" type="email" value="" required>										
										</div>
									</div>
									
									<div class="col-sm-6">
										<div class="form-group">
											<label for="exampleInputEmail1"><?= __('Phone number') ?></label>
											<input class="form-control" name="phone_number" type="text" value="">										
										</div>
									</div>
								</div>				
								<br />
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label for="exampleInputEmail1"><?= __('Your message') ?></label>
											<textarea class="form-control" name="comment" cols="50" rows="10" required></textarea>										
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-12">
							<input name="url" type="hidden" value="">							<input name="report_type" type="hidden">														<input class="btn btn-primary pull-right" type="submit" value="Send">						</div>
					</div>
					
				</div>
				
			</div>
		</div>
		

                    </div>


                    <!-- Misc Elements -->

                </div>
                <!-- /row -->

            </div>

        </div>
    </div>

                        
@endsection