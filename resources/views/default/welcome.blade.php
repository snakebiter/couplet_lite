@extends('layouts.app')

@section('content')

            <div class="jumbotron home-search" style="">
    <div class="overlday">
        <div class="container">
            <div class="row">

                <div class="col-md-8 col-sm-6">
                    <div class="row">
                        <div class="col-sm-8">
                            <h1 class="hidden-sm">
                                <?= __("<strong>Join now</strong> and find your <strong>match</strong> today.<br />It's <strong>totally free!</strong>") ?><br />
                            </h1>
							<h2 class="visible-sm">
                                <?= __("<strong>Join now</strong> and find your <strong>match</strong> today.<br />It's <strong>totally free!</strong>") ?><br />
                            </h2>
                            <br />
                            <p>
                                <?= __('Set up your own personal profile, start messaging people and find your perfect match today') ?>
                            </p>
                        </div>
                    </div>


                    <br />
                    <br />
                    <br />
                    <br />

                    <div class="row visible-md visible-lg">
                        <div class="col-sm-12">
                            <h2 class="writing"><?= __('Recently joined') ?></h2>
                        </div>

                        <latest-members></latest-members>
                        
                    </div>



                </div>


                <div class="col-md-4 col-sm-6 pull-right" >

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <registration-form></registration-form>

                        </div>
                    </div>

                </div>        

            </div>
        </div>
    </div>
</div>

<div class=" quotes">
    <div class="container">


        <div class="row">

            <div class="col-xs-4">
                <blockquote>
                    <p>"The simplest dating site to use"</p>
                    <footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
                </blockquote>
            </div>

            <div class="col-xs-4">
                <blockquote>
                    <p>"Fun to use and loads of people to meet"</p>
                    <footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
                </blockquote>
            </div>

            <div class="col-xs-4">
                <blockquote>
                    <p>"Met my match in minutes"</p>
                    <footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
                </blockquote>
            </div>

        </div>

    </div>
</div>
@endsection
