@extends('layouts.app')
@section('title', __("%s's profile", $user->display_name))
@section('content')


    <div class="container" id="profile" data-member="<?= $user->id ?>">

    <br />
    <div class="row">
        <div class="col-sm-12">
            <ol class="breadcrumb">
                <li><a href="<?= url('/') ?>"><?= __("Home") ?></a></li>
                <li><a href="<?= url('/members') ?>"><?= __("Members") ?></a></li>
                <li class="active"><?= $user->display_name ?></li>
            </ol>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-3  hidden-xs  hidden-sm  hidden-md">
            <div class="panel panel-default" style="min-height: 340px;">
                <div class="panel-body" v-cloak>


                    <div class="col-sm-12">
                        <div class="imgholder plain">
                            <div class="outer1 circle"></div>
                            <div class="outer2 circle"></div>
                            <figure>
                                <img src="<?= asset($user->avatar) ?>" v-if="currentUser != null" />
                                <img src="<?= asset($user->avatar) ?>" v-if="currentUser == null" />
                            </figure>
                            
                        </div>
                    </div>
					
                    <div class="col-sm-12" v-if="false">
                        <div class="imgholder plain">
                            <div class="outer1 circle"></div>
                            <div class="outer2 circle"></div>
                            <figure>
                                <img src="<?= asset($user->avatar) ?>" v-if="currentUser != null" />
                                <img src="<?= asset($user->avatar) ?>" v-if="currentUser == null" />
                            </figure>
                            
                        </div>
                    </div>

                    <div class="col-sm-12 " style="text-align: center; margin-top: 20px;">
                        <h3 style="text-align: center;"><a class="" href=""><?= $user->display_name ?></a></h3>
                        <figcaption style="text-align: center;" class="caption"><?= $user->age ?>/<?= ($user->gender=="F")?'Female':'Male' ?>/<?= $user->city ?> (<?= $user->country ?>)</figcaption>
                        <br />
                        <a href="#" v-on:click="likeMember" class="btn btn-default btn-block hidden-sm hidden-md @{{(currentUser && profileUser && profileUser.id == currentUser.id)?'disabled':''}}" v-cloak >
                            <i class="fa @{{(profileIsLiked)?'fa-star':'fa-star-o'}}" aria-hidden="true"></i> @{{(profileIsLiked)?'Liked':'Like'}} </a><br />
                            <a href="#myModalMessage" data-toggle="modal" data-target="#myModalMessage" class="btn btn-default btn-block hidden-sm hidden-md @{{ (currentUser && profileUser && profileUser.id == currentUser.id)?'disabled':''}}" ><?= __('Send a private message') ?></a><br />
                    </div>

                </div>            
            </div>   
        </div>

        <div class="col-xs-12 col-lg-9 pull-right listings">



            <div class="wells">
                                <div class="row">
                    <div class="col-sm-12">
                        <div id="my-tab-content" class="tab-content">

                            <div class="tab-pane active" id="my_summary">

                                <h3 class="profile-title"><?= __("My summary") ?></h3>                                    <br />

                                
                                <div class="row">
                                <div class="col-sm-4 hidden-lg">

                                <div class="imgholder plain">
                            <div class="outer1 circle"></div>
                            <div class="outer2 circle"></div>
                            <figure>
                                <img src="<?= asset($user->avatar) ?>" />
                            </figure>
                            
                            

                        </div>
                                </div><div class="col-sm-8 col-lg-12">
                                <p><?= $user->summary ?></p>
                                <div class="visible-sm visible-xs visible-md">
                                    <br />
                                 <a href="#" v-on:click="likeMember" class="btn btn-default" v-cloak>
                            <i class="fa @{{(profileIsLiked)?'fa-star':'fa-star-o'}}" aria-hidden="true"></i> @{{(profileIsLiked)?'Liked':'Like'}} </a></a>
                       <a href="#myModalMessage" data-toggle="modal" data-target="#myModalMessage" class="btn btn-default"><?= __("Send a private message") ?></a><br /></div>
                                <br /><br /><br />
                                </div>
                                </div>
                                
                                

                                <div class="row">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="well">
                                            <div class="row">
                                                <div class="col-sm-1 hidden-xs" style="padding-right: 0;">
                                                    <i class="fa fa-quote-left fa-6" style="font-size: 32px"></i>
                                                </div>

                                                <div class="col-sm-10 col-xs-12">
                                                    <strong><?= __("Message me if:") ?></strong>
                                                    <br />
                                                    <?= ($user->why_message_me)?$user->why_message_me:__('You really really want to!') ?>
                                                </div>

                                                <div class="col-sm-1 hidden-xs" style="padding-left: 0;">
                                                    <i class="fa fa-quote-right fa-6" style="font-size: 32px"></i>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>  
                                </div>  
                                <br /><br />


                                <div class="row profile-bullets">




                                    <div class="col-md-12">
                                        <h3 class="profile-title"><?= __("Things you need to know") ?></h3>
                                        <br />
                                    </div>

                                    <div class="col-md-12">
                                        <ul class="3-column">
                                            <? foreach($user_properties as $user_property) : ?>
                                            <li><span class="text-muted"><?= $user_property['title'] ?>:</span> <?= (is_string($user_property['value']))?$user_property['value']:''; ?> </li>
                                            <? endforeach; ?>
                                        </ul>
                                    </div>

                                </div>



                            </div>



                        </div>

                    </div>
                </div>
            </div>
            <br />
            <br />

            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-tabs">
                        <li  class="active"><a href="#tab-profile" data-toggle="tab"><?= __("About me") ?></a></li>
                        <li><a href="#tab-gallery" data-toggle="tab" v-on:click="getImages"><?= __("Photo gallery") ?></a></li>
                    </ul>
                </div>
            </div>
            <br />


            <div id="my-tab-profile" class="tab-content">

                <div class="tab-pane active" id="tab-profile">


                    <div class="row">
                    <div class="col-md-8">

                        <div class="panel-group" id="accordion">
                            
                            <? foreach($profile_properties as $profile_property) : ?>      
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $profile_property['field'] ?>">
                                                <i class="fa fa-bookmark"></i> <?= $profile_property['title'] ?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?= $profile_property['field'] ?>" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <?= (isset($user[$profile_property['field']]))?$user[$profile_property['field']]:'Nothing here' ?>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-body">

                                <strong class="text-info"><?= __("My preferences") ?></strong><br /><br />



                                        <ul>
                                            <li><?= ($user->pref_gender=='women')?'Women':'Men' ?></li>
                                            <li><?= __('Ages') ?> <?= $user->pref_min_age ?> to <?= ($user->pref_max_age)?$user->pref_max_age:'-'  ?></li>
                                            <?if($user->pref_marital_status != 'anything') { ?>
                                            <li ><?= __('Who are') ?> <?= $user->pref_marital_status ?></li>
                                            <?} ?>
                                            <?if($user->pref_location == 'anywhere') { ?>
                                            <li><?= __('Living anywhere') ?></li>
                                            <?} ?>
                                            <?if($user->pref_location == 'nearby') { ?>
                                            <li><?= __('Near me') ?></li>
                                            <?} ?>
                                            <?if($user->pref_new_friends) { ?>
                                            <li><?= __('For new friends') ?></li>
                                            <?} ?>
                                            <?if($user->pref_long_term) { ?>
                                            <li><?= __('For long term relationships') ?></li>
                                            <?} ?>
                                            <?if($user->pref_short_term) { ?>
                                            <li><?= __('For short term relationships') ?></li>
                                            <?} ?>
                                            <?if($user->pref_casual_sex) { ?>
                                            <li><?= __('For casual sex') ?></li>
                                            <?} ?>
                                        </ul>


                            </div>
                        </div>

                    </div>
                    </div>

                </div>

                <div class="tab-pane" id="tab-gallery">


                    <div class="row">        
                        <div class="col-sm-12" style="min-height: 400px">

                                <div class="" v-if="!currentUser">

  <br />  
                        <br />  
<div class="row">
                        

                        <div class="col-sm-12 text-center" style="height: 110px">
                            <h3 class="text-center"><?= __('Oops!') ?></h3><br />
                            <a href="/"><?= __('You need to login') ?><br /><?= __('to view any photo gallery') ?></a>
                        </div>


                </div>
                </div>							

        <div v-if="settings.can_see_gallery == 'Yes'">
            <div class="" v-if="!currentUserIsLiked &&(currentUser && profileUser && currentUser.id != profileUser.id)">

                <br />  
                <br />  
                
                <div class="row">
                
                

                    <div class="col-sm-12 text-center" style="height: 110px">
                        <h3 class="text-center"><?= __('Oops!') ?></h3><br />
                        <a href="/account/dashboard"><?= __('This gallery is protected') ?><br /><?= $user->display_name ?> <?= __("must LIKE you to give you permissions") ?></a>
                    </div>


                </div>
            </div>
        </div>

        <div class="" v-if="currentUserIsLiked || settings.can_see_gallery == 'No'  || (currentUser && profileUser && currentUser.id == profileUser.id)">
                            <div class="row" v-if="galleryImages == null">
                                <div class="col-md-12 text-center">
                                    <p><?= __('Loading...') ?></p>
                                </div>
                            </div>


                            <div class="row" v-if="galleryImages != null && galleryImages.length == 0">
                                <div class="col-md-12 text-center">
                                    <p><?= __('This user as not uploaded any photos yet!') ?></p>
                                </div>
                            </div>

                            <div class="row" v-if="galleryImages != null && galleryImages.length > 0">
                                
                                <div class="col-md-2 col-sm-3 col-xs-6" v-for="galleryImage in galleryImages">
                                    <a href="@{{ galleryImage.image }}" class="fancybox" rel="group" >
                                        <img class="img-responsive" alt="" :src="galleryImage.thumbnail" style="width: 100%" />
                                    </a>
                                </div>
                                                               
                            </div>


                        </div>                    
                        </div>                    
                    </div>

                </div>





            </div>

        </div>

    </div>
</div>
                    
@endsection