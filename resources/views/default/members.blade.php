@extends('layouts.app')
@section('title', 'Members page')
@section('content')
    <br />

                    <template id="browse-members">

<div class="container">



    <div class="row">
        <div class="col-sm-3  hidden-xs">
            <br />
            <br />
            <br />
        @include('partials/search_sidebar')
</div>

        <div class="col-sm-9 pull-right listings">



            <div class="row">
                <div class="col-sm-12">
                    <div class="pull-right">
                        
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div class="row">

                    <div class="col-sm-12 text-center starting">
                        <img src="<?= asset('images/default.gif') ?>" />
                    </div>
					
                    <div class="col-sm-12 text-center" style="height: 110px" v-if="members && members.length == 0">
                        <h3 class="text-center"><?= __('Oops!') ?></h3><br />
                        <a href="" v-on:click.prevent="resetSearch()"><?= __('No results found') ?><br /><?= __('Try another search') ?></a>
					</div>
					
                    <div class="col-sm-4 col-xs-6" v-if="members" v-for="member in members">
                        <div class="panel panel-default" style="height: 300px;">
                            <div class="panel-body">


                                <div class="col-sm-12">
                                    <a class=""  href="<?= url('/profile/') ?>/@{{member.id}}">
                                        <br />
                                        <div class="imgholder plain">
                                            <div class="outer1 circle"></div>
                                            <div class="outer2 circle"></div>
                                            <figure>
                                                <img v-bind:src="avatar_image(member)" />
                                            </figure>
                                    </a>
											
									<a v-if="(((+new Date() - (+member.last_active)))/6000 < 60*2)" href="" style="color: #fff;"><span class="badge" style="background:#5cb85c;margin-left: 94px;"><?= __('online') ?></span></a>

                                    </div>
                                </div>

                                <div class="col-sm-12" style="text-align: center; margin-top: 20px;">
                                    <h3 style="text-align: center;"><a class=""  href="<?= url('/profile/') ?>/@{{member.id}}">@{{member.display_name}} </a></h3>
                                    <figcaption style="text-align: center;" class="caption">@{{calculate_age(member)}} / @{{(member.gender=='F')?'Female':'Male'}} / @{{member.city}}</figcaption>
                                    <br />
                                    <a href="<?= url('/profile/') ?>/@{{member.id}}" class="btn btn-default"><?= __('View profile') ?></a>
                                </div>

                            </div>
                        </div>
                    </div>


         
                            </div>
 <div class="row" v-if="total_pages > 1">
            <div class="col-sm-12"  >
                <ul class="pagination">
                    <li><a href="#" v-on:click.prevent="previousPage()"><?= __('Prev') ?></a></li>
                    <li class="@{{((p+1) == page)?'active':''}}" v-for="p in total_pages"><a href="#" v-on:click.prevent="search(p+1)">@{{p+1}}</a></li>
                    <li><a href="#" v-on:click.prevent="nextPage()"><?= __('Next') ?></a></li>
                </ul>
            </div>
            </div>

        </div>

    </div>
    </div>

                    </template>            
                <browse-members :current-user="currentUser"></browse-members>

@endsection