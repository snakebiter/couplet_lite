@extends('layouts.app')
@section('title', 'My Account')

@section('content')

     <br />
            <div class="container">

    <div class="row">

        <div class="col-sm-3">
                            <account-sidebar :current-user="currentUser"></account-sidebar>

       </div>
        

        <div class="col-sm-9">
                <router-view current-user.sync="currentUser" ></router-view>
        </div>

    </div>

@endsection