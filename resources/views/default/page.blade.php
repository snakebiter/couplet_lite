@extends('layouts.app')
@section('title', $page_translation->title)
@section('content')

    <br />
    <br />

    <div class="container">
        <div class="row">

            <div class="col-md-12">

                <h1>
                    <?= $page_translation->title ?>
                </h1>
                <hr />
                <!-- Headings & Paragraph Copy -->
                <div class="row">

                    <div class="col-md-12">
                        <?= $page_translation->content ?>
                    </div>


                    <!-- Misc Elements -->
                </div>
                <!-- /row -->

            </div>

        </div>
    </div>
@endsection