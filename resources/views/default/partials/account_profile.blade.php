<template id="account-profile">
    
            <div class="panel panel-default">
                <div class="panel-heading"><?= __('Dashboard') ?></div>
                <div class="panel-body">
                    <form class="form-horizontal" v-on:submit.prevent="updateForm" >
                        <fieldset>
                            <div class="form-group">

                                <div class="col-sm-7">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="inputEmail1" class="control-label"><?= __('My self-summary') ?></label>
                                            <textarea class="form-control" rows="4"  v-model="user['summary']" style="width: 99%"></textarea>
                                        </div>
                                    </div>                           

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="inputEmail1" class="control-label"><?= __('Why message me') ?></label>
                                            <textarea class="form-control" rows="4"  v-model="user['why_message_me']" style="width: 99%"></textarea>
                                        </div>
                                    </div>                           

                                    <div class="form-group" v-for="user_property in user_properties" v-if="user_property.input == 'textarea'">
                                        <div class="col-sm-12">
                                            <label for="inputEmail1" class="control-label">@{{ (settings[user_property.field] !== undefined)?settings[user_property.field]:user_property.title }}</label>
                                            <textarea class="form-control" rows="4"  v-model="user[user_property.field]" style="width: 99%"></textarea>
                                        </div>
                                    </div>                           

                                </div>
                                <div class="col-sm-5">

<br />

                                    <div class="well text-ridght" style="margin-top: 7px">
                                        <div class="text-left" >
                                            <a href="" class="text-sm" data-toggle="modal" data-target="#mySearch">
                                                <?= __("I'm looking for") ?> <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                        </div><br />
                                        

                                        <ul>
                                            <li>@{{(user.pref_gender=='women')?'Women':'Men'}}</li>
                                            <li><?= __("Ages") ?> @{{user.pref_min_age}} to @{{user.pref_max_age}}</li>
                                            <li v-if="user.pref_location == 'anywhere'"><?= __("Living anywhere") ?></li>
                                            <li v-if="user.pref_location == 'nearby'"><?= __("Near me") ?></li>
                                            <li v-if="user.pref_marital_status == 'single'"><?= __("Who are") ?> @{{user.pref_marital_status}}</li>
                                            <li v-if="user.pref_new_friends"><?= __("For new friends") ?></li>
                                            <li v-if="user.pref_long_term"><?= __("For long term relationships") ?></li>
                                            <li v-if="user.pref_short_term"><?= __("For short term relationships") ?></li>
                                            <li v-if="user.pref_casual_sex"><?= __("For casual sex") ?></li>
                                        </ul>


                                    </div>
<br />
                                <div class="well text-ridght">
                                    <div class="text-left">
                                        <a href="" class="text-sm"  data-toggle="modal" data-target="#mySettings"><?= __("Edit profile") ?> <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    </div>
                                    <br />

                                    <div class="form-group form-group-sm" style="margin-bottom: 0px;" v-for="user_property in user_properties" v-if="user_property.input != 'textarea'">

                                        <label for="inputEmail1" class="col-sm-6 control-label text-left">@{{ user_property.title }} @{{ user_property.show }}</label>
                                        <div class="col-sm-6">
                                            <p class="form-control-static" v-if="user_property.field != 'mother_tongue' && user_property.field != 'country_living_in' && user_property.field != 'country_grew_up_in' && user_property.field != 'ethnicities' && !profile_names[user_property.field]"> @{{ user[user_property.field] }} </p>
                                            <p class="form-control-static" v-if="user_property.field == 'ethnicities'"> @{{ user.ethnicities}} </p>
                                            <p class="form-control-static" v-if="user_property.field == 'country_living_in'"> @{{ country_name(user[user_property.field]) }} </p>
                                            <p class="form-control-static" v-if="user_property.field == 'country_grew_up_in'"> @{{ country_name(user[user_property.field]) }} </p>
                                            <p class="form-control-static" v-if="user_property.field == 'mother_tongue'"> @{{ language_name(user[user_property.field]) }} </p>
                                            <p class="form-control-static" v-if="user_property.field != 'mother_tongue' && user_property.field != 'country_living_in' && user_property.field != 'country_grew_up_in' && user_property.field != 'ethnicities' && profile_names[user_property.field]"> @{{ user[user_property.field]?profile_names[user_property.field][user[user_property.field]].name:'-' }}</p>
                                        </div>

                                    </div>	


                                </div>
                                </div>

                            </div>

                        

                            <br />
                            <button type="submit" class="btn btn-primary" v-if="!saving"><?= __("Save profile") ?></button>
                            <button type="button" class="btn btn-primary" v-if="saving"><?= __("Saving...") ?></button>


            </fieldset>
        </form>


                    </div>
                    </div>




                @include('partials/modal_preferences')
                @include('partials/modal_settings')



                
                </template>