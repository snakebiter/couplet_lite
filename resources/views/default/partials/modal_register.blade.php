<template id="registration-modal">

    <!-- Modal -->
    <div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="modalRegister" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title text-center"><?= __("Found several Matches") ?></h3>
                    <h4 class="modal-title text-center"><?= __("Sign up free to connect with the right one") ?></h4>
                </div>
                <div class="modal-body">
                    <registration-form></registration-form>
                </div>

                <div class="modal-footer" style="text-align: center">
                    <div class="error-message"><p style="color: #000; font-weight: normal;"><a href="" data-toggle="modal" data-target="#modalLogin"><?= __('Already a member? sign-in') ?></a></p></div>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</template>
        
<registration-modal></registration-modal>
