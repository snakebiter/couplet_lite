
<!-- Modal -->
<div class="modal" id="mySearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?= __("Your preferences") ?></h4>
      </div>
      <div class="modal-body">
                            <form role="form" v-on:submit.prevent="updateForm">

                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?= __("I'm interested in:") ?></label>
                                            <select id="start_element" name="amount" class="form-control"  v-model="user.pref_gender">
                                                <option value="women"><?= __("Women") ?></option>
                                                <option value="men"><?= __("Men") ?></option>
                                            </select>
                                        </div>
                                    </div>                        

                                    <div class="col-sm-6">
                                            <label for="exampleInputEmail1"><?= __("Age range") ?></label>

                                        <div class="row">

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    
                                                    <input type="text" class="form-control" v-model="user.pref_min_age">

                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" v-model="user.pref_max_age">
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?= __("Martial status:") ?></label>
                                            <select name="amount" class="form-control" v-model="user.pref_marital_status">
                                                <option value="single"><?= __("Must be single") ?></option>
                                                <option value="anything"><?= __("Doesn't matter") ?></option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?= __(">Location:") ?></label>
                                            <select name="amount" class="form-control" v-model="user.pref_location">
                                                <option value="nearby"><?= __("Near me") ?></option>
                                                <option value="anywhere"><?= __("Anywhere") ?></option>
                                            </select>
                                        </div>
                                    </div>

                                </div>


                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="exampleInputEmail1"><?= __("I’m here for:") ?></label>
                                    </div>
                                    <div class="col-sm-6">
                                         <div class="checkbox">
                                            <label>
                                                <input type="checkbox" v-model="user.pref_new_friends"> <?= __("New friends") ?>
                                            </label>
                                        </div>
                                    </div>                        

                                    <div class="col-sm-6">
                                         <div class="checkbox">
                                            <label>
                                                <input type="checkbox" v-model="user.pref_long_term"> Long-term dating
                                            </label>
                                        </div>
                                    </div> 

                                    <div class="col-sm-6">
                                         <div class="checkbox">
                                            <label>
                                                <input type="checkbox" v-model="user.pref_short_term"> Short-term dating
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                         <div class="checkbox">
                                            <label>
                                                <input type="checkbox" v-model="user.pref_casual_sex"> Casual sex
                                            </label>
                                        </div>
                                    </div> 

                                </div>



                            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" v-on:click="updateForm" v-if="!saving">Save changes</button>
        <button type="button" class="btn btn-primary" v-on:click="updateForm" v-if="saving">Save changes</button>
      </div>
    </div>
  </div>
</div>