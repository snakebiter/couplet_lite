<template id="account-inbox">

    <div class="panel panel-default">
        <div class="panel-heading"><?= __('Inbox') ?></div>
        <div class="panel-body">




            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-success" role="alert" v-if="threads && threads.length == 0"><?= __('No messages') ?></div>

                    <div class="mail-option">
                        <!--<div class="chk-all">
                                 <input type="checkbox" class="mail-checkbox mail-group-checkbox">
                                 <div class="btn-group">
                                     <a data-toggle="dropdown" href="#" class="btn mini all" aria-expanded="false">
                                         All
                                         <i class="fa fa-angle-down "></i>
                                     </a>
                                     <ul class="dropdown-menu">
                                         <li><a href="#"> <?= __('None') ?></a></li>
                                         <li><a href="#"> <?= __('Read') ?></a></li>
                                         <li><a href="#"> <?= __('Unread') ?></a></li>
                                     </ul>
                                 </div>
                             </div>-->

                        <div class="btn-group">
                            <a href="#" v-on:click.prevent="refresh()" class="btn mini tooltips" v-if="!refreshing">
                                <i class=" fa fa-refresh "></i>
                            </a>
                            <a href="#" class="btn mini tooltips" v-if="refreshing">
                                <i class=" fa fa-refresh  fa-spin"></i>
                            </a>
                        </div>
                        <!--
                             <div class="btn-group hidden-phone">
                                 <a data-toggle="dropdown" href="#" class="btn mini blue" aria-expanded="false">
                                     More
                                     <i class="fa fa-angle-down "></i>
                                 </a>
                                 <ul class="dropdown-menu">
                                     <li><a href="#"><i class="fa fa-pencil"></i> <?= __('Mark as Read') ?></a></li>
                                     <li><a href="#"><i class="fa fa-ban"></i> <?= __('Spam') ?></a></li>
                                     <li class="divider"></li>
                                     <li><a href="#"><i class="fa fa-trash-o"></i> <?= __('Delete') ?></a></li>
                                 </ul>
                             </div>
                             <div class="btn-group">
                                 <a data-toggle="dropdown" href="#" class="btn mini blue">
                                     Move to
                                     <i class="fa fa-angle-down "></i>
                                 </a>
                                 <ul class="dropdown-menu">
                                     <li><a href="#"><i class="fa fa-pencil"></i> <?= __('Mark as Read<') ?>/a></li>
                                     <li><a href="#"><i class="fa fa-ban"></i> <?= __('Spam') ?></a></li>
                                     <li class="divider"></li>
                                     <li><a href="#"><i class="fa fa-trash-o"></i> <?= __('Delete') ?></a></li>
                                 </ul>
                             </div>
                            -->
                        <ul class="unstyled inbox-pagination" v-if="threads  && threads.length > 0">
                            <li><span>1-@{{threads.length}} of @{{threads.length}}</span></li>
                            <li>
                                <a class="np-btn" href="#"><i class="fa fa-angle-left  pagination-left"></i></a>
                            </li>
                            <li>
                                <a class="np-btn" href="#"><i class="fa fa-angle-right pagination-right"></i></a>
                            </li>
                        </ul>
                        <br />
                        <br />
						<ul class="nav nav-tabs" id="myTabs" role="tablist">
							<li role="presentation" class="@{{mode == 'all'?'active':''}}"><a href="#home" v-on:click.prevent="changeMode('all')"><?= __('Conversations') ?></a></li>
							<li role="presentation" class="@{{mode == 'inbox'?'active':''}}"><a href="#home" v-on:click.prevent="changeMode('inbox')"><?= __('Inbox') ?></a></li>
							<li role="presentation" class="@{{mode == 'sent'?'active':''}}"><a href="#home" v-on:click.prevent="changeMode('sent')"><?= __('Sent') ?></a></li>
						</ul>

                        <table class="table  table-bordered table-striped" style="border-top: 0;" v-if="mode == 'all'">

                            <tbody>
                                <thead>
                                    <tr>
                                        <th><?= __('Sender') ?></th>
                                        <th><?= __('Message') ?></th>
                                        <th><?= __('Date') ?></th>
                                    </tr>
                                </thead>
                                <tr v-for="thread in history">

                                    <td v-if="thread.sender.id == currentUser.id">
                                        
                                        <a href="/inbox/@{{thread.sender.id}}" v-link="{ path: '/account/inbox/' + thread.receiver.id }"><strong>@{{thread.sender.display_name}}</strong></a>
                                    </td>
                                    <td v-else>
                                        <a href="/inbox/@{{thread.receiver.id}}" v-link="{ path: '/account/inbox/' + thread.sender.id }"><strong>@{{thread.sender.display_name}}</strong></a>
                                    </td>
                                    <td>
                                        @{{thread.message}} 
                                    </td>
                                    <td>@{{ thread.updated_at | moment "from" "now" }}</td>
                                </tr>
                                <tr v-for="n in 10">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>




                            </tbody>
                        </table>                        
						
                        <table class="table  table-bordered table-striped" style="border-top: 0;" v-if="mode == 'inbox'">

                            <tbody>
                                <thead>
                                    <tr>
                                        <th><?= __('Read') ?></th>
                                        <th><?= __('Sender') ?></th>
                                        <th><?= __('Message') ?></th>
                                        <th><?= __('Date') ?></th>
                                    </tr>
                                </thead>
                                <tr v-for="thread in threads">

                                    <td class="inbox-small-cells text-center">
                                        <i class="fa fa-envelope text-primary" title="read" v-if="!thread.is_read"></i>
                                        <i class="fa fa-envelope-o text-muted " title="unread" style="" v-if="thread.is_read"></i>
                                    </td>
                                    <td>
                                        <a href="/inbox/@{{thread.sender.id}}" v-link="{ path: '/account/inbox/' + thread.sender.id }"><strong>@{{thread.sender.display_name}}</strong></a>
                                    </td>
                                    <td>
                                        @{{thread.message}} 
                                    </td>
                                    <td>@{{ thread.updated_at | moment "from" "now" }}</td>
                                </tr>
                                <tr v-for="n in 10">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>




                            </tbody>
                        </table>                        
						<table class="table  table-bordered table-striped" style="border-top: 0;" v-if="mode == 'sent'">

                            <tbody>
                                <thead>
                                    <tr>
                                        <th><?= __('Read') ?></th>
                                        <th><?= __('Recipient') ?></th>
                                        <th><?= __('Message') ?></th>
                                        <th><?= __('Date') ?></th>
                                    </tr>
                                </thead>
                                <tr v-for="thread in sent_threads" v-if="thread.receiver">

                                    <td class="inbox-small-cells text-center">
                                        <i class="fa fa-envelope text-primary" v-if="!thread.is_read"></i>
                                        <i class="fa fa-envelope-o text-muted " style="" v-if="thread.is_read"></i>
                                    </td>
                                    <td>
                                        <a href="/inbox/@{{thread.receiver.id}}" v-link="{ path: '/account/inbox/' + thread.receiver.id }"><strong>@{{thread.receiver.display_name}}</strong></a>
                                    </td>
                                    <td>
                                        @{{thread.message}}
                                    </td>
                                    <td>@{{ thread.updated_at | moment "from" "now" }} -</td>
                                </tr>
                                <tr v-for="n in 10">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>




                </div>
                </div>
                </div>
                </div>
</template>