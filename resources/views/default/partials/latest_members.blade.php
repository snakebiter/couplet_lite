<template id="latest-members">
 
   
                        <div class="col-sm-12">
                            <div class="imgholder home-img" v-for="member in members">
                                <div class="outer1 circle"></div>
                                <div class="outer2 circle"></div>
                                <figure class="text-center">
                                    <img v-bind:src="avatar_image(member)" />
                                    <figcaption class="caption text-center">@{{member.display_name}} - @{{member.age}}</figcaption>
                                </figure>
                            </div>

                            

                        </div>        
</template>        

