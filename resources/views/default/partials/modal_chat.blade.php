<template id="chat-box">
    <a href="" v-on:click.prevent="showChatBox()" v-if="!show_box && currentUser" class="chat-box-icon @{{(chat_count > 0)?'blink':''}} ">
        <i class="fa fa-comments-o"></i>
    </a>
    <div class="row chat-window col-xs-4" id="chat_window_1" style="margin-left:10px;" v-if="show_box && currentUser">
        <div class="col-xs-12 col-md-12">
        	<div class="panel panel-default">
                <div class="panel-heading top-bar">
                    <div class="row">
                    <div class="col-md-8 col-xs-9">
                        <h3 class="panel-title"><i class="fa fa-weixin new-messages @{{(blink)?'active':''}} " aria-hidden="true" v-on:click="openList"></i> @{{(currentView == 'list')?'<?= __('Chat') ?>':'<?= __('Chat') ?> - ' + member.display_name}}</h3>
                    </div>
                    <div class="col-md-4 col-xs-3" style="text-align: right;">
                        <!--<a href="#"><span id="minim_chat_window" class="glyphicon glyphicon-minus icon_minim"></span></a>-->
                        <a href="#" v-on:click.prevent="refreshChat()" v-if="currentView == 'chat'" style="margin-right: 10px;"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                        <a href="#" v-on:click.prevent="hideChatBox()"  class="icon_close"><i class="fa fa-times" aria-hidden="true"></i></a>
                    </div>
                    </div>
                </div>
                
                    <ul class="list-group"  v-if="currentView == 'list'">
                        <li class="list-group-item" v-for="contact in contactList">
                            <a href="" v-on:click.prevent="openChat(contact.sender)">
                                <i class="fa fa-envelope text-primary" v-if="!contact.is_read"></i>
                                <i class="fa fa-envelope-o text-muted " style="" v-if="contact.is_read"></i>
                                @{{contact.sender.display_name}}</a> <span class="text-muted">@{{ contact.updated_at | moment "from" "now" }}</span></li>
                        <li class="list-group-item"><span class="text-muted"><i>&mdash;</i></span></li>
                        <li class="list-group-item"><span class="text-muted"><i>&mdash;</i></span></li>
                        <li class="list-group-item"><span class="text-muted"><i>&mdash;</i></span></li>
                        <li class="list-group-item"><span class="text-muted"><i>&mdash;</i></span></li>
                    </ul>

                <div class="panel-body msg_container_base" v-if="currentView == 'chat'">

                    <div class="row msg_container base_receive" v-if="messages.length == 0">

                       <div class="col-md-2 col-xs-2 avatar">
                            <img src="/images/chat_male.png" class=" img-responsive ">
                        </div>

                        <div class="col-md-10 col-xs-10">
                            <div class="messages msg_receive">
                                <p><?= __('Hi, type your message below to start off a conversation with') ?> @{{member.display_name}}</p>
                                <time datetime="2009-11-13T20:00"><?= __('Match Team') ?> • @{{ new Date() | moment "from" "now" }}</time>
                            </div>
                        </div>

                    </div>

                    <div class="row msg_container @{{(message.sender.id == currentUser.id)?'base_sent':'base_receive'}}" v-for="message in messages">

                       <div class="col-md-2 col-xs-2 avatar" v-if="(message.sender.id != currentUser.id)">
                            <img :src="message.sender.avatar" class=" img-responsive ">
                        </div>

                        <div class="col-md-10 col-xs-10">
                            <div class="messages @{{(message.sender.id == currentUser.id)?'msg_sent':'msg_receive'}}">
                                <p>@{{message.message}}</p>
                                <time datetime="2009-11-13T20:00" v-if="(message.id == 0)">@{{message.sender.display_name}} • sending...</time>
                                <time datetime="2009-11-13T20:00" v-else>@{{message.sender.display_name}} • @{{ message.created_at | moment "from" "now" }}</time>
                            </div>
                        </div>
                       <div class="col-md-2 col-xs-2 avatar" v-if="(message.sender.id == currentUser.id)">
                            <img :src="message.sender.avatar" class=" img-responsive ">
                        </div>

                    </div>

                   
                </div>
                <div class="panel-footer">

                    <form method="POST" action="" accept-charset="UTF-8" id="user-login-form" class="form ajax"  v-on:submit.prevent="submit(message)"  v-if="currentView == 'chat'" autocomplete="off">
                        
                        <div class="input-group">
                            <input id="btn-input" type="text" autocomplete="off" v-model="message" class="form-control input-sm chat_input" required placeholder="<?= __('Write your message here...') ?>" />
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary btn-sm" id="btn-chat" v-if="!saving"><?= __('Send') ?></button>
                                <button type="button" class="btn btn-primary btn-sm" id="btn-chat" v-if="saving"><img src="{{asset('themes/default/css/images/ellipsis.gif')}}" /></button>
                            </span>
                        </div>

                    </form>


                </div>
    		</div>
        </div>
    </div>

</template>

<chat-box :current-user="currentUser" v-ref:chat></chat-box>
