                        <template id="account-friends">

                                <div class="" v-if="!isPremium() && !settings.can_see_liked">

  <br />  
                        <br />  
<div class="row">
                        
                        

                        <div class="col-sm-12 text-center" style="height: 110px">
                            <h3 class="text-center"><?= __('Oops!') ?></h3><br />
                            <a v-link="'/account/dashboard'"><?= __('Upgrade to a premium account') ?><br /><?= __('to view who liked you') ?></a>
                        </div>


                </div>
                </div>
                <div  v-else>
                <div class="panel panel-default">
                    <div class="panel-heading"><?= __('Your matches') ?></div>
                    <div class="panel-body">
  <br />  
                        <br />  
<div class="row">
                        
                        

                        <div class="col-sm-12 text-center" style="height: 50px"  v-if="memberMatches && memberMatches.length == 0">
                            <?= __('No members') ?>
                        </div>

                        <div class="col-sm-4 col-md-3 col-xs-6" style="height: 210px"  v-if="memberLiked" v-for="memberLiked in memberMatches">
                            <a href="<?= url('/profile') ?>/@{{memberLiked.member.id}}">
                            <div class="imgholder plain">
                                <div class="outer1 circle"></div>
                                <div class="outer2 circle"></div>
                                <figure>
                                     <img v-bind:src="avatar_image(memberLiked.member)" />
                                    <figcaption class="caption">@{{memberLiked.member.display_name}}</figcaption>
                                </figure>
                            </div>
                            </a>
                        </div>


                                                  
                    </div>
                    </div>

                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><?= __("People you've liked") ?></div>
                    <div class="panel-body">
                        <br />  
                        <br />  

<div class="row">
                        
                        <div class="col-sm-12 text-center" style="height: 50px"  v-if="membersYouLiked && membersYouLiked.length == 0">
                            <?= __('No members') ?>
                        </div>

                        <div class="col-sm-4 col-md-3 col-xs-6" style="height: 210px"  v-if="memberLiked" v-for="memberLiked in membersYouLiked">
<a href="<?= url('/profile') ?>/@{{memberLiked.member.id}}">
                            <div class="imgholder plain">
                                <div class="outer1 circle"></div>
                                <div class="outer2 circle"></div>
                                <figure>
                                     <img v-bind:src="avatar_image(memberLiked.member)" />
                                    <figcaption class="caption">@{{memberLiked.member.display_name}}</figcaption>
                                </figure>
                            </div>

                             </a>
                        </div>


                                                  
                    </div>
                    </div>

                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading"><?= __("People who've liked you") ?></div>
                    <div class="panel-body">
  <br />  
                        <br />  
<div class="row">
                        
                        <div class="col-sm-12 text-center" style="height: 50px"  v-if="membersWhoLikedYou && membersWhoLikedYou.length == 0">
                            <?= __('No members') ?>
                        </div>

                        <div class="col-sm-4 col-md-3 col-xs-6" style="height: 210px"  v-if="memberLiked" v-for="memberLiked in membersWhoLikedYou">
<a href="<?= url('/profile') ?>/@{{memberLiked.user.id}}">
                            <div class="imgholder plain">
                                <div class="outer1 circle"></div>
                                <div class="outer2 circle"></div>
                                <figure>
                                     <img v-bind:src="avatar_image(memberLiked.user)" />
                                    <figcaption class="caption">@{{memberLiked.user.display_name}}</figcaption>
                                </figure>
                            </div>
                            </a>
                        </div>


                                                  
                    </div>
                    </div>

                </div>
                </div>
				
                <div class="panel panel-default" v-if="isPremium()">
                    <div class="panel-heading"><?= __('Interactions') ?></div>
                    <div class="panel-body">
  <br />  
                        <br />  
<div class="row">
                        
                        <div class="col-sm-12 text-center" style="height: 50px"  v-if="membersWhoInteractedWithYou && membersWhoInteractedWithYou.length == 0">
                            <?= __('No interactions yet') ?>
                        </div>

                        <div class="col-sm-4 col-md-3 col-xs-6" style="height: 210px"  v-if="memberLiked" v-for="memberLiked in membersWhoInteractedWithYou">
                        
							<a href="/profile/@{{memberLiked.member.id}}">
                            <div class="imgholder plain">
                                <div class="outer1 circle"></div>
                                <div class="outer2 circle"></div>
                                <figure>
                                     <img v-bind:src="avatar_image(memberLiked.member)" />
                                    <figcaption class="caption">@{{memberLiked.member.display_name}}</figcaption>
                                </figure>
                            </div>
                            </a>
                        </div>


                                                  
                    </div>
                    </div>

                </div>



                        </template>