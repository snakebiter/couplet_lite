<div class="panel-body">
	<fieldset>				

		<div class="row filter-row">
			<div class="col-sm-12">

				<select name="gender" class="form-control" v-model="params.gender" style="width: 100%">
					<option value="" selected>{{__('Any')}}</option>
					<option value="M">{{__('Man')}}</option>
					<option value="F">{{__('Woman')}}</option>
				</select>

			</div>
		</div>

		</fieldset>

</div>

