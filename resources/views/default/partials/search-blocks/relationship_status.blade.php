                    <template id="search-gender">

<div class="panel-heading">@{{property.title}}</div>

<div class="panel-body">
	<fieldset>				

		<div class="row filter-row">
			<div class="col-sm-12">

                                            <select id="start_element" name="amount" class="form-control" v-model="user.gender">
                                                <option value="M">{{__('Man')}}</option>
                                                <option value="F">{{__('Woman')}}</option>
                                            </select>

			</div>
		</div>

		</fieldset>

</div>
</template>

