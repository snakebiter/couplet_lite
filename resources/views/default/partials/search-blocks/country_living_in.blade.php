

<div class="panel-body">
	<fieldset>				

		<div class="row filter-row">
			<div class="col-sm-12">

                                    <select name="amount" class="form-control" v-model="params.country_living_in" style="width: 100%">
                                        <option value="" :value="null">-- {{__('Any')}} --</option>
                                        <option v-for="option in country_codes" v-bind:value="option['alpha-2']">
                                            @{{ option.name }}
                                        </option>
                                    </select>

			</div>
		</div>

		</fieldset>

</div>

