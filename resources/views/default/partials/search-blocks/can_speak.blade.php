<div class="panel-body">
	<fieldset>				

		<div class="row filter-row">
			<div class="col-sm-12">

                            <v-select multiple label="English" :value.sync="params.languages" :options="language_codes"></v-select>

			</div>
		</div>

		</fieldset>

</div>

