                    <template id="account-sidebar">

            <div class="sidebar-account">		

						<div class="row" style="margin-top: 0px">
                                                                    <div class="col-sm-12">
											<img v-bind:src="avatar_image(currentUser)" style="cursor: pointer; min-height: 200px;" onclick="$('#myInput').click();" width="100%"/>

                                             <br />

                                            <span class="btn btn-default btn-xs btn-file">
                                                {{ __('Change avatar') }} <input type="file" id="myInput" v-on:change="onFileChange">
                                            </span>


                                    </div>
                                    </div>
<br />
	<div class="row ">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">{{ __('My account') }}</div>
				<div class="panel-body panel-sidebar">
					<ul class="nav">
						<li >
							<a href="<?= url('account/dashboard') ?>" v-link="{ path: '/account/dashboard', activeClass: 'active' }">{{ __('Dashboard') }}</a>
						</li>	
						                        <li>
							<a href="<?= url('account/settings') ?>" v-link="{ path: '/account/settings', activeClass: 'active' }">{{ __('My account') }}</a>
						</li>					
						<li>	
							<a class="" href="<?= url('account/profile') ?>" v-link="{ path: '/account/profile', activeClass: 'active' }">{{ __('My profile') }}</a>
						</li>
						<li>	
							<a class="" href="<?= url('account/gallery') ?>" v-link="{ path: '/account/gallery', activeClass: 'active' }">{{ __('Photo gallery') }}</a>
						</li>

						<li>
							<a class="" href="<?= url('inbox') ?>" v-link="{ path: '/account/inbox', activeClass: 'active' }">{{ __('Inbox') }}</a>
						</li>
						<li>
							<a class="" href="<?= url('members') ?>" >{{ __('Browse') }}</a>
						</li>
						<li>
							<a class="" href="<?= url('account/friends') ?>" v-link="{ path: '/account/friends', activeClass: 'active' }">{{ __('Liked Members') }} <span class="badge badge-info" v-if="settings.can_see_liked == 'Yes'"><?= __('premium only') ?></span></a>
						</li>
					</ul>

				</div>
			</div>
		</div>
	</div>

	<div class="row hidden-xs">
<div class="col-lg-12">
		<div class="well">
			<div class="row ">

			<div class="col-lg-12">
<h4 style="margin-top: 0"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> {{ __('Tip') }}</h4>
			<p>{{ __('Make sure you fill out your profile to gain more visibility') }}</p>

			</div>
			</div>

		</div>
		</div>
		</div>

</div>        
</template>        

