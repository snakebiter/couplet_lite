<template id="inbox-thread">


  <div class="panel panel-default">
    <div class="panel-heading">
      <?= __('Message Center') ?>
    </div>
    <div class="panel-body">


      <div class="row">
        <div class="col-sm-12">
          <a v-link="{ path: '/account/inbox/' }"><i class="fa fa-chevron-left" aria-hidden="true"></i> <?= __('Back to inbox') ?></a><br
          /><br />
          <div class="alert alert-danger" role="alert" v-if="messages && messages.length == 0"><?= __('No messages') ?></div>

          <div id="microposts" class="feed" v-if="messages && messages.length  > 0">
            <div class="micropost" v-for="message in messages">
              <div class="content row">
                <div class="avatar-content col-md-1">
                  <a href="/profile/@{{message.sender.id}}"><img v-bind:src="message.sender.avatar" alt="@{{message.sender.avatar}}"></a>
                </div>
                <div class="post-content  col-md-8">
                  <span class="name"><a href="/profile/@{{message.sender.id}}">@{{message.sender.display_name}}</a></span>
                  <span class="username">@{{message.sender.first_name}} @{{message.sender.last_name)}}</span>
                  <div class="post">@{{message.message}}</div>
                  <div class="post">
                    <br />
                    <span class="text-muted" style="font-style: italic;"><?= __('Message sent on:') ?>  @{{ message.created_at |  moment "dddd, MMMM Do YYYY, h:mm:ss a" }}</span>
                  </div>
                </div>
                <div class="right-content  col-md-2 text-right">
                  <span>@{{ message.created_at | moment "from" "now" }}</span>
                </div>
              </div>
              <div class="row">
                <div class="col-md-10 col-md-offset-1">
                  <br />
                  <br />
                  <br />
                </div>
              </div>
            </div>


          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="d">
    <div class="well">
      <div id="new-micropost" class="panel-body">
        <form method="POST" action="" accept-charset="UTF-8" id="user-login-form" class="form ajax" v-on:submit.prevent="submit(message)">
          <div class="form-group">
            <textarea cols="10" class="form-control" placeholder="Compose..." v-model="message" rows="4" style="overflow: hidden; word-wrap: break-word; height: 156px;"></textarea>
          </div>
          <button type="submit" class="btn btn-primary pull-right" v-if="!saving" :disabled="message.length == 0"><?= __('Send message') ?></button>
          <button type="button" class="btn btn-primary pull-right" v-if="saving"><?= __('Sending...') ?></button>
        </form>
      </div>
    </div>
  </div>



</template>