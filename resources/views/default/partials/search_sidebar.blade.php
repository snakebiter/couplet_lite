<div class="sidebar ">		
                    <form class="form-inline mini" style="margin-bottom: 0px;" v-on:submit.prevent="search">

    <div class="row ">


        <div class="col-sm-11">

<div class="panel-group" id="accordion">

<? foreach($search_properties as $i => $search_property): ?>

    <div class="panel panel-default"  <? if($search_property['field'] == 'can_speak') :  ?>style="overflow: visible;" <? endif; ?>>
      <div class="panel-heading">
          <?= $search_property['title'] ?>
          <a class="accordion-toggle pull-right" data-toggle="collapse" href="#c<?= $search_property['field'] ?>">
            <i class="fa fa-<?= ($i < 3)?'minus':'plus' ?>" aria-hidden="true"></i>
          </a>
      </div>
      <div id="c<?= $search_property['field'] ?>" class="panel-collapse collapse <?= ($i < 3)?'in':'' ?>">
                    <? if($search_property['template']) {  ?>
                      @include('partials/search-blocks/' . $search_property['field'])
                    <? } else { ?>
                
                    <div class="panel-body">

                        <fieldset>				

                            <div class="row filter-row">
                                <div class="col-sm-12">
                                        
                                    <input type="checkbox" id="<?= $search_property['field'] ?>_any" value="-1" v-model="any['<?= $search_property['field'] ?>']" v-on:change="clearParams('<?= $search_property['field'] ?>')">
                                    <label for="<?= $search_property['field'] ?>_any"><?= __("Any/Doesn't matter") ?></label><br />

                                    <? if($profile_options[$search_property['field']]) { ?>
                                        <? foreach($profile_options[$search_property['field']] as $profile_option) { ?>
                                            <input type="checkbox" id="<?= $search_property['field'] ?>_<?= $profile_option['field'] ?>" value="<?= $profile_option['field'] ?>" v-model="params['<?= $search_property['field'] ?>']" v-on:change="clearAny('<?= $search_property['field'] ?>')">
                                            <label for="<?= $search_property['field'] ?>_<?= $profile_option['field'] ?>"><?= $profile_option['name'] ?></label><br />
                                        <? } ?>
                                    <? } ?>

                                </div>
                            </div>
                        
                        </fieldset>


                    </div>
                     <? } ?>
      </div>
    </div>
        
<? endforeach; ?>

</div>



            </div>

    </div>


    <div class="row ">
        <div class="col-sm-11">
            <div class="panel panel-default">
                <div class="panel-body" style="text-align: center;">
                    
                    <button v-on:click.prevent="search" class="btn btn-primary" type="submit"><?= __('Update results') ?></button>

                </div>
            </div>
        </div>
    </div>

    </form>
</div>