                        <template id="account-settings">

                <div class="panel panel-default">
                    <div class="panel-heading"><?= __('Dashboard') ?></div>
                    <div class="panel-body">

                            <form class="form-vertical" v-on:submit.prevent="updateForm">
                                <fieldset>

                                    <div class="row">
                                        <div class="col-sm-12 ">

                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?= __('Name') ?></label>
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <select name="amount" class="form-control" v-model="user.gender">
                                                            <option value="M"><?= __('Mr') ?></option>
                                                            <option value="F"><?= __('Mrs') ?></option>
                                                        </select>
                                                        <p class="help-block" style="margin-bottom: 0"><?= __('Mr/Mrs') ?></p>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control " v-model="user.name" placeholder="<?= __('Your full name') ?>">
                                                        <p class="help-block" style="margin-bottom: 0"><?= __('Your full name') ?></p>

                                                    </div>
                                                </div>

                                            </div>
                                            <br />

                                                                                        <div class="form-group">
                                                <label for="exampleInputEmail1"><?= __('Where you live') ?></label>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control " id="my-city" v-model="user.city" placeholder="<?= __('City') ?>">
                                                                                            <p class="help-block" style="margin-bottom: 0"><?= __('City you live in') ?></p>

                                                    </div>
                                                    <div class="col-sm-5">
                                                                                           <select name="amount" class="form-control" v-model="user.country">
                                        <option value="" :value="null">-- <?= __('Country') ?> --</option>
                                        <option v-for="option in country_codes" v-bind:value="option['alpha-2']">
                                            @{{ option.name }}
                                        </option>
                                    </select>
                                                                        <p class="help-block" style="margin-bottom: 0"><?= __('Country you live in') ?></p>

                                                    </div>
                                                </div>

                                            </div>

<br />
                                            <div class="form-grsoup">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label for="exampleInputEmail1"><?= __('I was born on') ?>:</label>
                                                    </div>
                                                    <div class="col-sm-2">

                                                        <div class="form-group">
                                                            <select name="amount" class="form-control" v-model="user.birth_day">
                                                <option value="" :value="null">-- <?= __('Date') ?> --</option>
                                                <option v-for="n in days" v-bind:value="n">@{{ n }}</option>
                                            </select>
                                                                                                    <p class="help-block" style="margin-bottom: 0"><?= __('Date') ?></p>

                                                        </div>
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                            <select name="amount" class="form-control" v-model="user.birth_month">
                                                <option value="" :value="null">-- <?= __('Month') ?> --</option>
                                                        <option value = "1">01</option>
                                                            <option value = "2">02</option>
                                                            <option value = "3">03</option>
                                                            <option value = "4">04</option>
                                                            <option value = "5">05</option>
                                                            <option value = "6">06</option>
                                                            <option value = "7">07</option>
                                                            <option value = "8">08</option>
                                                            <option value = "9">09</option>
                                                            <option value = "10">10</option>
                                                            <option value = "11">11</option>
                                                            <option value = "12">12</option> 
                                                </select>
                                                <p class="help-block" style="margin-bottom: 0"><?= __('Month') ?></p>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                            <select name="amount" class="form-control" v-model="user.birth_year">
                                                <option value="" :value="null">-- <?= __('Year') ?> --</option>
                                                <option v-for="n in years" v-bind:value="n">@{{ n }}</option>
                                            </select>
                                            <p class="help-block" style="margin-bottom: 0"><?= __('Year') ?></p>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>



<br />

<br />
                                            <div class="form-group" >
                                                <label for="exampleInputEmail1"><?= __('Display name') ?></label>
												
                                                <div class="row" v-if="!isPremium() && !settings.can_change_display_name">

                                                    <div class="col-sm-6">
                                                        <input type="email" class="form-control" value="@{{user.display_name}}" disabled placeholder="">
														<div class="checkbox">
															<label>
																<input type="checkbox" v-model="user.protect_avatar" v-if="!isPremium() && settings.can_have_private_gallery">
																<input type="checkbox" v-model="user.protect_avatar" disabled v-else>
																<?= __('Make my pictures private') ?> <span class="text-muted">(<?= __('Only users who you have liked can see your photos') ?>)</span>
															</label>
														</div>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <a v-link="'/account/dashboard'"><?= __('Upgrade to a premium account<br />to change your display name') ?></a>
                                                    </div>

                                                </div>

                                                <div class="row" v-else>

                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" v-model="user.display_name" required>
														<div class="checkbox">
															<label>
																<input type="checkbox" v-model="user.protect_avatar"> <?= __('Make my pictures private') ?> <span class="text-muted">(<?= __('Only users who you have liked can see your photos') ?>)</span>
															</label>
														</div>
                                                    </div>

                                                </div>

                                            </div>
                                            <br />
                                            <br />
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?= __('Email address') ?></label>
                                                <input type="email" class="form-control" value="@{{user.email}}" disabled placeholder="<?= __('Enter email') ?>">
                                            </div>


<br />
<hr />

                                            <div class="form-group" style="margin-bottom: 0">
                                                <div class="row">

                                                    <div class="col-sm-6">

                                                        <div class="form-group">
                                                            <label for="exampleInputPassword1"><?= __('Password') ?>  <a href=""  v-if="!change_password" v-on:click.prevent="toggle_password()" style="font-weight: normal;">(<?= __('change password') ?>)</a></label>
                                                            <p v-if="!change_password" class="form-control-static">******</p>
                                                            <input v-if="change_password" type="password" v-model="password" class="form-control" id="exampleInputPassword1" placeholder="<?= __('Password') ?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="exampleInputPassword1"><?= __('Confirm password') ?></label>
                                                            <p class="form-control-static" v-if="!change_password">******</p>
                                                            <input v-if="change_password" type="password" v-model="password_confirm" class="form-control" id="exampleInputPassword1"
                                                                placeholder="<?= __('Confirm your password') ?>">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
<hr />
                                            <div class="checkbox">
                                                <label>
                                            <input type="checkbox" v-model="user.email_permission"> <?= __('We can contact you with relevant properties, offers and news') ?>
                                        </label>
                                            </div>
                                            <br />
                                            <button type="submit" class="btn btn-primary" v-if="!saving"><?= __('Save details') ?></button>
                                            <button type="button" class="btn btn-primary" v-if="saving"><?= __('Saving...') ?></button>


                                        </div>




                                    </div>



                                </fieldset>
                            </form>
                                                                </div>
                                    </div>

                        </template>
