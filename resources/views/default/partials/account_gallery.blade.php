
                        <template id="account-gallery">
            <div class="panel panel-default">
                <div class="panel-heading"><?= __('Dashboard') ?></div>
                <div class="panel-body">

<div class="row">
    
    <div class="col-md-4 col-lg-3">

        <div class="panel panel-default add-image-panel" style="border: 2px dashed #d2d2d2; height: 243px;">
            <div class="panel-body" style="text-align: center; color: #b2b2b2; font-weight: bold;" >
<br>
<div :style="{display: (!isUploading?'block':'none')}">
                             <!-- only show the menu when ready -->
    <ul v-show="uploadedFiles.length > 0">
      <!-- loop through the completed files -->
      <li v-for="file in uploadedFiles">Name: <em>@{{ file.name }}</em> Size: <em>@{{ file.size | prettyBytes }}</em></li>
    </ul>
    <!-- only show when ready, fileProgress is a percent -->
	
    <div class="progress-bar" style="width: @{{ fileProgress }}%" v-show="fileProgress > 0" ></div>
    <!-- message for all uploads completing -->
    <p v-if="allFilesUploaded"><strong><?= __('All Files Uploaded') ?></strong></p>
    <!-- full usage example -->
    <file-upload class="my-file-uploader" name="myFile" id="myCustomId" action="upload.php" multiple></file-upload>
    
                


            </div>            
<div :style="{display: (isUploading?'block':'none')}">

    <p><strong><?= __('Please wait...') ?></strong></p><br >
<i class="fa fa-circle-o-notch fa-spin fa-picture-o" style="font-size: 90px;" ></i>


            </div>
			            </div>            

        </div>
    </div>
    
    


    <div class="col-md-4 col-lg-3" v-for="galleryImage in galleryImages">

        <div class="panel panel-default image-panel">
            <div class="panel-body">
<a class="close_ico" href="#" v-on:click.prevent="deleteImage(galleryImage, $event)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to remove"></a>
                <img class="thumbnail" :src="galleryImage.thumbnail" style="width: 100%">


            </div>
        </div>



    </div>
    



    </div>
    </div>
    </div>

                        </template>
