<template id="registration-form">

	<form role="form" v-on:submit.prevent="register">

		<div class="row">

			<div class="col-sm-6">
				<div class="form-group">
					<label >{{ __("I'm a") }}:</label>
					<select id="start_element" name="amount" class="form-control" v-model="user.gender">
						<option value="M">{{ __("Man") }}</option>
						<option value="F">{{ __("Woman") }}</option>
					</select>
				</div>
			</div>                        

			<div class="col-sm-6">
				<div class="form-group">
					<label >{{ __("I want a") }}:</label>
							<select id="start_element" name="amount" class="form-control" v-model="user.pref_gender">
						<option value="women">{{ __("Woman") }}</option>
						<option value="men">{{ __("Man") }}</option>
					</select>
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col-sm-12">
				<div class="form-group">
					<label >{{ __("Location") }}:</label>
					<input id="searchTextField" class="form-control" type="text" size="50" placeholder="Enter your location" autocomplete="off" runat="server" />  
	<input type="hidden" id="country" name="country"  v-model="user.country"/>
	<input type="hidden" id="city" name="city"  v-model="user.city"/>
	<input type="hidden" id="cityLat" name="cityLat"  v-model="user.lat"/>
	<input type="hidden" id="cityLng" name="cityLng"  v-model="user.lng"/>  
	<p class="help-block" style="font-size: 10px">@{{user.city}} @{{user.country}}</p>
				</div>
			</div>

		</div>


		<div class="row">
			<div class="col-sm-12">
				<label ><?= __("I was born on:") ?></label>
			</div>
			<div class="col-sm-4">

				<div class="form-group">
									<select name="amount" class="form-control" v-model="user.birth_day">
						<option value="" :value="null">DD</option>
						<option v-for="n in days" v-bind:value="n">@{{ n }}</option>
					</select>
				</div>
			</div>                        

			<div class="col-sm-4">
				<div class="form-group">
									<select name="amount" class="form-control" v-model="user.birth_month">
						<option value="" :value="null">MM</option>
								<option value = "1">01</option>
									<option value = "2">02</option>
									<option value = "3">03</option>
									<option value = "4">04</option>
									<option value = "5">05</option>
									<option value = "6">06</option>
									<option value = "7">07</option>
									<option value = "8">08</option>
									<option value = "9">09</option>
									<option value = "10">10</option>
									<option value = "11">11</option>
									<option value = "12">12</option> 
						</select>
				</div>
			</div>  

			<div class="col-sm-4">
				<div class="form-group">
						<select name="amount" class="form-control" v-model="user.birth_year">
						<option value="" :value="null">YY</option>
						<option v-for="n in years" v-bind:value="n">@{{ n }}</option>
					</select>
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col-sm-12">
				<div class="form-group">
					<label ><?= __("My email address is:") ?></label>
					<input type="email" class="form-control" required  v-model="user.email">
				</div>
			</div>

		</div>

	                              <div class="row">

			<div class="col-sm-12">
				<div class="form-group">
					<label ><?= __("My full name:") ?></label>
					<input type="text" class="form-control"  v-model="user.full_name" required>
				</div>
			</div>

		</div>

		<div class="row">                        
			<div class="col-sm-12">
				<div class="form-group">
					<label ><?= __("My password should be:") ?></label>
					<input type="password" class="form-control" required placeholder="6 or more characters"  v-model="user.password">
				</div>
			</div>                        
		</div>

	<div class="alert alert-danger" role="alert" v-if="error.length > 0">@{{error}}</div>

		<button tyle="submit" class="btn btn-primary btn-block" v-if="!saving"><?= __("Join now") ?></button>
		<button tyle="button" class="btn btn-primary btn-block" v-if="saving"><?= __("Please wait...") ?></button>

		<div class="row">                        
			<div class="col-sm-12">
				<div class="checkbox">
					<label>
						<input type="checkbox" required> <?= __("By signing up, you agree to our") ?> <a href="#"><?= __("Terms") ?></a> <?= __("and") ?> <a href="#"><?= __("Privacy Policy") ?></a> <?= __("and are at least 18 years old.") ?>
					</label>
				</div>
			</div>                        
		</div>

	</form>


</template>