<div class="modal" id="mySettings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-dismiss="modal" data-backdrop="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= __("Edit your details") ?></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" v-on:submit.prevent="updateForm"  style="max-height: 60vh; overflow-x: hidden; overflow-y: scroll;">
                    <input type="hidden" value="straight" v-model="user.orientation" />

                    <div class="row">
                        <div class="col-md-12">


                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?= __("Ethnicity") ?></label>
                        <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-4"  v-for="ethnicity in profile_options.ethnicities">
                                         <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="@{{ethnicity.name}}" value="@{{ethnicity.field}}" v-model="user.ethnicities"> @{{ethnicity.name}}
                                            </label>
                                        </div>
                                    </div>                        


                                </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?= __("Your origin") ?></label>
                        <div class="col-sm-9">
                    <div class="row">
                                                <div class="col-sm-11">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group"  style="margin-bottom: 0">
                                <div class="col-sm-12">
                                    <select name="amount" class="form-control" v-model="user.mother_tongue">
                                        <option value="" :value="null">-- <?= __("Mother Tongue") ?> --</option>
                                        <option v-for="option in language_codes" v-bind:value="option.alpha2">
                                            @{{ option.English }}
                                        </option>
                                    </select>
                                    <p class="help-block" style="margin-bottom: 0"><?= __("Where you live in") ?></p>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group"  style="margin-bottom: 0">
                                <div class="col-sm-12">
                                    <select name="amount" class="form-control" v-model="user.country_grew_up_in">
                                        <option value="" :value="null">-- <?= __("Born in") ?> --</option>
                                        <option v-for="option in country_codes" v-bind:value="option['alpha-2']">
                                            @{{ option.name }}
                                        </option>
                                    </select>
                                    <p class="help-block" style="margin-bottom: 0"><?= __("Where you grew up in") ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                        </div>
                        </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?= __("Other languages") ?></label>
                        <div class="col-sm-7">
                            <v-select multiple label="English" :value.sync="user.languages" :options="language_codes"></v-select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?= __("Relationship Status") ?></label>
                        <div class="col-sm-7">
                            <select name="amount" class="form-control" v-model="user.relationship_status">
                                <option v-for="option in profile_options.relationship_status" v-bind:value="option.field">
                                    @{{ option.name }}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?= __("Height (cm)") ?></label>
                        <div class="col-sm-7">
                            <div class="input-group">
                                <input type="number" class="form-control" v-model="user.height" placeholder="">
                                <div class="input-group-addon"><?= __("cm") ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?= __("Body Type") ?></label>
                        <div class="col-sm-7">
                            <select name="amount" class="form-control" v-model="user.body_type">
                                <option v-for="option in profile_options.body_type" v-bind:value="option.field">
                                    @{{ option.name }}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?= __("Diet") ?></label>
                        <div class="col-sm-7">
                            <select name="amount" class="form-control" v-model="user.diet">
                                <option v-for="option in profile_options.diet" v-bind:value="option.field">
                                    @{{ option.name }}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?= __("Smoker") ?></label>
                        <div class="col-sm-7">
                            <select name="amount" class="form-control" v-model="user.smoker">
                                <option v-for="option in profile_options.smoker" v-bind:value="option.field">
                                    @{{ option.name }}
                                </option>
                            </select>
                            </div>
                    </div>
                    </div>
                    <div class="col-md-12">

                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?= __("Drinking") ?></label>
                        <div class="col-sm-7">
                            <select name="amount" class="form-control" v-model="user.drinking">
                                <option value="">-</option>
                                <option v-for="option in profile_options.drinking" v-bind:value="option.field">
                                    @{{ option.name }}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?= __("Drugs") ?></label>
                        <div class="col-sm-7">
                            <select name="amount" class="form-control" v-model="user.drugs">
                                <option value="">-</option>
                                <option v-for="option in profile_options.drugs" v-bind:value="option.field">
                                    @{{ option.name }}
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?= __("Religion") ?></label>
                        <div class="col-sm-7">
                            <select name="amount" class="form-control" v-model="user.religion">
                                <option value="">-</option>
                                <option v-for="option in profile_options.religion" v-bind:value="option.field">
                                    @{{ option.name }}
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?= __("Education") ?></label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" v-model="user.education">
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?= __("Profession") ?></label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" v-model="user.profession">
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?= __("Offspring") ?></label>
                        <div class="col-sm-7">
                            <select name="amount" class="form-control" v-model="user.offspring">
                                <option value="">-</option>
                                <option v-for="option in profile_options.offspring" v-bind:value="option.field">
                                    @{{ option.name }}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?= __("Pets") ?></label>
                        <div class="col-sm-7">
                                <div class="row">
                                    <div class="col-sm-4" >
                                         <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="1" v-model="user.has_dog"> <?= __("Has dog") ?>
                                            </label>
                                        </div>
                                    </div>                        
                                    <div class="col-sm-4" >
                                         <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="1" v-model="user.has_cat"> <?= __("Has cat") ?>
                                            </label>
                                        </div>
                                    </div>                        


                                </div>
                        </div>
                    </div>



                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __("Close") ?></button>
                <button type="button" class="btn btn-primary" v-on:click="updateForm"><?= __("Save changes") ?></button>
            </div>
        </div>
    </div>
</div>
</div>