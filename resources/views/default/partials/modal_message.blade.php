                    <template id="message-modal">

<!-- Modal -->
<div class="modal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-labelledby="myModalMessage" aria-hidden="true" v-if="profileUser">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __("Send a private message") ?></h4>
            </div>
            <div class="modal-body">
                <p><?= __("Type your message into the box below to get in touch with") ?> <strong>@{{profileUser.display_name}}</strong>.</p>

<div class="alert alert-danger" role="alert" v-if="error.length > 0">@{{error}}</div>


                <form method="POST" action="" accept-charset="UTF-8" id="user-login-form" class="form ajax"  v-on:submit.prevent="submit(message)">

                    <div class="form-group">
                        <textarea class="form-control" name="message" type="text" v-model="message" rows="4">                
                        </textarea>
                        </div>
                    <div class="row">
                        <div class="col-md-6">

                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary pull-right" v-if="!saving"><?= __("Send messsage") ?></button>
                            <button type="button" class="btn btn-primary pull-right" v-if="saving"><?= __("Sending...") ?></button>
                        </div>
                    </div>

                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
                </template>
                <message-modal :current-user="currentUser"></message-modal>
