                    <template id="login-modal">

<!-- Modal -->
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="modalLogin" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Sign in to your account') ?></h4>
            </div>
            <div class="modal-body">
                <p><?= __('If you have an account with us, please enter your details below.') ?></p>

<div class="alert alert-danger" role="alert" v-if="error.length > 0">@{{error}}</div>


                <form method="POST" action="" accept-charset="UTF-8" id="user-login-form" class="form ajax"  v-on:submit.prevent="login(email, password)">

                    <div class="form-group">
                        <input placeholder="Your email address" class="form-control" name="email" type="text" v-model="email">                </div>

                    <div class="form-group">
                        <input placeholder="Your password" class="form-control" name="password" type="password" value="" v-model="password">                </div>

                    <div class="row">
                        <div class="col-md-6">

                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary pull-right" v-if="!saving"><?= __('Login') ?></button>
                            <button type="button" class="btn btn-primary pull-right" v-if="saving"><?= __('Please wait...') ?></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <a data-toggle="modal" href="<?= url('/password/reset') ?>"><?= __('Forgot your password?') ?></a>
                        </div>
                    </div>

                </form>
            </div>

            <div class="modal-footer" style="text-align: center">
                <div class="error-message"><p style="color: #000; font-weight: normal;"><?= __("Don't have an account?") ?> <a class="link-info" href="<?= url('/') ?>"><?= __("Register now") ?></a></p></div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
                </template>
                <login-modal :current-user="currentUser"></login-modal>
