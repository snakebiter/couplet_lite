<template id="forgot-modal">

    <!-- Modal -->
    <div class="modal fade" id="modalForgot" tabindex="-1" role="dialog" aria-labelledby="modalForgot" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><?= __('Forgot your password?') ?></h4>
                </div>
                <div class="modal-body">
                    <p><?= __('Enter your email to continue') ?></p>
                    
                    <div class="alert alert-danger" role="alert" v-if="error.length > 0">@{{error}}</div>
                    <div class="alert alert-success" role="alert" v-if="success.length > 0">@{{success}}</div>

                    <form role="form" v-on:submit.prevent="submit(email)">
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Your email address"  v-model="email">
                        </div>

                        <div class="row">
                            <div class="col-md-6">

                            </div>
                            <div class="col-md-6">
                            <button type="submit" class="btn btn-primary pull-right" v-if="!saving"><?= __('Continue') ?></button>
                            <button type="button" class="btn btn-primary pull-right" v-if="saving"><?= __('Please wait...') ?></button>
                            </div>
                        </div>
                    </form>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</template>

<forgot-modal :current-user="currentUser"></forgot-modal>
