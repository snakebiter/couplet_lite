<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', $title)</title>
	<meta name="generator" content="Couplet - getcouplet.com. All rights reserved." />

    <!-- Styles -->

    <link href="//netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyBkQZCogLNnc1U0jaPUaOt34AsQ9VUW7Ms"></script>
    <script src="https://cdn.jsdelivr.net/places.js/1/places.min.js"></script>
    <link href="{{asset('themes/default/css/app.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="../../assets/js/html5shiv.js"></script>
        <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            'userToken' => $token,
        ]); ?>
    </script>
    <script src="<?= asset('/js/app.js') ?>"></script>

</head>
<body data-time="<?= time() ?>">

<div class="page-transition" style="display: none;"></div>

        <!-- Wrap all page content here -->
        <div id="app">

        <div id="wrap">


            <nav class="navbar navbar-default" role="navigation">
                <div class="container">

                    <div class="navbar-header" style="position: relative;">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a href="{{ url('/') }}" class="navbar-brand ">
							<img src="<?= asset('/images/logo.png') ?>" style="max-height: 30px;margin-top: -24px;margin-left: 0px;"/>
                        </a>				

                    </div>


                    <template id="navbar-template">

                        <div class="collapse navbar-collapse">

                            <ul class="nav navbar-nav navbar-right" v-if="!currentUser">
                                <li><a href="" data-toggle="modal" data-target="#modalLogin"><?= __('Already a member? sign-in') ?></a></li>
                            </ul>

                            <ul class="nav navbar-nav navbar-right" v-if="currentUser">
                                <? $route = (isset($account))?'v-link':'link'; ?>

                                <li><a href="<?= url('account/settings') ?>" <?= $route ?>="{ path: '/account/settings' }"><?= __('Welcome') ?> @{{currentUser.display_name}} <span class="badge badge-primary" style="position: absolute;right: -23px; top: 28px;" v-if="isPremium(currentUser)"><?= __('premium') ?></span></a></li>
                                <li><a href="<?= url('members') ?>"><?= __('Members') ?></a></li>
                                <li  class="acdive"><a href="<?= url('account/profile') ?>" <?= $route ?>="{ path: '/account/profile' }"><?= __('My profile') ?></a></li>
                                <li title="@{{inbox}} unread threads"><a href="<?= url('account/inbox') ?>"  <?= $route ?>="{ path: '/account/inbox' }"><?= __('Inbox') ?> <span class="badge badge-info" v-if="inbox > 0">@{{inbox}}</span></a></li>
                                <li><a href="<?= url('account/settings') ?>"  <?= $route ?>="{ path: '/account/settings' }"><?= __('Settings') ?></a></li>
                                <li><a href="" v-on:click="logout" v-on:click.prevent><?= __('Sign out') ?></a></li>
                            </ul>

                        </div>
                        
                    </template>
                    
                    <navbar :current-user="currentUser" :inbox="inbox"></navbar>

                </div>





            </nav>
			

    @yield('content')


<br /></div><!-- Wrap Div end -->

@include('partials/modal_login')
@include('partials/modal_forgot')
@include('partials/modal_message')
@include('partials/modal_register')
@include('partials/latest_members')
@include('partials/account')
@include('partials/account_profile')
@include('partials/account_dashboard')
@include('partials/account_settings')
@include('partials/account_gallery')
@include('partials/account_friends')
@include('partials/inbox')
@include('partials/inbox_thread')
@include('partials/account_sidebar')
@include('partials/registration_form')

</div>


<div class="footer footer-home">
    <div class="container">

        <div class="row">

            <div class="col-sm-4 col-xs-12">
                
            </div>			

            <div class="col-sm-8 col-xs-12">
                <p class="footer-links">
                    <a href="/"><?= __('Home') ?></a>
                    <a href="<?= get_page('about', $language) ?>"><?= __('About us') ?></a>
                    <a href="<?= get_page('privacy', $language) ?>"><?= __('Privacy policy') ?></a>
                    <a href="<?= get_page('terms', $language) ?>"><?= __('Terms and Conditions') ?></a>
                    <a href="<?= url('contact') ?>"><?= __('Contact') ?></a>
                </p>               
            </div>
        </div>        
		<br />
		<div class="row">
	

            <div class="col-xs-12">
                <p class="footer-links">
                    <a href="//getcouplet.com" class="text-muted"><?= __('Powered by Couplet') ?></a>
                </p>
            </div>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->



<!-- Latest compiled and minified JavaScript -->


<script src="https://checkout.stripe.com/checkout.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/superagent/2.2.0/superagent.min.js"></script>
<script src="{{asset('themes/default/js/sdk.js')}}"></script>
<script src="{{asset('themes/default/js/app.js')}}"></script>

<script>
var settings = <?= json_encode($settings) ?>;
var json_language = <?= json_encode($json_language) ?>;
function __(text) {
    return (json_language[text] !== undefined)?json_language[text]:text;
}

function site_url(path) {
    path = typeof path !== 'undefined' ? path : '';
    path = '/' + path;
    path = path.replace(new RegExp('//', 'g'), '/');
    var url = '<?= str_replace( '/', '\\/', (url(''))) ?>' + path;
    return url;
}
function asset_url(path) {
    path = typeof path !== 'undefined' ? path : '';
    return '<?= asset('') ?>' + path;
}
</script>
<script src="{{asset('themes/default/js/vue.js')}}?t=<?= time() ?>"></script>

</body>
</html>
