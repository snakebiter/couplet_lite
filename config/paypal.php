<?php
/**
 * PayPal Setting & API Credentials
 */

return [
    'mode' => 'sandbox', // Can only be 'sandbox' Or 'live'. If empty or invalid, 'live' will be used.
    'sandbox' => [
        'username' => env('PAYPAL_SANDBOX_API_USERNAME', 'support_api1.expresspixel.com'),
        'password' => env('PAYPAL_SANDBOX_API_PASSWORD', 'G8646D2GP9MA3ENK'),
        'secret' => env('PAYPAL_SANDBOX_API_SECRET', 'AFcWxV21C7fd0v3bYYYRCpSSRl31AvDgWjxGlYYAzsa2xJpYUjuM0i9.'),
        'certificate' => env('PAYPAL_SANDBOX_API_CERTIFICATE', ''),
        'app_id' => 'APP-80W284485P519543T',    // Used for testing Adaptive Payments API in sandbox mode
    ],
    'live' => [
        'username' => env('PAYPAL_LIVE_API_USERNAME', ''),
        'password' => env('PAYPAL_LIVE_API_PASSWORD', ''),
        'secret' => env('PAYPAL_LIVE_API_SECRET', ''),
        'certificate' => env('PAYPAL_LIVE_API_CERTIFICATE', ''),        
        'app_id' => '',         // Used for Adaptive Payments API 
    ],

    'payment_action' => 'Sale', // Can Only Be 'Sale', 'Authorization', 'Order'
    'currency' => 'USD',
    'notify_url' => '', // Change this accordingly for your application.
];
