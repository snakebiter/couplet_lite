<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '1'], function () {
    Route::get('/users/logout', 'UserController@getLogout');
    Route::get('/users/login', 'UserController@getLogin');
    Route::post('/users/register', 'UserController@register');
    Route::get('/users/me', 'UserController@getMe');
    Route::post('/members', 'MembersController@index');
    Route::get('/members/latest/{limit}', 'MembersController@latest');
    Route::get('/members/{id}', 'MembersController@show');
    Route::get('/members/{id}/photos', 'MembersController@getPhotos');
});

Route::post('/1/users/refresh', ['uses' => 'UserController@refresh', 'middleware' => 'jwt.refresh']); 

Route::group(['prefix' => '1', 'middleware' => 'jwt.auth'], function () {
    Route::put('/users/me', 'UserController@putMe');
    Route::get('/members/like/{id}', 'MembersController@like');
    Route::get('/members/liked/{id}', 'MembersController@liked');
    Route::post('/members/send-message/{id}', 'MembersController@sendMessage');
    Route::post('/users/upload-avatar', 'UserController@uploadAvatar');
    Route::post('/users/upload-photo', 'UserController@uploadPhoto');
    Route::delete('/users/photos/{id}', 'UserController@deletePhoto');
    Route::get('/users/photos', 'UserController@getPhotos');
    Route::get('/users/threads', 'UserController@getThreads');
    Route::get('/users/sent-threads', 'UserController@getSentThreads');
    Route::get('/users/thread-messages/{id}', 'UserController@getThreadMessages');
    Route::get('/users/latest-messages', 'UserController@getLatestMessages');
    Route::get('/users/unread', 'UserController@unread');
    Route::get('/members/mark-read/{id}', 'UserController@markAsRead');

    Route::get('/users/liked', 'UserController@liked');
    Route::get('/users/liked-you', 'UserController@likedYou');
    Route::get('/users/matches', 'UserController@matches');
    Route::get('/users/interactions', 'UserController@interactions');
});
