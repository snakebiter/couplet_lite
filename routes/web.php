<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'web'], function () {
   

    Route::get('/', 'HomeController@index');
    Route::get('/members', 'MembersController@view');
    Route::get('/profile/{id}', 'MembersController@getProfile');

    Route::get('/account', 'AccountController@index');
    Route::get('/account/dashboard', 'AccountController@index');
    Route::get('/account/profile', 'AccountController@index');
    Route::get('/account/friends', 'AccountController@index');
    Route::get('/account/gallery', 'AccountController@index');
    Route::get('/account/settings', 'AccountController@index');
    Route::get('/account/inbox', 'AccountController@index');
    Route::get('/account/inbox/{id}', 'AccountController@index');

    Route::any('/stripe/webhook', 'StripeController@postWebhook');
    Route::post('/stripe/after-payment', 'StripeController@postAfterPayment');
    Route::get('/paypal/start', 'PaypalController@getStart');
    Route::post('/paypal/ipn', 'PaypalController@postIPN');
    Route::get('/paypal/cancel', 'PaypalController@getCancel');
    Route::any('/paypal/success', 'PaypalController@getSuccess');

    Route::get('/page/{page}', 'PageController@getPage');
    Route::get('/page/{lang}/{page}', 'PageController@getPageTranslation');
    //Route::post('/publish', 'PublishController@postMessage');
    //Route::post('/themes', 'ThemeController@getIndex');
    Route::get('/contact', 'ContactController@getIndex');
    Route::post('/contact', 'ContactController@postIndex');

    Route::get('/login', 'UserController@postLogin');
    Route::post('/logout', 'AuthController@getLogout');

	Route::get('password/reset', ['as' => 'password.reset', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
    Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
    Route::get('password/reset/{token}', ['as' => 'password.reset.token', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
    Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\ResetPasswordController@reset']);
    Route::get('password/success', ['as' => 'password.reset.success', 'uses' => 'Auth\ResetPasswordController@success']);

	Route::get('/home', function () {
		return redirect('/');
	});	
    //Auth::routes();

});

